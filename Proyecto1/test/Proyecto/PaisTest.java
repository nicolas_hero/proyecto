package Proyecto;

import Mundo.Pais;
import junit.framework.TestCase;

public class PaisTest extends TestCase
{
	public static final String ARCHIVO_BOROUGHS = "./data/Boroughs.json";
	public static final String ARCHIVO_WARD = "./data/Ward.json";
	
	public static final String SOLICITUDES_ALCALDES = "./data/Solicitudes_Alcaldes.csv";
	public static final String SALIDA_PARQUEADERO = "./data/Salida_del_parqueadero.csv";
	public static final String INGRESO_PARQUEADERO = "./Ingreso_al_parqueadero.csv";
	
	
	
	private Pais pais;
	
	public void setupEscenario1()
	{
		Pais nuevo = new Pais("Colombia", ARCHIVO_BOROUGHS, SOLICITUDES_ALCALDES, ARCHIVO_WARD);
		
		String a  = nuevo.darNombre();
		
		assertEquals("El nombre no es el correcto", "Colomabia", a);
	}
	
	public void testAgregarObjetosBoroughs()
	{
		setupEscenario1();
		
		try
		{
			pais.agregarObjetosBoroughs(ARCHIVO_BOROUGHS, SOLICITUDES_ALCALDES);
		}
		catch(Exception e)
		{
			fail("Se genera una excepcion al intentar leer los archivos");
		}
		
	}
	
	public void testAgregarObjetosWard()
	{
		try
		{
			pais.agregarObjetosWard(ARCHIVO_WARD);
		}
		catch(Exception e)
		{
			fail("Se genera una excepcion al intentar leer los archivos");
		}
	}
}
