package Mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JEditorPane;

import Mundo.Lista.Nodo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class Pais
{
	private String nombre;

	private Lista<Area> areas;
	private Parqueadero parqueadero;
	private int maximoSlightA�o;
	private int maximoSerious;

	public Pais(String pNombre, String pRutaBoroughs,  String pRuta2Csv, String pRutaWard )
	{
		nombre = pNombre;
		areas = new Lista<Area>();
		maximoSlightA�o = 40;
		maximoSerious = 100;
		try
		{
			agregarObjetosBoroughs(pRutaBoroughs, pRuta2Csv);
			agregarObjetosWard(pRutaWard);
			agregarA�os(pRutaBoroughs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public String darNombre()
	{
		return nombre;
	}

	public void agregarObjetosBoroughs(String ruta, String ruta2) throws Exception
	{
		JsonParser boroughsActual = new JsonParser();
		try
		{ 
			JsonArray arr= (JsonArray) boroughsActual.parse(new FileReader(ruta));

			for (int i = 0; i < arr.size(); i++)
			{
				JsonObject obj = (JsonObject) arr.get(i);
				String area = obj.get("Area").getAsString();
				Area creado = new Area(area);
				areas.agregar(creado);
			}
		}
		catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}


	public void agregarObjetosWard(String pRuta)  throws Exception
	{
		JsonParser wardActual = new JsonParser();
		JsonArray arr= (JsonArray) wardActual.parse(new FileReader(pRuta));

		for (int i = 0; i < arr.size(); i++) 
		{
			JsonObject obj= (JsonObject) arr.get(i);

			String barrio = obj.get("Ward name").getAsString();
			String area = obj.get("Local Authority").getAsString();

			Nodo analizado = areas.darPrimero();
			boolean termino = false;

			while (!((Area) analizado.darElemento()).darNombre().equals(area) && !termino)
			{
				if (!(analizado.darSiguiente() == null))
				{
					analizado = analizado.darSiguiente();
				}
				else
				{
					termino = true;
				}				
			}

			Barrio barrioCreado  = new Barrio(barrio);			

			((Area) analizado.darElemento()).darBarrios().agregar(barrioCreado);

			Calendar cal = Calendar.getInstance();
			int a�oLimite = cal.get(Calendar.YEAR);

			for (int j = 2005; j <= a�oLimite; j++) 
			{
				JsonElement totalVer = obj.get("Total "+j);		
				JsonElement fatalVer = obj.get("Fatal " + j);
				JsonElement seriousVer = obj.get("Serius " + j);
				JsonElement slightVer = obj.get("Sliht " + j);

				if(totalVer != null && fatalVer != null && seriousVer != null && slightVer != null )
				{
					String total = totalVer.getAsString();
					String fatal = fatalVer.getAsString();
					String serious = seriousVer.getAsString();
					String slight = slightVer.getAsString();

					A�o  nuevo = new A�o(Integer.toString(j), Integer.parseInt(fatal), Integer.parseInt(serious), Integer.parseInt(slight), Integer.parseInt(total));

					((Barrio) ((Area) analizado.darElemento()).darBarrios().darUltimo().darElemento()).darA�os().agregar(nuevo);		

				}
			}
		}
	}	

	public void agregarA�os(String ruta) throws JsonIOException, JsonSyntaxException, FileNotFoundException
	{
		JsonParser boroughsActual = new JsonParser();
		JsonArray arr= (JsonArray) boroughsActual.parse(new FileReader(ruta));

		Calendar cal = Calendar.getInstance();
		int a�oLimite = cal.get(Calendar.YEAR);

		Nodo analizado = areas.darPrimero();

		while(analizado != null)
		{
			String areaDeLista = ((Area)analizado.darElemento()).darNombre(); 
			boolean ter = false;

			for (int i = 0; i < arr.size() && !ter; i++) 
			{
				JsonObject obj = (JsonObject)arr.get(i);
				String areaArch = obj.get("Area").getAsString(); 

				if(areaDeLista.compareTo(areaArch) == 0)
				{
					for (int j = 1900; j <= a�oLimite; j++) 
					{
						JsonElement fatalVer = obj.get("Fatal " + j);
						JsonElement seriousVer = obj.get("Serious " + j);
						JsonElement slightVer = obj.get("Slight " + j);
						JsonElement totalVer = obj.get("Total " + j);				

						if(fatalVer != null  && seriousVer != null && slightVer != null &&  totalVer != null)
						{
							String total = totalVer.getAsString();
							String fatal = fatalVer.getAsString();
							String serious = seriousVer.getAsString();
							String slight = slightVer.getAsString();

							A�o  nuevo = new A�o(Integer.toString(j), Integer.parseInt(fatal), Integer.parseInt(serious), Integer.parseInt(slight), Integer.parseInt(total));
							((Area)analizado.darElemento()).darA�os().agregar(nuevo);
							ter = true;
						}
					}
				}

			}
			analizado = analizado.darSiguiente();
		}
	}

	public String reporteSlight()
	{
		String respuesta = "Area Barrio A�o Slight: ";

		Nodo paseoPorArea = areas.darPrimero();


		while(paseoPorArea != null)
		{
			System.out.println("entro while");
			Nodo paseoPorBarrio = ((Area)(paseoPorArea.darElemento())).darBarrios().darPrimero();

			while(paseoPorBarrio != null)
			{
				System.out.println("entro segundo while");
				Nodo paseoPorA�o = ((Barrio) paseoPorBarrio.darElemento()).darA�os().darPrimero();

				while (paseoPorA�o != null)
				{
					System.out.println("entro tercer while");
					int slightAnalizado = ((A�o) paseoPorA�o.darElemento()).darLeves();
					if(slightAnalizado > maximoSlightA�o)
					{
						String nombreArea = ((Area) paseoPorArea.darElemento()).darNombre();
						String nombreBarrio = ((Barrio) paseoPorBarrio.darElemento()).darNombre();
						String numeroA�o = ((A�o) paseoPorA�o.darElemento()).darNumero();
						respuesta = respuesta + nombreArea + " " + nombreBarrio + " " + numeroA�o + " " + slightAnalizado + "\n";
					}

					paseoPorA�o = paseoPorA�o.darSiguiente();
				}

				paseoPorBarrio = paseoPorBarrio.darSiguiente();
			}

			paseoPorArea = paseoPorArea.darSiguiente();
		}

		return respuesta;
	}

	public String mayorSligthEnRango(int a�oMenor, int a�oMayor)
	{
		String respuesta = "";

		int maximo = a�oMayor;

		int minimo = a�oMenor;

		Nodo paseoPorArea = areas.darPrimero();

		Barrio mayor = null;

		int slightMayor = 0;

		while(paseoPorArea != null)
		{			
			Nodo paseoPorBarrio = ((Area)(paseoPorArea.darElemento())).darBarrios().darPrimero();

			while(paseoPorBarrio != null)
			{
				Nodo paseoPorA�o = ((Barrio) paseoPorBarrio.darElemento()).darA�os().darPrimero();

				while (paseoPorA�o != null)
				{

					int slightAnalizado = ((A�o) paseoPorA�o.darElemento()).darLeves();
					int numeroA�o = Integer.parseInt(((A�o) paseoPorA�o.darElemento()).darNumero());

					if(numeroA�o <= maximo && numeroA�o >= minimo)
					{
						if(slightAnalizado > slightMayor)
						{
							slightMayor = slightAnalizado;
							mayor = (Barrio) paseoPorBarrio.darElemento();
						}
					}

					paseoPorA�o = paseoPorA�o.darSiguiente();
				}

				paseoPorBarrio = paseoPorBarrio.darSiguiente();
			}

			paseoPorArea = paseoPorArea.darSiguiente();
		}

		respuesta = mayor.darNombre();

		return respuesta;


	}

	public String slightBarrio (String barrioNombre, String areaNombre)
	{
		String respuesta = "A�o Slight: ";

		Nodo paseoPorArea = areas.darPrimero();

		boolean encontroBarrio = false;

		boolean encontroArea = false;

		while(paseoPorArea != null && !encontroArea )
		{			
			Nodo paseoPorBarrio = ((Area)(paseoPorArea.darElemento())).darBarrios().darPrimero();
			String nombreAreaAnalizada = ((Area)(paseoPorArea.darElemento())).darNombre();

			if(nombreAreaAnalizada.equals(areaNombre))
			{
				encontroArea = true;
			}

			while(paseoPorBarrio != null && !encontroBarrio && encontroArea)
			{
				String nombreBarrioAnalizado = ((Barrio) paseoPorBarrio.darElemento()).darNombre();
				if (nombreBarrioAnalizado.equals(barrioNombre))
				{
					encontroBarrio = true;
				}
				Nodo paseoPorA�o = ((Barrio) paseoPorBarrio.darElemento()).darA�os().darPrimero();

				if( encontroBarrio && encontroArea)
				{
					while (paseoPorA�o != null)
					{
						String numero = ((A�o) paseoPorA�o.darElemento()).darNumero();
						int slights = ((A�o) paseoPorA�o.darElemento()).darLeves();
						respuesta = respuesta + numero + " " + slights + ",";

						paseoPorA�o = paseoPorA�o.darSiguiente();
					}
				}

				paseoPorBarrio = paseoPorBarrio.darSiguiente();
			}

			paseoPorArea = paseoPorArea.darSiguiente();
		}

		if(!(encontroArea && encontroBarrio))
		{

			respuesta = "No existe dicho Barrio, en el �rea en cuesti�n, o no existe el �rea.";
		}
		return respuesta;
	}

	public double promedioSlightBarrio( String barrioNombre, String areaNombre)
	{
		double respuesta = 0;

		Nodo paseoPorArea = areas.darPrimero();

		boolean encontroBarrio = false;

		boolean encontroArea = false;

		while(paseoPorArea != null && !encontroArea )
		{			
			Nodo paseoPorBarrio = ((Area)(paseoPorArea.darElemento())).darBarrios().darPrimero();
			String nombreAreaAnalizada = ((Area)(paseoPorArea.darElemento())).darNombre();

			if(nombreAreaAnalizada.equals(areaNombre))
			{
				encontroArea = true;
			}

			while(paseoPorBarrio != null && !encontroBarrio && encontroArea)
			{
				String nombreBarrioAnalizado = ((Barrio) paseoPorBarrio.darElemento()).darNombre();
				if (nombreBarrioAnalizado.equals(barrioNombre))
				{
					encontroBarrio = true;
				}
				Nodo paseoPorA�o = ((Barrio) paseoPorBarrio.darElemento()).darA�os().darPrimero();

				if( encontroBarrio && encontroArea)
				{
					double slightTotal = 0;
					int ultimoA�o = 0;
					while (paseoPorA�o != null)
					{
						int slights = ((A�o) paseoPorA�o.darElemento()).darLeves();

						slightTotal += slights;	

						int a�oAnalizado = Integer.parseInt(((A�o) paseoPorA�o.darElemento()).darNumero());

						if (a�oAnalizado > ultimoA�o)
						{
							ultimoA�o = a�oAnalizado;
						}
						paseoPorA�o = paseoPorA�o.darSiguiente();
					}

					respuesta = slightTotal/(ultimoA�o-2010);


				}

				paseoPorBarrio = paseoPorBarrio.darSiguiente();
			}

			paseoPorArea = paseoPorArea.darSiguiente();
		}

		return respuesta;
	}

	public int slightRecienteBarrio (String barrioNombre, String areaNombre)
	{
		int respuesta = 0;

		Nodo paseoPorArea = areas.darPrimero();

		boolean encontroBarrio = false;

		boolean encontroArea = false;

		while(paseoPorArea != null && !encontroArea )
		{			
			Nodo paseoPorBarrio = ((Area)(paseoPorArea.darElemento())).darBarrios().darPrimero();
			String nombreAreaAnalizada = ((Area)(paseoPorArea.darElemento())).darNombre();

			if(nombreAreaAnalizada.equals(areaNombre))
			{
				encontroArea = true;
			}

			while(paseoPorBarrio != null && !encontroBarrio && encontroArea)
			{
				String nombreBarrioAnalizado = ((Barrio) paseoPorBarrio.darElemento()).darNombre();
				if (nombreBarrioAnalizado.equals(barrioNombre))
				{
					encontroBarrio = true;
				}				

				if( encontroBarrio && encontroArea)
				{
					int slightUltimoA�o = ((A�o)(((Barrio) paseoPorBarrio.darElemento()).darA�os().darUltimo().darElemento())).darLeves();
					respuesta = slightUltimoA�o;
				}

				paseoPorBarrio = paseoPorBarrio.darSiguiente();
			}

			paseoPorArea = paseoPorArea.darSiguiente();
		}

		return respuesta;
	}


	//-------------------------------------------------------------------------------------------------------
	// METODOS PARA LA PARTE B.
	//-------------------------------------------------------------------------------------------------------

	/**
	 * Retorna el reporte para todas las areas sobre las colsiones serias por cada a�o.
	 * @return el reporte.
	 */
	public String reporteSerious()
	{
		String mensaje = "Serious collisions by cities: \n";

		Nodo nodoAreaActual = areas.darPrimero();

		while(nodoAreaActual != null)
		{
			String nombreDelArea = ((Area)nodoAreaActual.darElemento()).darNombre();

			Nodo a�oActual = ((Area)nodoAreaActual.darElemento()).darA�os().darPrimero();

			while(a�oActual != null)
			{
				String elA�o = ((A�o)a�oActual.darElemento()).darNumero();
				int colisionesSerias = ((A�o)a�oActual.darElemento()).darSerias();

				if(colisionesSerias > maximoSerious)
				{
					mensaje+= " Nombre de la ciudad: "+nombreDelArea+"\n Numero de colisiones Serious: "+colisionesSerias+"\n A�o en que ocurrio: " + elA�o;
				}

				mensaje+="\n";
				a�oActual = a�oActual.darSiguiente();
			}
			nodoAreaActual = nodoAreaActual.darSiguiente();
		}
		return mensaje;
	}

	/**
	 * Retorna el area con mayor indice de colsiones para un rango de a�os.
	 * @param a�oInicio a�o menor del rango.
	 * @param a�oFin a�o mayor del rango.
	 * @return nombre del area con mayor inidice de colsiones serias en el rango dado.
	 */
	public String areaIndiceColisionesSeriousRango(int a�oInicio, int a�oFin)
	{
		String res = "";
		int mayor = 0;

		Nodo areaActual = areas.darPrimero();

		while(areaActual != null)
		{
			Nodo a�oActual = ((Area)areaActual.darElemento()).darA�os().darPrimero();
			int sumatoriaSeriousRango = 0;

			while(a�oActual != null)
			{
				String elA�o = ((A�o)a�oActual.darElemento()).darNumero();

				if(Integer.parseInt(elA�o) >= a�oInicio  && Integer.parseInt(elA�o) <= a�oFin )
				{
					int cantidadSeriosA�o = ((A�o)a�oActual.darElemento()).darSerias();
					sumatoriaSeriousRango+=cantidadSeriosA�o;
				}
				a�oActual = a�oActual.darSiguiente();
			}

			if(mayor < sumatoriaSeriousRango)
			{
				mayor = sumatoriaSeriousRango;
				res = ((Area)areaActual.darElemento()).darNombre();
			}
			areaActual = areaActual.darSiguiente();
		}

		return res;
	}

	/**
	 * Retorna el historial de colisiones serias de todos los a�os para el area dada por parametro
	 * @param pArea nombre del area de la cual se requiere imformacion.
	 * @return historial de colsiones serias por a�o.
	 */
	public String historialColisionesSeriousArea(String pArea)
	{
		String res = "Historial de colsiones serious para el area:  " + pArea +  " \n";

		Nodo areaActual = areas.darPrimero();

		while(areaActual != null)
		{
			System.out.println("entra 1");
			String nombreArea = ((Area)areaActual.darElemento()).darNombre();

			if(nombreArea.compareTo(pArea) == 0)
			{
				System.out.println("entra 2");
				Nodo a�oActual = ((Area) areaActual.darElemento()).darA�os().darPrimero();

				while(a�oActual != null)
				{
					System.out.println("entra 3");
					String elA�o = ((A�o) a�oActual.darElemento()).darNumero();
					int lasSerious = ((A�o)a�oActual.darElemento()).darSerias();

					res += "Para el a�o: " + elA�o + "  La cantidad de colsiones serious fueron: "+ lasSerious + "\n"; 
					a�oActual = a�oActual.darSiguiente();
				}
				res+="\n";
			}
			areaActual = areaActual.darSiguiente();
		}
		return res;
	}


	/**
	 * Retorna el promedio de colsiones serias entre todos los a�os para el area dada por parametro.
	 * @param pArea nombre del area de la cual se requiere informacion.
	 * @return promedio de colisiones serias.
	 */
	public String promedioColsionesSeriousArea(String pArea)
	{
		double prom = 0;

		Nodo areaActual = areas.darPrimero();
		boolean encontroArea = false;

		while(areaActual != null && !encontroArea)
		{
			String nombreArea = ((Area) areaActual.darElemento()).darNombre();

			if(nombreArea.compareTo(pArea) == 0)
			{
				Nodo a�oActual = ((Area)areaActual.darElemento()).darA�os().darPrimero();
				encontroArea = true;
				int cantidadTotalSeriousArea = 0;
				int a�os = 0;

				while(a�oActual != null)
				{
					int colSirious = ((A�o)a�oActual.darElemento()).darSerias();
					cantidadTotalSeriousArea+=colSirious;
					a�os++;

					a�oActual = a�oActual.darSiguiente();
				}
				prom = cantidadTotalSeriousArea/a�os;
			}
			areaActual = areaActual.darSiguiente();
		}
		return "El promedio de colsiones para el area:  "+pArea+ " es de:" +prom;
	}

	/**
	 * Retorna el valor mas reciente de la colision seria para el area dada por parametro.
	 * @param pArea Nombre del area de la cual se quiere informacion
	 * @return valor reciente de colsicion seria.
	 */
	public int valorRecienteSeriousArea(String pArea)
	{
		int  res = 0;
		Nodo areaActual = areas.darPrimero();

		while(areaActual != null)
		{
			String nombreArea = ((Area)areaActual.darElemento()).darNombre();

			if(nombreArea.compareTo(pArea) == 0)
			{
				Nodo a�o = ((Area)areaActual.darElemento()).darA�os().darUltimo();
				int cant = ((A�o)a�o.darElemento()).darSerias();

				res = cant;
			}
			areaActual = areaActual.darSiguiente();
		}

		return res;
	}
}
