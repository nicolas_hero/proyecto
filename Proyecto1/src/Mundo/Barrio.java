package Mundo;

public class Barrio 
{
	private String nombre;
	
	private Lista<A�o> a�os;
	
	public Barrio(String pNombre)
	{
		nombre = pNombre;
		a�os = new Lista<A�o>();
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public Lista<A�o> darA�os()
	{
		return a�os;
	}
}
