package Mundo;

public class Lista<T> 
{
	private Nodo primero;
	
	private Nodo ultimo;
	
	private int tama�o;
	
	public class Nodo<T>
	{
		private Nodo siguiente;
		
		private Nodo anterior;
		
		private T elemento;
		
		public Nodo(T pElemento)
		{
			siguiente = null;
			anterior = null;
			elemento = pElemento;
		}
		
		public Nodo darSiguiente()
		{
			return siguiente;
		}
		
		public Nodo darAnterior()
		{
			return anterior;
		}
		
		public T darElemento()
		{
			return elemento;
		}
		
		public void insertar(Nodo pSig)
		{
			siguiente = pSig;
			pSig.anterior = this;
		}
	}
	
	public Lista()
	{
		primero = null;
		ultimo = null;
		tama�o = 0;
	}
	
	public Nodo darPrimero()
	{
		return primero;
	}
	
	public Nodo darUltimo()
	{
		return ultimo;
	}
	
	public int darTama�o()
	{
		return tama�o;
	}
	
	public boolean vacio()
	{
		boolean estaVacio = false;
		
		if(tama�o==0 && primero == null)
		{
			estaVacio = true;
		}
		
		return estaVacio;
	}
	
	public void agregar(T pElemento)
	{
		Nodo agregado = new Nodo(pElemento);
		
		if(vacio())
		{
			primero = agregado;
			ultimo = agregado;
		}
		else
		{
			ultimo.insertar(agregado);
			ultimo = agregado;
		}
	}
}
