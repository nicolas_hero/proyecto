package Mundo;

import java.util.Date;
import java.util.Iterator;
import java.util.Queue;
import java.util.Stack;


public class Parqueadero <T>
{
	/**
	 * Constante que modela la longitud de la pila de carros.
	 */
	public static final int LONGITUD_PILA = 5;
	
    /**
     * Cola de carros en espera para ser estacionados
     */
	
	private Cola<Carro> colaEnEspera;
	
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	
	private Pila<Carro>[] pilasParqueaderos;
	  
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	
	private Pila<Carro> pilaTemporal;
   
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Parqueadero ()
    {
        colaEnEspera = new Cola<Carro>();
        pilasParqueaderos = new Pila[LONGITUD_PILA];
        pilaTemporal = new Pila<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro carroEntrante = new Carro(pColor, pMatricula, pNombreConductor);
    	colaEnEspera.encolar(carroEntrante);    	
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	String respuesta;
    	Carro porParquear = colaEnEspera.desencolar();
    	if (porParquear == null)
    	{
    		throw new Exception("No hay carros en la cola de esperar para parquear.");
    	}
    	else
    	{
    		String matriculaParqueado = porParquear.darMatricula();
    		String parqueoCarro = parquearCarro(porParquear);
    		
    		respuesta = matriculaParqueado + " " + parqueoCarro;
    	}
    	
        return respuesta;
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {    	
    	Carro sacado = sacarCarro(matricula);
    	double tarifa = 0;
    	if (sacado == null)
    	{
    		throw new Exception ("El auto no est� en ning�n parqueadero");
    	}
    	else
    	{
    		tarifa = cobrarTarifa(sacado);
    	}    	
    		
    	return tarifa;
    }
    
  /**
   * Busca un parqueadero con cupo dentro de los 8 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro.
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	
    	String respuesta;
    	String ubicaci�n = "";
    	try
    	{
	    	int i = 1;
	    	boolean parqueado = false;
	    	while (i < 6 && !parqueado)
	    	{
	    		if(pilasParqueaderos[i].darTama�o()<8)
	    		{
	    			pilasParqueaderos[i].push(aParquear);
	    			ubicaci�n = "P" + i;
	    		}
	    		else
	    		{
	    			i++;	    			
	    		}
	    		
	    	}
    	}
    	catch (Exception e)
    	{
    		throw new Exception("No hay parqueaderos con espacio alguno.");
    	}
    	
    	respuesta = ubicaci�n;
    	
    	return respuesta;    	    
    }
    
    /**
     * Itera sobre los ocho parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	int i = 1;
    	boolean encontro = false;
    	String matBuscada = matricula;
    	Carro sacado = null;
    	while ( i < 6 && !encontro)
    	{
    		while( !pilasParqueaderos[i].vac�o() && !encontro)
    		{
    			Carro analizado = pilasParqueaderos[i].pop();
    			if (analizado.darMatricula().equals(matBuscada))
    			{
    				encontro = true;
    				pilasParqueaderos[i].push(analizado);
    				while (!pilaTemporal.vac�o())
    				{
    					pilasParqueaderos[i].push(pilaTemporal.pop());
    				}
    			}
    			else
    			{
    				pilaTemporal.push(analizado);
    			}
    		}
    		
    		if (!encontro)
    		{    			
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[i].push(pilaTemporal.pop());
    			}
    			i++;
    		}
    		else
    		{
    			if (i == 1)
    			{
    				sacado = sacarCarroP1(matBuscada);
    			}
    			else if (i == 2)
    			{
    				sacado = sacarCarroP2(matBuscada);
    			}
    			else if (i == 3)
    			{
    				sacado = sacarCarroP3(matBuscada);
    			}
    			else if (i == 4)
    			{
    				sacado = sacarCarroP4(matBuscada);
    			}
    			else if (i == 5)
    			{
    				sacado = sacarCarroP5(matBuscada);
    			}   			
    		}
    	}    	
    	
    	return sacado;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	boolean saco = false;
    	Carro sacado = null;
    	while(!pilasParqueaderos[1].vac�o() && !saco)
    	{
    		if(((Carro)pilasParqueaderos[1].darTope().darElemento()).darMatricula().equals(matricula))
    		{
    			sacado = pilasParqueaderos[1].pop();
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[1].push(pilaTemporal.pop());
    			}
    			saco = true;
    		}
    		else
    		{
    			pilaTemporal.push(pilasParqueaderos[1].pop());
    		}
    	}
    	return sacado;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	boolean saco = false;
    	Carro sacado = null;
    	while(!pilasParqueaderos[2].vac�o() && !saco)
    	{
    		if(((Carro)pilasParqueaderos[2].darTope().darElemento()).darMatricula().equals(matricula))
    		{
    			sacado = pilasParqueaderos[2].pop();
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[2].push(pilaTemporal.pop());
    			}
    			saco = true;
    		}
    		else
    		{
    			pilaTemporal.push(pilasParqueaderos[2].pop());
    		}
    	}
    	return sacado;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	boolean saco = false;
    	Carro sacado = null;
    	while(!pilasParqueaderos[3].vac�o() && !saco)
    	{
    		if(((Carro)pilasParqueaderos[3].darTope().darElemento()).darMatricula().equals(matricula))
    		{
    			sacado = pilasParqueaderos[3].pop();
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[3].push(pilaTemporal.pop());
    			}
    			saco = true;
    		}
    		else
    		{
    			pilaTemporal.push(pilasParqueaderos[3].pop());
    		}
    	}
    	return sacado;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	boolean saco = false;
    	Carro sacado = null;
    	while(!pilasParqueaderos[1].vac�o() && !saco)
    	{
    		if(((Carro)pilasParqueaderos[4].darTope().darElemento()).darMatricula().equals(matricula))
    		{
    			sacado = pilasParqueaderos[4].pop();
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[4].push(pilaTemporal.pop());
    			}
    			saco = true;
    		}
    		else
    		{
    			pilaTemporal.push(pilasParqueaderos[4].pop());
    		}
    	}
    	return sacado;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	boolean saco = false;
    	Carro sacado = null;
    	while(!pilasParqueaderos[5].vac�o() && !saco)
    	{
    		if(((Carro)pilasParqueaderos[5].darTope().darElemento()).darMatricula().equals(matricula))
    		{
    			sacado = pilasParqueaderos[5].pop();
    			while(!pilaTemporal.vac�o())
    			{
    				pilasParqueaderos[5].push(pilaTemporal.pop());
    			}
    			saco = true;
    		}
    		else
    		{
    			pilaTemporal.push(pilasParqueaderos[5].pop());
    		}
    	}
    	return sacado;
    }
    
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	Date ahora = new Date();
    	long ahoraMili = ahora.getTime();
    	int minutosParqueado = (int) ((car.darLlegada() - ahoraMili)/100);
    	double tarifa = minutosParqueado * 25;
    	return tarifa;	
    }
}
