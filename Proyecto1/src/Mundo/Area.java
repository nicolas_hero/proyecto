package Mundo;

public class Area
{
	private Lista<Barrio> barrios;
	
	private Lista<A�o> a�os;
	
	private String nombre;
	
	private String alcalde;
	
	public Area(String pNombre)
	{
		nombre = pNombre;
		barrios = new Lista<Barrio>();
		a�os = new Lista<A�o>();
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public String darAlcalde()
	{
		return alcalde;
	}
	
	public Lista<Barrio> darBarrios()
	{
		return barrios;
	}
	
	public Lista<A�o> darA�os()
	{
		return a�os;
	}
}
