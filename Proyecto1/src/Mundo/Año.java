package Mundo;

public class A�o 
{
	private String numero;
	
	private int leves;
	
	private int fatales;
	
	private int serias;
	
	private int totales;
	
	public A�o(String pNumero, int pLeves, int pSerias, int pFatales, int pTotales)
	{
		numero = pNumero;
		leves = pLeves;
		serias = pSerias;
		fatales = pFatales;
		totales = pTotales;
	}
	
	public String darNumero()
	{
		return numero;
	}
	
	public int darLeves()
	{
		return leves;
	}
	
	public int darFatales()
	{
		return fatales;
	}
	
	public int darSerias()
	{
		return serias;
	}
	
	public int darTotales()
	{
		return totales;
	}
}
