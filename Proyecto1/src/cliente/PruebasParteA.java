package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import Mundo.Pais;

public class PruebasParteA 
{
	public static final String RUTA_BOROUGHS = "data/Boroughs.json";
	public static final String RUTA_WARD = "data/Ward.json";
	public static final String SOLICITUDES_ALCALDES = "data/Solicitudes_Alcaldes.csv";
	
	BufferedWriter escritor;
	Scanner lector;
	
	//TODO: Declarar objetos de la parte A
	
	public Pais pais;
	
	public PruebasParteA(BufferedWriter escritor, Scanner lector) 
	{
		this.escritor = escritor;
		this.lector = lector;
	}
	
	public void pruebas()
	{
		int opcion = -1;
				
		pais = new Pais("Inglaterra", RUTA_BOROUGHS, SOLICITUDES_ALCALDES, RUTA_WARD);
		long tiempoDeCarga = System.nanoTime();
		
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto A---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de autoridades locales con mas\n     de 40 colisiones Slight al ano\n");
				escritor.write("2: Barrios en que mas generaron alarmas Slight\n     en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Slight para un barrio\n");
				escritor.write("4: Promedio de colisiones Slight para un barrio\n");
				escritor.write("5: Valor mas reciente de colisiones Slight de un\n     barrio\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte4(); break;
				case 5: reporte5(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException
	{
		long tiempo = System.nanoTime();
		
		String reporte = pais.reporteSlight();
		escritor.write(reporte);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte2() throws IOException{
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		String rep = pais.mayorSligthEnRango(inicio, fin);
		escritor.write(rep);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte3() throws IOException{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		
		escritor.write("Ingrese el area del barrio \n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		
		String repo = pais.slightBarrio(barrio, area);
		escritor.write(repo+ "\n");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte4() throws IOException
	{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		
		escritor.write("Ingrese el area del barrio \n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		
		long tiempo = System.nanoTime();
		double prom = pais.promedioSlightBarrio(barrio, area);
		escritor.write(""+prom+"\n");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte5() throws IOException{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		
		escritor.write("Ingrese el area del barrio \n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		
		int valor = pais.slightRecienteBarrio(barrio, area);
		escritor.write(valor+"\n");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}