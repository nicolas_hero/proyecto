package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import Mundo.Pais;

public class PruebasParteB {
	
	public static final String RUTA_BOROUGHS = "data/Boroughs.json";
	public static final String RUTA_WARD = "data/Ward.json";
	public static final String SOLICITUDES_ALCALDES = "data/Solicitudes_Alcaldes.csv";
	
	BufferedWriter escritor;
	Scanner lector;
	
	public Pais pais;
	
	public PruebasParteB(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}
	
	public void pruebas() {
		int opcion = -1;
				
		pais = new Pais("Inglaterra",RUTA_BOROUGHS, SOLICITUDES_ALCALDES, RUTA_WARD);
		long tiempoDeCarga = System.nanoTime();		
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto B---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de colisiones Serious que\n     estan por encima del maximo permitido\n");
				escritor.write("2: Area con el indice de colisiones Serios mas\n     alto en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Serios para un area\n");
				escritor.write("4: Promedio de colisiones Serious para un area\n");
				escritor.write("5: Valor mas reciente de colisiones Serious de\n     un area\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte4(); break;
				case 5: reporte5(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException{
		long tiempo = System.nanoTime();
		String res = pais.reporteSerious();
		
		escritor.write(res);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte2() throws IOException{
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		String area = pais.areaIndiceColisionesSeriousRango(inicio, fin);
		
		escritor.write(area + "\n") ;
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte3() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		String historial = pais.historialColisionesSeriousArea(area);
		escritor.write(historial);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte4() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		String prom = pais.promedioColsionesSeriousArea(area);
		escritor.write(prom);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte5() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		int valor = pais.valorRecienteSeriousArea(area);
		escritor.write(valor);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}