package Estructuras;

public class TablaHashLP <Key extends Comparable<Key>, Value> implements ITablaHash<Key, Value> 
{
	private int num;

	private int tam = 100;

	private Key[] keys;

	private Value[] vals;
	
	@SuppressWarnings("unchecked")

	public TablaHashLP() 
	{
		
		keys = (Key[]) new Comparable[tam];

		vals = (Value[]) new Object[tam];

	}

	public void put(Key key, Value value) 
	{
		
		if(num >= tam/2) resize(2*tam);

		
		
		int i;

		for(i = hash(key); keys[i] != null; i = (i+1) % tam)
			
			if(keys[i].equals(key)) { vals[i] = value; return;}

		

		keys[i] = key;
		
		vals[i] = value;
		
		num++;
		
	}

	

	public Value get(Key key) 
	{

		for(int i = hash(key); keys[i] != null; i = (i+1) % tam)

			if(keys[i].equals(key)) return vals[i];

		

		return null;

	}

	

	public int hash(Key key) 
	{

		return (key.hashCode() & 0x7fffffff) % tam;

	}

	

	public void delete(Key key) 
	{

		if(!contains(key)) return;

		

		int i = hash(key);

		while(!key.equals(keys[i])) 
		{

			i = (i+1) % tam;

		}

		

		keys[i] = null;

		vals[i] = null;

		

		i = (i+1) % tam;

		

		while(keys[i] != null) 
		{

			Key keyToRedo = keys[i];

			Value valueToRedo = vals[i];

			keys[i] = null;

			vals[i] = null;

			num--;

			put(keyToRedo, valueToRedo);

			i = (i+1) % tam;

		}

		num--;

		

		if(num > 0 && num == tam/8)

			resize(tam/2);

	}

	

    // Helpers 

	

	public void resize(int size) 
	{

		  TablaHashLP<Key, Value> t = new TablaHashLP<Key, Value>();

		  for(int i = 0; i < tam; i++) 
		  {

			  if(keys[i] != null)

				  t.put(keys[i], vals[i]);

		  }

		  

		  keys = t.keys;

		  vals = t.vals;

		  tam = t.tam;

		  num = t.num;

	}

	

	public boolean contains(Key key) 
	{

		for(int i = hash(key); keys[i] != null; i = (i+1) % tam) {

			if(keys[i].equals(key))

				return true;

		}

		

		return false;

	
	}
}
