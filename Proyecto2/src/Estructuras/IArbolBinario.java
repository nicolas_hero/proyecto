package Estructuras;

public interface IArbolBinario <Key extends Comparable<Key>, Value>
{
	public void put(Key key, Value value);
	public Value get(Key key);
	public int size();
	public void delete(Key key);
}
