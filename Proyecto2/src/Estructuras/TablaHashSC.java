package Estructuras;

public class TablaHashSC <Key extends Comparable<Key>, Value> implements ITablaHash<Key, Value> 
{
	private int num;

	private int tam;

	private SequentialSymbolTable<Key, Value>[] st;	

	public TablaHashSC() 
	{

		this(997);

	}
	

	@SuppressWarnings("unchecked")

	public TablaHashSC(int tam) 
	{

		this.tam = tam;

		st = (SequentialSymbolTable<Key, Value>[]) new SequentialSymbolTable[tam];

		for(int i = 0; i < st.length; i++) 
		{

			st[i] = new SequentialSymbolTable<Key,Value>();

		}

	}

	

	public void put(Key key, Value value) 
	{

		st[hash(key)].put(key, value);

		num++;

	}

	

	public Value get(Key key) 
	{

		return (Value) st[hash(key)].get(key);

	}

	

	public void delete(Key key) 
	{

		st[hash(key)].delete(key);

		num--;

	}

	

	public int hash(Key key) 
	{

		return (key.hashCode() & 0x7fffffff) % tam;

	}
}
