package Estructuras;

public interface IHeapMax<Key> 
{
	public void insert(Key i);

	public Key delMax();

	public boolean isEmpty();

	public int size();

	public void swim(int k);

	public void sink(int k);
}
