package Estructuras;

public class Lista<T> 
{
	private Nodo<T> primero;
	
	private Nodo<T> ultimo;
	
	private int tama�o;
	
	public class Nodo<C>
	{
		private Nodo<C> siguiente;
		
		private Nodo<C> anterior;
		
		private C elemento;
		
		public Nodo(C pElemento)
		{
			siguiente = null;
			anterior = null;
			elemento = pElemento;
		}
		
		public Nodo<C> darSiguiente()
		{
			return siguiente;
		}
		
		public Nodo<C> darAnterior()
		{
			return anterior;
		}
		
		public C darElemento()
		{
			return elemento;
		}
		
		public void insertar(Nodo<C> pSig)
		{
			siguiente = pSig;
			pSig.anterior = this;
		}
	}
	
	public Lista()
	{
		primero = null;
		ultimo = null;
		tama�o = 0;
	}
	
	public Nodo<T> darPrimero()
	{
		return primero;
	}
	
	public Nodo<T> darUltimo()
	{
		return ultimo;
	}
	
	public int darTama�o()
	{
		return tama�o;
	}
	
	public boolean vacio()
	{
		boolean estaVacio = false;
		
		if(tama�o==0 && primero == null)
		{
			estaVacio = true;
		}
		
		return estaVacio;
	}
	
	public void agregar(T pElemento)
	{
		Nodo<T> agregado = new Nodo<T>(pElemento);
		
		if(vacio())
		{
			primero = agregado;
			ultimo = agregado;
		}
		else
		{
			ultimo.insertar(agregado);
			ultimo = agregado;
		}
	}
}
