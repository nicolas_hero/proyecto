package Mundo;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JEditorPane;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Estructuras.ArbolBinario;
import Estructuras.HeapMax;
import Estructuras.Lista;
import Estructuras.Lista.Nodo;
import Estructuras.TablaHashLP;
import Estructuras.TablaHashSC;

public class Base
{
	//------------------------------------------------------------------------------------------
	// Constantes
	//------------------------------------------------------------------------------------------
	
	public static final String BOROUGH_CHILD = "data/road-casualties-severity-borough-child.json";
	public static final String BOROUGH_YEARS_WEIGHT = "data/road-casualties-severity-borough-years-weight.json";
	public static final String BOROUGH = "data/road-casualties-severity-borough.json";
	public static final String BOROUGH_ALL = "data/traffic-flow-borough-all.json";
	public static final String BOROUGH_CARS = "data/traffic-flow-borough-cars.json";
	public static final String TRAFFIC_BOROUGH_YEARS_WEIGHT = "data/traffic-flow-borough-years-weight.json";

	
	//------------------------------------------------------------------------------------------
	// Atributos
	//------------------------------------------------------------------------------------------
	
	private HeapMax<Authority> heapA;
	private Lista<Authority> local;
	private HeapMax<Authority> heapB;
	private TablaHashSC<String,TablaHashLP<String,Integer>> tablaSCA;
	private TablaHashSC<String,TablaHashLP<String,Integer>> tablaSCB;
	private ArbolBinario<String, String> arbolLA;
	private ArbolBinario<String, ArrayList> arbolCODA;
	private ArbolBinario<String, ArrayList> arbolCODB;
	
	//------------------------------------------------------------------------------------------
	// Constructor
	//------------------------------------------------------------------------------------------
	
	public Base()
	{	
		local = new Lista<Authority>(); 
		heapA = new HeapMax<Authority>(1000);
		heapB = new HeapMax<Authority>(1000);
		tablaSCA = new TablaHashSC<String,TablaHashLP<String, Integer>>();
		tablaSCB = new TablaHashSC<String,TablaHashLP<String, Integer>>();
		arbolLA = new ArbolBinario<String, String>();
		arbolCODA = new ArbolBinario<String, ArrayList>();
		arbolCODB = new ArbolBinario<String, ArrayList>();
		
		try
		{
			leerInformacion_A(BOROUGH, BOROUGH_YEARS_WEIGHT, BOROUGH_ALL, BOROUGH_CARS, TRAFFIC_BOROUGH_YEARS_WEIGHT);			
			leerInformacion_B(BOROUGH_CHILD, BOROUGH_YEARS_WEIGHT);
			armarTablasA();
			armarTablasB();
			armarArbolLA();			
			armarArbolCODA();			
			armarArbolCODB();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		
		
		
		
		

	}
	
	//------------------------------------------------------------------------------------------
	// Metodos
	//------------------------------------------------------------------------------------------
	
	/**
	 * Guarda la informacion en memoria y crea el heap para los local authority con mayor prioridad
	 * @param pRutaChild Ruta del archivo para la informacion de ni�os accidentados entre 0-15 a�os de edad.
	 * @param pRutaWeight Ruta del archivo que contiene la informacion del peso de cada a�o.
	 * @throws Exception Excepcion si no se puede leer los archivos.
	 */
	
	public HeapMax<Authority> darHeapA()
	{
		return heapA;
	}
	
	public HeapMax<Authority> darHeapB()
	{
		return heapB;
	}
	
	public TablaHashSC<String, TablaHashLP<String, Integer>> darTablaSCA()
	{
		return tablaSCA;
	}
	
	public TablaHashSC<String, TablaHashLP<String, Integer>> darTablaSCB()
	{
		return tablaSCB;
	}
	
	public void leerInformacion_A(String pRutaBorough, String pRutaWeight, String pRutaAll, String pRutaCars, String pRutaTrafficWeight) throws Exception 
	{
		Calendar cal = Calendar.getInstance();
		int fecha = cal.get(Calendar.YEAR);

		
		JsonParser boroughsBorough = new JsonParser();
		JsonParser boroughsWeight = new JsonParser();
		JsonParser trafficAll = new JsonParser();
		JsonParser trafficCars = new JsonParser();
		JsonParser trafficWeight = new JsonParser();

		JsonArray arrBorough= (JsonArray) boroughsBorough.parse(new FileReader(pRutaBorough));
		JsonArray arrWeight= (JsonArray) boroughsWeight.parse(new FileReader(pRutaWeight));
		JsonArray arrAll = (JsonArray) trafficAll.parse(new FileReader(pRutaAll));
		JsonArray arrCars = (JsonArray) trafficCars.parse(new FileReader(pRutaCars));
		JsonArray arrTrafficWeight = (JsonArray) trafficWeight.parse(new FileReader(pRutaTrafficWeight));


		for (int i = 0; i < arrBorough.size(); i++) 
		{
			

			JsonObject obj = (JsonObject)arrBorough.get(i);
			

			String autho = obj.get("Local-Authority").getAsString();
			

			String codeN = obj.get("Code").getAsString();


			Authority nuevo = new Authority(autho, codeN);
			local.agregar(nuevo);
		}
		
		//Con tama�o de la lista de los local Authority, entonces se inicializan el heapA y el heapB con respecto a ellas.
		
		

		Nodo actual = local.darPrimero();
		
		while(actual != null)
		{
			
			String nom = ((Authority)actual.darElemento()).getLocalAuthority();
			
			for (int i = 0; i < arrBorough.size(); i++) 
			{
				
				JsonObject obj = (JsonObject)arrBorough.get(i);
				String nombArch = obj.get("Local-Authority").getAsString();
				if(nom.equals(nombArch))
				{
					int j = 0;
					while(j<=fecha )
					{
						JsonElement ele = obj.get(""+j);

						if(ele != null)
						{
							int p2 = ele.getAsInt();
							
							A�o a�oNuevo = new A�o(Integer.toString(j));
							a�oNuevo.insertarValorA(p2);
							((Authority)actual.darElemento()).getListaA�os().agregar(a�oNuevo);
						}
						j = j+1;
					}
				}
			}

			actual = actual.darSiguiente();
		}
		

		actual = local.darPrimero();
		while(actual != null)
		{
			Nodo a�oAc = ((Authority)actual.darElemento()).getListaA�os().darPrimero();
			while(a�oAc != null)
			{
				for (int i = 0; i < arrWeight.size(); i++) 
				{
					JsonObject ob= (JsonObject)arrWeight.get(i);
					int a = 0;
					
					while(a < fecha)
					{
						JsonElement element = ob.get(""+a);
						if(element != null)
						{
							String al = ((A�o)a�oAc.darElemento()).getNumeroA�o();
							double pes = element.getAsDouble();
							if(Integer.parseInt(al) == a)
							{
								((A�o)a�oAc.darElemento()).insertarPeso(pes);
							}
						}
						a = a+1;
					}
				}
				a�oAc = a�oAc.darSiguiente();
			}
			actual = actual.darSiguiente();
		}

		//Agregamos traffic cars, all y pesos.
		
		actual = local.darPrimero();
		
		while(actual != null)
		{
			Authority paraAgregar = (Authority)actual.darElemento();		
			
			
				for (int i = 0; i < arrAll.size() ; i++)
				{
					JsonObject comparado = (JsonObject)arrAll.get(i);
					
					JsonElement localComparado = comparado.get("Local-Authority");
					int a�o = 0;
					
					if (localComparado.getAsString().equals(paraAgregar.getLocalAuthority()))
					{
					
						while (a�o < fecha)					
						{
							JsonElement a�oComparado = comparado.get(a�o+"");
							
							if(a�oComparado != null)
							{
								
								int a�oValorAll = a�oComparado.getAsInt();
								A�o a�oAllAgregar = new A�o(Integer.toString(a�o));
								a�oAllAgregar.insertarFA(a�oValorAll);
								paraAgregar.getListaA�osTraffic().agregar(a�oAllAgregar);
							}			
							a�o ++;
						}
					}
					
				}
			actual = actual.darSiguiente();
		}
		
		actual = local.darPrimero();
		
		while(actual != null)
		{
			Authority authorityActual = (Authority)actual.darElemento();
			Nodo barrerA�osTra = authorityActual.getListaA�osTraffic().darPrimero();
			
			while(barrerA�osTra != null)
			{
				A�o modi = ((A�o)barrerA�osTra.darElemento());
				String year = modi.getNumeroA�o();
				
				
				for (int i = 0; i < arrCars.size(); i++)
				{
					JsonObject comparado = (JsonObject)arrCars.get(i);
					JsonElement authorityComparado =  comparado.get("Local-Authority");
					String nombreAC = authorityComparado.getAsString();
					
					int a�o = 0;
					
					if (nombreAC.equals(authorityActual.getLocalAuthority()))
					{
						
						
						while (a�o < fecha)
						{
							if(Integer.parseInt(year) == a�o)
							{
								JsonElement a�oComparado = comparado.get(a�o+"");
								if(a�oComparado != null)
								{
									
									int a�oValorCars = a�oComparado.getAsInt();
									modi.insertarFC(a�oValorCars);
									
										
								}
							}
							a�o ++;
						}
					}
				}
				barrerA�osTra = barrerA�osTra.darSiguiente();
			}
			actual = actual.darSiguiente();
		}
		
		actual = local.darPrimero();
		
		while(actual != null)
		{
			Nodo elA�o = ((Authority)actual.darElemento()).getListaA�osTraffic().darPrimero();
			
			while(elA�o != null)
			{
				
				A�o b = ((A�o)elA�o.darElemento());
				
				for(int i = 0; i < arrTrafficWeight.size(); i++)
				{
					
					JsonObject comparado = (JsonObject)arrTrafficWeight.get(i);
					int a�oVer = 0;
					while(a�oVer < fecha)
					{
						JsonElement elem = comparado.get(""+a�oVer);
						
						if(elem != null)
						{
							double valorTrafficPeso = elem.getAsDouble();
							
							if(a�oVer == Integer.parseInt(b.getNumeroA�o()))
							{
								
								b.insertarPeso(valorTrafficPeso);
								
							}
						}
						a�oVer++;
					
					}		
				}
				elA�o = elA�o.darSiguiente();
			}
			actual = actual.darSiguiente();
		}
		prioridad(true);		
	}
	
	/**
	 * Lista que muestra las ubiaciones de las local authority de acuerdo a su prioridad en el heap.
	 */
	
	public void ver_B()
	{	
		//Heapsort
		
		Authority arreglo[] = new Authority[heapB.size()];
		
		HeapMax<Authority> copiaHeapB = heapB;
		int contador=0;
		while (!copiaHeapB.isEmpty())
		{			
			arreglo[contador]=(Authority)(copiaHeapB.delMax());
			
			contador ++;
		}
					
		
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println("INFORMACION DE PARTE B:  ");
		System.out.println("-------------------------------------------------------------------------------------------");

		for (int i = 0; i < arreglo.length; i++) 
		{
			Authority d = (Authority)arreglo[i];
			System.out.println(d.getLocalAuthority() +" --  Prioridad:  "+ d.getPrioridadB() + " Code: " + d.getCode());
			Nodo a = d.getListaA�os().darPrimero();
			while(a != null)
			{
				String n = ((A�o)a.darElemento()).getNumeroA�o();
				double c = ((A�o)a.darElemento()).getPeso(); 
				double h = ((A�o)a.darElemento()).getValorB();
				
				System.out.println(n+" - - "+h+" - - "+c);
				a = a.darSiguiente();
			}
			System.out.println(" - - ");

		}
	}
	
	public void ver_A()
	{
		//Heapsort
		
		Authority arreglo[] = new Authority[heapA.size()];
		
		HeapMax<Authority> copiaHeapA = heapA;
		int contador=0;
		while (!copiaHeapA.isEmpty())
		{
			
			arreglo[contador]=(Authority)(copiaHeapA.delMax());
			
			contador ++;
		}
		
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println("INFORMACION DE PARTE A");
		System.out.println("-------------------------------------------------------------------------------------------");

		for (int i = 0; i < arreglo.length; i++) 
		{
			Authority d = (Authority)arreglo[i];
			System.out.println(d.getLocalAuthority() +" -- Prioridad: "+d.getPrioridadA() + " Code: " + d.getCode());
			Nodo a = d.getListaA�os().darPrimero();
			while(a != null)
			{
				String n = ((A�o)a.darElemento()).getNumeroA�o();
				double c = ((A�o)a.darElemento()).getPeso(); 
				double h = ((A�o)a.darElemento()).getValorA();
				
				System.out.println(n+" - - "+h+" - - "+c);
				a = a.darSiguiente();
			}
			System.out.println(" - - ");
		}
	}

	public void verAllCars()
	{
		Nodo actual = local.darPrimero();		
		
		
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println("INFORMACION ALL Y CARS");
		System.out.println("-------------------------------------------------------------------------------------------");
		
		while (actual != null)
		{
		
			Authority d = (Authority)actual.darElemento();
			System.out.println(d.getLocalAuthority() + " Code: " + d.getCode());
			Nodo a = d.getListaA�osTraffic().darPrimero();
			while(a != null)
			{
				String n = ((A�o)a.darElemento()).getNumeroA�o();
				double c = ((A�o)a.darElemento()).getPeso(); 
				double h = ((A�o)a.darElemento()).getFA();
				double j = ((A�o)a.darElemento()).getFC();
				
				System.out.println(n+" - - "+c+" - - "+h+" - - "+j);
				a = a.darSiguiente();
			}
			System.out.println(" - - ");
			
			actual= actual.darSiguiente();
		}
	}
	
	/**
	 * Inserta la prioridad para cada local authority
	 */
	public void prioridad(boolean pLeer)
	{
		if (pLeer)
		{			
			Nodo actual = local.darPrimero();

			while(actual != null)
			{
				Nodo nod = ((Authority)actual.darElemento()).getListaA�os().darPrimero();
				double mul = 0.0;
				double sum = 1.0;
				

				while(nod != null)
				{
					double pes = ((A�o)nod.darElemento()).getPeso();
					double val = ((A�o)nod.darElemento()).getValorA();
					mul += pes*val;					
					
				
					

					nod = nod.darSiguiente();
				}
				
				double prioriNueva = mul/sum;
				
				
				((Authority)actual.darElemento()).insertarPrioridadA(prioriNueva);
				actual = actual.darSiguiente();
			}
			
			Nodo insercion = local.darPrimero();
			
			while(insercion != null)
			{
				Authority agre = ((Authority)insercion.darElemento());
				agre.cambiarAB(false);

				heapA.insert(agre);
				insercion = insercion.darSiguiente();
			}
		}
		else if (!pLeer)
		{			
			Nodo actual = local.darPrimero();
			
			while(actual != null)
			{
				Nodo nod = ((Authority)actual.darElemento()).getListaA�os().darPrimero();
				double mul = 0.0;
				double sum = 1.0;

				while(nod != null)
				{
					double pes = ((A�o)nod.darElemento()).getPeso();
					double val = ((A�o)nod.darElemento()).getValorB();
					mul += pes*val;
					

					nod = nod.darSiguiente();
				}

				double prioriNueva = mul/sum;
				((Authority)actual.darElemento()).insertarPrioridadB(prioriNueva);
				actual = actual.darSiguiente();
			}
			
			Nodo insercion = local.darPrimero();
			
			while(insercion != null)
			{
				Authority agre = ((Authority)insercion.darElemento());
				agre.cambiarAB(true);
				heapB.insert(agre);
				insercion = insercion.darSiguiente();
			}
		}
	}
	
	/**
	 * Guarda la informacion en memoria y crea el heap para los local authority con mayor prioridad
	 * @param pRutaChild Ruta del archivo para la informacion de ni�os accidentados entre 0-15 a�os de edad.
	 * @param pRutaWeight Ruta del archivo que contiene la informacion del peso de cada a�o.
	 * @throws Exception Excepcion si no se puede leer los archivos.
	 */
	public void leerInformacion_B(String pRutaChild, String pRutaWeight) throws Exception 
	{		
		Calendar cal = Calendar.getInstance();
		int fecha = cal.get(Calendar.YEAR);

		JsonParser child = new JsonParser();
		JsonParser boroughsWeight = new JsonParser();

		JsonArray arr= (JsonArray) child.parse(new FileReader(pRutaChild));
		JsonArray arrWeight= (JsonArray) boroughsWeight.parse(new FileReader(pRutaWeight));

		Nodo actual = local.darPrimero();

		
		while(actual != null)
		{
			String nom = ((Authority)actual.darElemento()).getLocalAuthority();
			
			for (int i = 0; i < arr.size(); i++) 
			{
				JsonObject obj = (JsonObject)arr.get(i);
				String nombArch = obj.get("LocalAuthority").getAsString();
				if(nom.equals(nombArch))
				{
					int j = 2004;
					while(j<=fecha )
					{
						JsonElement ele = obj.get(""+j);

						if(ele != null)
						{
							int p2 = ele.getAsInt();
							
							Nodo nodoA�o = ((Authority)actual.darElemento()).getListaA�os().darPrimero();
							
							while (nodoA�o != null)
							{	
								if (((A�o)nodoA�o.darElemento()).getNumeroA�o().equals(Integer.toString(j)))
								{
									((A�o)nodoA�o.darElemento()).insertarValorB(p2);
								}	
								nodoA�o = nodoA�o.darSiguiente();
							}
						}
						j = j+1;
					}
				}
			}

			actual = actual.darSiguiente();
		}

		
		
		
		prioridad(false);
		
		
	}
	
	public void armarTablasA()
	{
		
		
		Nodo actual = local.darPrimero();
		
		while (actual != null)
		{
			Authority authorityActual = (Authority)actual.darElemento();
			String codigoActual = authorityActual.getCode();			
			
			TablaHashLP<String,Integer> tablaLPActual = new TablaHashLP<String, Integer>();
			
			Nodo aActual = authorityActual.getListaA�os().darPrimero();
			
			while (aActual != null)
			{
				A�o a�oActual = (A�o) aActual.darElemento();
				String a�o = a�oActual.getNumeroA�o();
				int valor = a�oActual.getValorA();
				
				tablaLPActual.put(a�o, valor);
				
				aActual = aActual.darSiguiente();
			}						
			
			tablaSCA.put(codigoActual, tablaLPActual);
			
			actual = actual.darSiguiente();
		}
		
	}
	
	public void armarTablasB()
	{
		
		
		Nodo actual = local.darPrimero();
		
		while (actual != null)
		{
			Authority authorityActual = (Authority)actual.darElemento();
			String codigoActual = authorityActual.getCode();			
			
			TablaHashLP<String,Integer> tablaLPActual = new TablaHashLP<String, Integer>();
			
			Nodo aActual = authorityActual.getListaA�os().darPrimero();
			
			while (aActual != null)
			{
				A�o a�oActual = (A�o) aActual.darElemento();
				String a�o = a�oActual.getNumeroA�o();
				int valor = a�oActual.getValorB();
				
				tablaLPActual.put(a�o, valor);
				
				aActual = aActual.darSiguiente();
			}						
			
			tablaSCB.put(codigoActual, tablaLPActual);
			
			actual = actual.darSiguiente();
		}
	}
	public void armarArbolLA()
	{
		Nodo actual = local.darPrimero();		
		
		while (actual != null)
		{
			Authority autorithyActual = (Authority)actual.darElemento();
			
			String nombreAuthorityActual = autorithyActual.getLocalAuthority();
			String codigoAuthorityActual = autorithyActual.getCode();
			
			arbolLA.put(nombreAuthorityActual, codigoAuthorityActual);
		
			actual = actual.darSiguiente();
		}
	}
	
	public void armarArbolCODA()
	{
		Nodo actual = local.darPrimero();		
		
		while (actual != null)
		{
			Authority autorithyActual = (Authority)actual.darElemento();			
			
			String codigoAuthorityActual = autorithyActual.getCode();
			
			TablaHashSC<String,Integer> tablaCars = new TablaHashSC<String,Integer>();
			TablaHashLP<String,Integer> tablaAll = new TablaHashLP<String,Integer>();
			
			
			Nodo aActual = autorithyActual.getListaA�osTraffic().darPrimero();
			
			while (aActual != null)
			{
				
				A�o a�oActual = (A�o) aActual.darElemento();
				
				String a�o = a�oActual.getNumeroA�o();
				
				int cars = a�oActual.getFC();
				
				int all = a�oActual.getFA();
				
				
				tablaCars.put(a�o, cars);
				
				tablaAll.put(a�o, all);
				
				
				aActual = aActual.darSiguiente();
				
			}
			
			
			ArrayList tablas = new ArrayList();
			
			tablas.add(tablaAll);
			tablas.add(tablaCars);
			
			arbolCODA.put(codigoAuthorityActual, tablas);
		
			actual = actual.darSiguiente();
		}
	}
	
	public void armarArbolCODB()
	{
		Nodo actual = local.darPrimero();		
		
		while (actual != null)
		{
			Authority autorithyActual = (Authority)actual.darElemento();			
			
			String codigoAuthorityActual = autorithyActual.getCode();
			
			TablaHashSC<String,Integer> tablaAll = new TablaHashSC<String,Integer>();
			TablaHashLP<String,Integer> tablaCars = new TablaHashLP<String,Integer>();
			
			Nodo aActual = autorithyActual.getListaA�osTraffic().darPrimero();
			
			while (aActual != null)
			{
				A�o a�oActual = (A�o) aActual.darElemento();
				String a�o = a�oActual.getNumeroA�o();
				int cars = a�oActual.getFC();
				int all = a�oActual.getFA();
				
				tablaCars.put(a�o, cars);
				tablaAll.put(a�o, all);
				
				aActual = aActual.darSiguiente();
			}
			
			ArrayList tablas = new ArrayList();
			
			tablas.add(tablaAll);
			tablas.add(tablaCars);
			
			arbolCODB.put(codigoAuthorityActual, tablas);
		
			actual = actual.darSiguiente();
		}
	}
	
	public int buscarValorA(String pCodigo, String pA�o)
	{
		
		int rpta = 0;
		
		TablaHashLP<String,Integer> tablaA�os = (TablaHashLP<String,Integer>) tablaSCA.get(pCodigo);
		
		
		rpta = tablaA�os.get(pA�o);		
		
		return rpta;
	}
	
	public int buscarValorB(String pCodigo, String pA�o)
	{
		int rpta = 0;
		
		TablaHashLP<String,Integer> tablaA�os = (TablaHashLP<String,Integer>) tablaSCB.get(pCodigo);
		
		rpta = tablaA�os.get(pA�o);		
		
		return rpta;
	}
	
	public String buscarValorTrafficA(String pLocal, String pA�o)
	{
		String respuesta = "";
		
		String codigo = arbolLA.get(pLocal);
		
		ArrayList tablas = new ArrayList();
		
		tablas = arbolCODA.get(codigo);
		
		TablaHashSC<String, Integer> tablaCars = (TablaHashSC<String, Integer>) tablas.get(1);
		TablaHashLP<String, Integer> tablaAll = (TablaHashLP<String, Integer>) tablas.get(0);
		
		int cars = tablaCars.get(pA�o);
		int all = tablaAll.get(pA�o);
		
		respuesta = "Los flujos de carros y de todos los veh�culos de "+pLocal+" en el a�o " + pA�o + " son: " + cars + " y " + all + " respectivamente.\n";
		
		
		
		return respuesta;
	}
	
	public String buscarValorTrafficB(String pLocal, String pA�o)
	{
		String respuesta = "";
		
		String codigo = arbolLA.get(pLocal);
		
		ArrayList tablas = new ArrayList();
		
		tablas = arbolCODB.get(codigo);
		
		TablaHashLP<String, Integer> tablaCars = (TablaHashLP<String, Integer>) tablas.get(1);
		
		TablaHashSC<String, Integer> tablaAll = (TablaHashSC<String, Integer>) tablas.get(0);
		
		
		int cars = tablaCars.get(pA�o);
		int all = tablaAll.get(pA�o);
		
		respuesta = "Los flujos de carros y de todos los veh�culos de "+pLocal+" en el a�o " + pA�o + " son: " + cars + " y " + all + " respectivamente.\n";
		
		
		
		return respuesta;
	}
	
	
	
	
	

	
	
//	public static void main(String[] args) 
//	{
//		Principal b = new Principal();
//	}
}
