package Mundo;

import Estructuras.Lista;

public class Authority implements Comparable<Authority>
{
	private boolean prioridad;
	private double prioridadA;
	private double prioridadB;
	private String localAuthority;
	private Lista<A�o> a�os;
	private Lista<A�o> a�osTraffic;
	private String code;
	
	public Authority(String pNombre, String pCode)
	{
		localAuthority = pNombre;
		a�os = new Lista<A�o>();	
		a�osTraffic = new Lista<A�o>();
		code = pCode;		
	}
	
	public double getPrioridadA()
	{
		return prioridadA;
	}
	
	public double getPrioridadB()
	{
		return prioridadB;
	}
	
	public String getLocalAuthority()
	{
		return localAuthority;
	}
	
	public Lista<A�o> getListaA�os()
	{
		return a�os;
	}
	
	public Lista<A�o> getListaA�osTraffic()
	{
		return a�osTraffic;
	}
	
	public String getCode()
	{
		return code;
	}
	
	public void insertarPrioridadA(double pPrior)
	{
		prioridadA = pPrior;
	}
	
	public void insertarPrioridadB(double pPrior)
	{
		prioridadB = pPrior;
	}
	
	public void cambiarAB(boolean pPriori)
	{
		prioridad = pPriori;
	}
	
	public int compareTo(Authority pNodo) 
	{
		int res = 0;
		
		if(!prioridad)
		{
			if(prioridadA > pNodo.getPrioridadA())
			{
				res = 1;
			}
			if(prioridadA < pNodo.getPrioridadA())
			{
				res = -1;
			}
		}
		else
		{
			if(prioridadB > pNodo.getPrioridadB())
			{
				res = 1;
			}
			if(prioridadB < pNodo.getPrioridadB())
			{
				res = -1;
			}
		}
		return res;
	}
	
	
	
	
}
