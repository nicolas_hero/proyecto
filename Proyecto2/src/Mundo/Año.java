package Mundo;

public class A�o 
{
	private String numeroA�o;
	private int valorB;
	private int valorA;
	private double peso;
	private int flujoAll;
	private int flujoCars;
	
	public A�o(String pNombre)
	{
		numeroA�o = pNombre;
		valorA = 0;
		valorB = 0;
		peso = 0;
		flujoAll = 0;
		flujoCars = 0;
	}
	
	public String getNumeroA�o()
	{
		return numeroA�o;
	}
	
	public int getValorA()
	{
		return valorA;
	}
	
	public int getValorB()
	{
		return valorB;
	}
	
	public double getPeso()
	{
		return peso;
	}
	

	
	public void insertarPeso(double pPeso)
	{
		peso = pPeso;
	}
	

	public void insertarFA(int pFlujoAll)
	{
		flujoAll = pFlujoAll;
	}
	
	public void insertarFC(int pFlujoCars)
	{
		flujoCars = pFlujoCars;
	}
	
	public int getFA()
	{
		return flujoAll;
	}
	
	public int getFC()
	{
		return flujoCars;
	}
	
	public void insertarValorA(int pValor)
	{
		valorA = pValor;
	}
	
	public void insertarValorB(int pValor)
	{
		valorB = pValor;
	}
}
