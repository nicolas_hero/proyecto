package Cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class PruebasParteC {
	
	BufferedWriter escritor;
	Scanner lector;
	
	//TODO: Declarar objetos de la parte A
	
    //TODO: Declarar objetos de la parte B
    
	
	public PruebasParteC(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}
	
	public void pruebas() {
		int opcion = -1;
		
		//TODO: Inicializar objetos de la parte A
        //TODO: Inicializar objetos de la parte B
	
		
		long tiempoDeCarga = System.nanoTime();
        //TODO: Cargar informacion de la parte A
        //TODO: Cargar informacion de la parte B

		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Requerimiento 1C. \n");
				escritor.write("2: Requerimiento 2C - 1. \n");
				escritor.write("3: Requerimiento 2C - 2. \n");
				escritor.write("4: Requerimiento 3C. \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: c1(); break;
				case 2: c21(); break;
				case 3: c22(); break;
				case 4: c3(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void c1() throws IOException{
		long tiempo = System.nanoTime();
		//TODO:Generar y obtener el conjunto de datos requeridos para realizar la grafica solicitada.
		//Para graficar PUEDE hacer uso de la  libreria  JFREECHART disponible en "http://www.jfree.org/jfreechart/"
		//Si opta por usar JFREECHART puede hacer uso (como guia) del ejemplo que se presenta en el siguiente link "https://www.tutorialspoint.com/jfreechart/jfreechart_line_chart.htm" 
		//Recuerde que en el eje de abcisas se tendr� el # de ciudades (N) y en el eje de ordenadas se tendr�n los tiempos de ejecuci�n en milisegundos o nanosegundos 
		//Independientemente de la forma en que grafique, nombre tanto la grafica como sus ejes
		escritor.write("La soluci�n est� en el archivo 'PARTE C' de la carpeta 'doc' del proyecto\n");
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();		
	}
	
	private void c21() throws IOException{
		long tiempo = System.nanoTime();
		//TODO:Generar y obtener el conjunto de datos requeridos para realizar la grafica solicitada.
		//Para graficar PUEDE hacer uso de la  libreria  JFREECHART disponible en "http://www.jfree.org/jfreechart/"
		//Si opta por usar JFREECHART puede hacer uso (como guia) del ejemplo que se presenta en el siguiente link "https://www.youtube.com/watch?v=qVkqyuTiWCc" 
		//Recuerde que en el eje de abcisas se tendr� los metodos para indexar ("Separate Chaining""Linear Probing") y en el eje de ordenadas se tendr�n los tiempos de ejecuci�n en milisegundos o nanosegundos 
		//Independientemente de la forma en que grafique, nombre tanto la grafica como sus ejes
		escritor.write("La soluci�n est� en el archivo 'PARTE C' de la carpeta 'doc' del proyecto\n");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();		
	}
	
	private void c22() throws IOException{
		long tiempo = System.nanoTime();
		//TODO:Generar y obtener el conjunto de datos requeridos para realizar la grafica solicitada.
		//Para graficar PUEDE hacer uso de la  libreria  JFREECHART disponible en "http://www.jfree.org/jfreechart/"
		//Si opta por usar JFREECHART puede hacer uso (como guia) del ejemplo que se presenta en el siguiente link "https://www.youtube.com/watch?v=qVkqyuTiWCc" 
		//Independientemente de la forma en que grafique, nombre tanto las grafica como sus ejes
		escritor.write("La soluci�n est� en el archivo 'PARTE C' de la carpeta 'doc' del proyecto\n");
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();	
		
	}
	private void c3() throws IOException{
		long tiempo = System.nanoTime();
		//TODO:Haga la comparacion pertienente, explique los resultados obtenidos
		//y concluya (Imprima en pantalla lo que compara, explique en la misma impresion
		//en que consiste dicha comparaci�n e imprima su conclusi�n)
		escritor.write("La soluci�n est� en el archivo 'PARTE C' de la carpeta 'doc' del proyecto\n");
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();	
		
	}
	
}