package Test;
import junit.framework.TestCase;
import Estructuras.HeapMax;
import Mundo.Authority;
import Mundo.Principal;

public class BaseTest extends TestCase
{
	public static final String BOROUGH_CHILD = "data/road-casualties-severity-borough-child.json";
	public static final String BOROUGH_YEARS_WEIGHT = "data/road-casualties-severity-borough-years-weight.json";
	public static final String BOROUGH = "data/road-casualties-severity-borough.json";
	public static final String BOROUGH_ALL = "data/traffic-flow-borough-all.json";
	public static final String BOROUGH_CARS = "data/traffic-flow-borough-cars.json";
	public static final String TRAFFIC_BOROUGH_YEARS_WEIGHT = "data/traffic-flow-borough-years-weight.json";

	private Principal base;

	public void setupEscenario1()
	{
		base = new Principal();
	}

	public void testConstruccionHeap_A()
	{
		setupEscenario1();
		try
		{
			base.leerInformacion_A(BOROUGH, BOROUGH_YEARS_WEIGHT, BOROUGH_ALL, BOROUGH_CARS, TRAFFIC_BOROUGH_YEARS_WEIGHT);
			
			HeapMax<Authority> correc = base.darHeapMax_A();
			
			int si = correc.size();
			assertEquals(98,si);
			
			Authority a = correc.delMax();
			assertEquals("Se elimina y disminuye el tama�o del heap correctamente",correc.size()+1, si);
			assertTrue("El heap no deberia estar vacio",!correc.isEmpty());
			
			assertNotSame("El maximo fue eliminado correctamente",correc.elPrimero(), a);
			
		}
		catch(Exception e )
		{
			fail("Hubo problemas leyendo los archivos");
		}
	}
	public void testConstruccionHeap_B()
	{
		setupEscenario1();
		try
		{
			base.leerInformacion_B(BOROUGH_CHILD, BOROUGH_YEARS_WEIGHT);
			
			HeapMax<Authority> correc = base.darHeapMax_B();
			
			assertTrue("El heap no deberia estar vacio",!correc.isEmpty());
			assertEquals(2472.96, correc.delMax().getPrioridadB(), 0.1);
			int si = correc.size();
			Authority a = correc.delMax();
			assertEquals("Se elimina y disminuye el tama�o del heap correctamente",correc.size()+1, si);
			
		}
		catch(Exception e )
		{
			fail("Hubo problemas leyendo los archivos");
		}
	}
}
