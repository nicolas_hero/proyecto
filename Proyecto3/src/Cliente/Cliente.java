package Cliente;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

import Mundo.Aerolinea;
import Mundo.Base;
import Mundo.Ciudad;
import Mundo.Viaje;

/**
 *
 * @author Samuel S. Salazar, Gustavo Alegria, Fernando De la Rosa
 */
public class Cliente 
{
	private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	private Base principal;		
	

	// TODO
	// Definir el atributo(s) del modelo del mundo (incluye el grafo(s) dirigido(s) con los datos de ciudades y vuelos)

	/** ***************************************************************************************************
	 * 								OPCIONES DEL MENU 
	 * ***************************************************************************************************
	 * 	- N: Numero total de opciones
	 *  - Las opciones se organizan en un arreglo de entradas que van de 0..N-1
	 *  - La opcion i (0..N-1) corresponde al requerimiento i+1 del menu.
	 * A continuacion se describe cada dato de entrada para cada requerimiento i (1..N-1):
	 * opciones[i][0] = Nombre de la opcion/requerimiento 
	 * 	 	+se define j como un entero que va de 1...p donde p es el numero de parametros de la opcion.
	 * 		+ Estos parametros (de tipo String) seran pasados en forma de arreglo al metodo que resuelve el requerimiento
	 * 		+ El metodo que resuelve el requerimiento debe llamarse req<i>(String[] params)
	 * opciones[i][j] = entrada/parametro j de la opcion (con j > 0)
	 * 
	 */
	
	private static  String[] opciones =
			// Req 1. Caso 1. En caso que se quiera leer al principio los vuelos del catalogo (definidos en archivo) usar la siguiente definicion:
			//		{"req1","Cargar un catalogo de vuelos desde archivo de datos","ruta archivo"}, //R1
			// Req 1. Caso 2. En caso que se quiera iniciar el catalogo de vuelos vacio usar la siguiente definicion:
		{"Req1 - Crear un catalogo de vuelos (vacio)", 
		"Req2 - Agregar una aerolinea al catalogo de vuelos, Nombre aerolinea", 
		"Req3 - Eliminar una aerolinea del catalogo de vuelos, Nombre aerolinea",
		"Req4 - Agregar y eliminar ciudades autorizadas para realizar vuelos autorizados,Agregar o Eliminar?,Nombre Ciudad", 
		"Req5 - Agregar un vuelo al catalogo de vuelos, # de vuelo, Aerolinea, Ciudad origen, Ciudad destino,Hora de salida, Hora de llegada, Tipo de avion, Cupo del vuelo, Dias de operacion",
		"Req6 - Calcular y actualizar las tarifas de los vuelos",
		"Req7 - Informar los conjuntos de ciudades que se pueden comuniacar entre si pero que no tienen comunicacion con el resto del pais sin importar las aerolinea",
		"Req8 - Informar los conjuntos de ciudades que se pueden comunicar entre si pero que no tienen comunicacion con el resto del pais para cada aerolinea",
		"Req9 - Calcular e imprimir el MST para vuelos nacionales, a partir de una ciudad especifica, utilizando el tiempo del vuelo como peso de los arcos, Ciudad origen",
		"Req10 - Calcular e imprimir el MST para vuelos nacionales de una aerolinea particular, a partir de una ciudad especifica, utilizando el costo de los vuelos como peso de los arcos, Nombre Aerolinea, Ciudad origen",
		"Req11 - Calcular e imprimir el MST  a partir de una ciudad especifica y de un dia particular, sin importar cambios de aerolinea en el viaje, Ciudad origen, Dia partida",
		"Req12 - Calcular e imprimir el itinerario de costo minimo para cada aerolinea,Ciudad origen,Ciudad destino,Dia partida",
		"Req13 - Calcular e imprimir el itinerario de costo minimo para diferentes aerolineas,Ciudad origen,Ciudad destino,Dia partida",
		"Req14 - Calcular e imprimir la ruta de costo minimo para ir a todas las otras ciudades cubiertas por una aerolinea,Ciudad,Nombre aerolinea",
		"Req15 - Calcular e imprimir la ruta de menor tiempo para ir a todas las otras ciudades cubiertas por una aerolinea,Ciudad,Nombre aerolinea",
		"Req16 - Saliendo de una ciudad particular y en un d�a espec�fico, calcule e imprima la ruta optima (por precio) para visitar todas las otras ciudades cubiertas por una aerol�nea",
		"Req17 -  Calcule la ciudad, el d�a y la hora en la que un viajero deber�a comenzar su viaje, de tal forma que logre visitar el mayor n�mero de ciudades posibles con el menor costo posible, en vuelos servidos por una misma aerol�nea",
		"Req18 -  Dado un d�a y una hora espec�ficos, calcule e imprima la ruta mas larga (mayor n�mero de ciudades) que puede hacer un viajero en un aerol�nea de su predilecci�n. Indique claramente cuando haya cambio de d�a en el itinerario.",
		"Req19 - Dado un d�a y una hora espec�ficos, calcule e imprima la ruta mas larga (mayor n�mero de ciudades) que puede hacer un viajero, as� deba conectar con vuelos de diferentes aerol�neas.",
		"Req20 - Un viajero debe salir de una ciudad particular en un d�a y hora espec�ficos y debe visitar un conjunto determinado de ciudades.  Se sabe que el viajero debe estar en cada ciudad al menos 2 horas.  No existe un orden espec�fico para las visitas.",
		"Salir"};
	
	//TODO Agregar los requerimientos opcionales en caso de querer realizar el bono
	//Nota: los metodos req16, ..., req20 ya aparecen definidos y falta completar segun la documentacion;

	/**
	 * Constructor del cliente que prueba los requerimientos del proyecto 3
	 */
	public Cliente()
	{
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal = new Base(principal.ARCHIVO_DATOS, principal.ARCHIVO_CPM);
		System.out.println("Tiempo de lectura de archivo y creaci�n de estructuras de datos: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		// TODO
		// Inicializar el atributo(s) del modelo del mundo
		//principal = new Base(Base.ARCHIVO_DATOS, Base.ARCHIVO_CPM );
		try
		{
			menu();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void menu() throws NumberFormatException, IOException
	{
		opcionesReq();
		System.out.println();
		boolean pararCiclo = true;
		while(pararCiclo)
		{
			int opcionUsuario = Integer.parseInt(in.readLine());
			if(opcionUsuario == 1)
			{
				req1();
			}
			else if(opcionUsuario == 2)
			{
				req2();
			}
			else if(opcionUsuario == 3)
			{
				req3();
			}
			else if(opcionUsuario == 4)
			{
				req4();
			}
			else if(opcionUsuario == 5)
			{
				req5();
			}
			else if(opcionUsuario == 6)
			{
				req6();
			}
			else if(opcionUsuario == 7)
			{
				req7();
			}
			else if(opcionUsuario == 8)
			{
				req8();
			}
			else if(opcionUsuario == 9)
			{
				req9();
			}
			else if(opcionUsuario == 10)
			{
				req10();
			}
			else if(opcionUsuario == 11)
			{
				req11();
			}
			else if(opcionUsuario == 12)
			{
				req12();
			}
			else if(opcionUsuario == 13)
			{
				req13();
			}
			else if(opcionUsuario == 14)
			{
				req14();
			}
			else if(opcionUsuario == 15)
			{
				req15();
			}
			else if(opcionUsuario == 16)
			{
				req16();
			}
			else if(opcionUsuario == 17)
			{
				req17();
			}
			else if(opcionUsuario == 18)
			{
				req18();
			}
			else if(opcionUsuario == 19)
			{
				req19();
			}
			else if(opcionUsuario == 20)
			{
				req20();
			}
			else if(opcionUsuario == 21)
			{
				pararCiclo = false;
			}
			menu();
		}
	}

	public void opcionesReq()
	{
		System.out.println("---------------------------------------------------------------------------------------------");
		System.out.println("------------------------------ API AERONAUTICA CIVIL ----------------------------------------");
		System.out.println("---------------------------------------------------------------------------------------------");

		System.out.println();
		System.out.println();

		System.out.println("Escoja la opcion de la cual quiere obtener informaci�n. Dependiendo del numero que precede la opcion");
		System.out.println();
		System.out.println();
		for (int i = 0; i < opciones.length; i++)
		{
			String op = opciones[i];

			System.out.println((i+1)+". "+ op);
		}
	}

	/**
	 * Metodo de terminacion del cliente
	 * @param params (ninguno)
	 */
	public void exit(String[] params)
	{
		System.out.println("exit: parametros"+Arrays.toString(params));
		System.out.println(">> Adios");
		System.exit(0);
	}

	/**
	 * Metodo encargar de crear (caso 1) o cargar (caso 2) un catalogo de vuelos
	 * @param params 
	 * @throws IOException 
	 */
	public  void req1() throws IOException
	{		
		//TODO
		// Completar segun documentacion del requerimiento
		
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal.leer();
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();
	}

	/**
	 * Agrega una aerolinea al catalogo
	 * @param params
	 * params[0] = Nombre de la aerolinea
	 * @throws IOException 
	 */
	public void req2() throws IOException
	{
		
		System.out.println("Porfavor introduzca el nombre de la erolinea que desea agregar al catalogo");
		System.out.print("-->  ");
		String nombreAerolinea = in.readLine();

		System.out.println("Porfavor introduzca el costo de la erolinea que desea agregar");
		System.out.print("-->  ");
		double costo  = Double.parseDouble(in.readLine());

		System.out.println("Porfavor introduzca el numero maximo de sillas de la erolinea que desea agregar");
		System.out.print("-->  ");
		int maxSillas = Integer.parseInt(in.readLine());

		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		boolean pudo = principal.agregarAerolineaCatalogo(nombreAerolinea, costo, maxSillas);

		if(pudo)
		{
			System.out.println();
			System.out.println("*Su aerolinea con nombre: "+nombreAerolinea+", con un costo basico de: "+costo+" y numero de sillas de:  "+ maxSillas+" se agrego correctamente*");
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
			
		}
		else
		{
			
			System.out.println();
			System.out.println("*No fue posible agrgar su aerolinea*");
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		try 
		{
			String vol = in.readLine();
		} 
		catch (IOException e)		
		{
			System.out.println(e.getMessage());
		}
	}
	/**
	 * Elimina una aerolinea del catalogo
	 * @param params
	 * params[0] = Nombre de la aerolinea
	 * @throws IOException 
	 */
	public void req3() throws IOException
	{
		
		System.out.println("Porfavor introduzca el nombre de la erolinea que desea eliminar del caralogo");
		System.out.print("-->  ");
		String nombreAerolinea = in.readLine();
		
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		boolean res = principal.eliminarAerolineaCatalogo(nombreAerolinea);

		if(res)
		{
			System.out.println();
			System.out.println("*Su aerolinea con nombre: "+nombreAerolinea+" se ha elimando correctamente*");
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}
		else
		{
			System.out.println();
			System.out.println("*No fue posible eliminar la aerolinea, NO EXISTE EN EL CATALOGO*");
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}

		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();


	}

	/**
	 * Agregar y eliminar ciudades autorizadas para realizar vuelos autorizados
	 * @param params
	 * params[0] = Agregar o eliminar una ciudad
	 * params[1]= Nombre ciudad autorizada
	 * @throws IOException 
	 */
	public void req4() throws IOException
	{

		System.out.println("Escriba el numero de la opcion que desea realizar");
		System.out.println("--- > 1. AGREGAR ciudad autorizada");
		System.out.println("--- > 2. ElIMINAR ciudad autorizada");


		int resp = Integer.parseInt(in.readLine());

		if(resp == 1)
		{
			System.out.println();
			System.out.println("------- > Escriba el nombre de la ciudad que desea agregar");
			System.out.print("-->  ");
			String ciudadA = in.readLine();
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			boolean res = principal.agregarCiudadCatalogo(ciudadA); 
			if(res)
			{
				System.out.println();
				System.out.println("------- > LA CIUDAD SE AGREG� CORRECTAMENTE");
				System.out.println();
				System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
			}
			else
			{
				System.out.println("NO SE PUDO AGREGAR LA CIUDAD");
				System.out.println();
				System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
			}
			System.out.println();
			System.out.println("Oprima cualquier letra");
			System.out.print("-->  ");
			String vol = in.readLine();
		}
		if(resp == 2)
		{
			System.out.println();
			System.out.println("------- > Escriba el nombre de la ciudad que desea eliminar");
			System.out.print("-->  ");
			String ciudadA = in.readLine();
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			boolean res = principal.eliminarCiudadCatalogo(ciudadA); 
			if(res)
			{
				System.out.println();
				System.out.println("------- > LA CIUDAD SE ELIMIN� CORRECTAMENTE");
				System.out.println();
				System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
			}
			else
			{
				System.out.println("NO SE PUDO ELIMINAR LA CIUDAD, NO EXISTE");
				System.out.println();
				System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
			}
			System.out.println();
			System.out.println("Oprima cualquier letra");
			System.out.print("-->  ");
			String vol = in.readLine();
		}
	}

	/**
	 * Agregar un vuelo al catalogo de vuelos
	 * @param params
	 * params[0] = # de vuelo
	 * params[1]= Aerolinea
	 * params[2]= Ciudad origen
	 * params[3]= Ciudad destino
	 * params[4] = Hora de salida
	 * params[5] = Hora de llegada
	 * params[6] = Tipo de avion
	 * params[7] = Cupo del vuelo
	 * params[8] = Dias de operacion
	 * @throws IOException 
	 */
	public void req5() throws IOException
	{
	
		System.out.println("Introduzca el id del vuelo que desea agregar");
		System.out.print("-->  ");
		double pNombre = Double.parseDouble(in.readLine());
		System.out.println();
		System.out.println("Introduzca el nombre de la arolinea a la cual va a pertencer el vuelo");
		System.out.print("-->  ");
		String pNombreAerolinea = in.readLine();
		System.out.println();
		System.out.println("Introduzca el nombre de la ciudad de ORIGEN del vuelo");
		System.out.print("-->  ");
		String pNombreCiudadOrigen = in.readLine();
		System.out.println();
		System.out.println("Introduzca el nombre de la ciudad de DESTINO del vuelo");
		System.out.print("-->  ");
		String pNombreCiudadDestino = in.readLine();
		System.out.println();
		System.out.println("Introduzca la hora de salida del vuelo desde la ciudad de oORIGEN, en el formato HH:MM");
		System.out.print("-->  ");
		String pHoraSalidaS = in.readLine();

		double pHoraSalida = (Double.parseDouble(pHoraSalidaS.split(":")[0])*60) + Double.parseDouble(pHoraSalidaS.split(":")[1]);

		System.out.println();
		System.out.println("Introduzca la hora de llegada del vuelo hasta la ciudad de DESTINO, en el formato HH:MM");
		System.out.print("-->  ");
		String pHoraLlegadaS = in.readLine();

		double pHoraLlegada = (Double.parseDouble(pHoraLlegadaS.split(":")[0])*60) + Double.parseDouble(pHoraLlegadaS.split(":")[1]);

		System.out.println();
		System.out.println("Introduzca el tipo de equipo");
		System.out.print("-->  ");
		String pEquipo = in.readLine();

		System.out.println();
		System.out.println("Introduzca el cupo del vuelo");
		System.out.print("-->  ");
		int pCupo = Integer.parseInt(in.readLine());

		System.out.println();
		System.out.println("Introduzca 1. si el vuelo es nacional, 0 si el vuelo es internacional");
		System.out.print("-->  ");
		int tipo = Integer.parseInt(in.readLine());
		boolean pTipo;

		if(tipo == 1)
		{
			pTipo = true;
		}
		else
		{
			pTipo = false;
		}

		System.out.println();
		System.out.print("Marque una X, si el vuelo trabajara en los dias de la semana indicados. Si no lo hara, oprima barra espaceadora");
		System.out.println();

		System.out.println("�El vuelo trabajar� el dia LUNES?");
		System.out.print("-->  ");
		String pLunes = in.readLine();

		System.out.println("�El vuelo trabajar� el dia MARETES?");
		System.out.print("-->  ");
		String pMartes = in.readLine();

		System.out.println("�El vuelo trabajar� el dia MIERCOLES?");
		System.out.print("-->  ");
		String pMiercoles = in.readLine();

		System.out.println("�El vuelo trabajar� el dia JUEVES?");
		System.out.print("-->  ");
		String pJueves = in.readLine();

		System.out.println("�El vuelo trabajar� el dia VIERNES?");
		System.out.print("-->  ");
		String pViernes = in.readLine();

		System.out.println("�El vuelo trabajar� el dia SABADO?");
		System.out.print("-->  ");
		String pSabado = in.readLine();

		System.out.println("�El vuelo trabajar� el dia DOMINGO?");
		System.out.print("-->  ");
		String pDomingo = in.readLine();
		
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		boolean res = principal.agregarVueloCatalogo( pNombre,pNombreAerolinea, pNombreCiudadOrigen, pNombreCiudadDestino, pHoraSalida, pHoraLlegada, pEquipo, pCupo,pTipo, pLunes, pMartes,  pMiercoles, pJueves,  pViernes,  pSabado,  pDomingo);

		if(res)
		{
			System.out.println("El vuelo se agreg� correctamente");
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}
		else
		{
			String r = " ";
			System.out.println("No fue posible agregar el vuelo, intentelo otra vez"+r+pNombre+pNombreAerolinea+r +pNombreCiudadOrigen+r+pNombreCiudadDestino+r+pHoraSalida +r+pHoraLlegada +r+pEquipo+r+ pCupo+pTipo +r+pLunes +r+pMartes +r+pMiercoles+r+ pJueves+r+ pViernes + r+pSabado +r+ pDomingo);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}

		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();

	}

	/**
	 * Calcular y actualizar las tarifas de los vuelos
	 * @param params
	 * @throws IOException 
	 */
	public void req6() throws IOException
	{		
		// TODO 
		// La informacion del requerimiento 6 corresponde a calcular la formula para
		// calcular la tarifa/costo de un tiquete de acuerdo al dia, a la aerolinea, al numero de sillas y al tiempo de vuelo (en minutos)
		// Este calculo se debe hacer para cada vuelo que se agregue al modelo del mundo
		

		System.out.println("Introduzca el nombre del viaje del cual ");
		System.out.println("Introduzca el id del vuelo que desea agregar");
		System.out.print("-->  ");
		double pNombre = Double.parseDouble(in.readLine());
		System.out.println();
		System.out.println("Introduzca el nombre de la arolinea a la cual va a pertencer el vuelo");
		System.out.print("-->  ");
		String pNombreAerolinea = in.readLine();
		System.out.println();
		System.out.println("Introduzca el nombre de la ciudad de ORIGEN del vuelo");
		System.out.print("-->  ");
		String pNombreCiudadOrigen = in.readLine();
		System.out.println();
		System.out.println("Introduzca el nombre de la ciudad de DESTINO del vuelo");
		System.out.print("-->  ");
		String pNombreCiudadDestino = in.readLine();
		System.out.println();
		System.out.println("Introduzca la hora de salida del vuelo desde la ciudad de oORIGEN, en el formato HH:MM");
		System.out.print("-->  ");
		String pHoraSalidaS = in.readLine();

		double pHoraSalida = (Double.parseDouble(pHoraSalidaS.split(":")[0])*60) + Double.parseDouble(pHoraSalidaS.split(":")[1]);

		System.out.println();
		System.out.println("Introduzca la hora de llegada del vuelo hasta la ciudad de DESTINO, en el formato HH:MM");
		System.out.print("-->  ");
		String pHoraLlegadaS = in.readLine();

		double pHoraLlegada = (Double.parseDouble(pHoraLlegadaS.split(":")[0])*60) + Double.parseDouble(pHoraLlegadaS.split(":")[1]);

		System.out.println();
		System.out.println("Introduzca el tipo de equipo");
		System.out.print("-->  ");
		String pEquipo = in.readLine();

		System.out.println();
		System.out.println("Introduzca el cupo del vuelo");
		System.out.print("-->  ");
		int pCupo = Integer.parseInt(in.readLine());

		System.out.println();
		System.out.println("Introduzca 1. si el vuelo es nacional, 0 si el vuelo es internacional");
		System.out.print("-->  ");
		int tipo = Integer.parseInt(in.readLine());;
		boolean pTipo;

		if(tipo == 1)
		{
			pTipo = true;
		}
		else
		{
			pTipo = false;
		}

		Viaje bus = null;

		if(principal.buscarAerolinea(pNombreAerolinea) != null && principal.buscarCiudad(pNombreCiudadDestino) != null && principal.buscarCiudad(pNombreCiudadOrigen) != null)
		{
			bus = new Viaje( pNombre, principal.buscarCiudad(pNombreCiudadOrigen), principal.buscarCiudad(pNombreCiudadDestino), pHoraSalida, pHoraLlegada, pEquipo, pCupo,pTipo, principal.buscarAerolinea(pNombreAerolinea));
		}
		else
		{
			System.out.println("Por favor digite ciudades y aerolineas existentes.");
			System.out.println();
			System.out.println("Oprima cualquier letra");
			System.out.print("-->  ");
			String vol = in.readLine();
		}

		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		double[] costos = principal.calcularCostos(bus);

		System.out.println("los costos para los dias lunes, martes, miercoles, jueves, viernes, sabado, domingo son:   ");
		for (int i = 0; i < costos.length; i++) 
		{
			double cos = costos[i];

			System.out.print(cos + " , " );
		}

		System.out.println("respectivamente.");
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();
	}

	/**
	 * Informar los conjuntos de ciudades que se pueden comunicar entre si  
	 * pero que no tienen comunicacion con el resto del pais sin importar las aerolinea
	 * @param params
	 * @throws IOException 
	 */
	public void req7() throws IOException
	{
		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar cada conjunto de la forma {ciudad1, ciudad2, ..., ciudadN}
		

		System.out.println("Introduzca una hora con el formato HH:MM");
		System.out.print("-->  ");
		String hora = in.readLine();
		double pHora = Double.parseDouble(hora.split(":")[0])*60 + Double.parseDouble(hora.split(":")[1]);

		System.out.println("Ingrese un dia de forma numerica. Por ejemplo lunes = 1, Jueves = 4, Domingo = 7");
		System.out.print("-->  ");
		int pDia = Integer.parseInt(in.readLine());

		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal.leerReq7(pHora, pDia);
		
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");

		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();
	}

	/**
	 * Informar los conjuntos de ciudades que se pueden comunicar entre si  
	 * pero que no tienen comunicacion con el resto del pais para cada aerolinea
	 * @param params
	 * @throws IOException 
	 */
	public void req8() throws IOException
	{
		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar cada conjunto de la forma <Aerolinea>: {ciudad1, ciudad2, ..., ciudadN}
		

		System.out.println("Introduzca una hora con el formato HH:MM");
		System.out.print("-->  ");
		String hora = in.readLine();
		double pHora = Double.parseDouble(hora.split(":")[0])*60 + Double.parseDouble(hora.split(":")[1]);

		System.out.println("Ingrese un dia de forma numerica. Por ejemplo lunes = 1, Jueves = 4, Domingo = 7");
		System.out.print("-->  ");
		int pDia = Integer.parseInt(in.readLine());

		System.out.println("Ingrese el nombre de la aerolinea");
		System.out.print("-->  ");
		String pAerolinea = in.readLine();
		
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal.leerReq8(pHora, pDia,pAerolinea);
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		String vol = in.readLine();
	}

	/**
	 * Calcular e imprimir el MST para vuelos nacionales, a partir de una ciudad especifica, 
	 * utilizando como peso de los arcos el tiempo del vuelo
	 * @param params
	 * params[0]= Ciudad origen
	 * @throws IOException 
	 */
	public void req9() throws IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar para ciudad en el MST al menos <Ciudad Origen>, <Ciudad Destino>, <Aerolinea>, <Numero Vuelo>, <Hora Vuelo>, <Tiempo Vuelo>, <Dia Vuelo>  
		

		System.out.println("Introduzca una hora con el formato HH:MM");
		System.out.print("-->  ");
		String hora = in.readLine();
		double pHora = Double.parseDouble(hora.split(":")[0])*60 + Double.parseDouble(hora.split(":")[1]);

		System.out.println("Ingrese un dia de forma numerica. Por ejemplo lunes = 1, Jueves = 4, Domingo = 7");
		System.out.print("-->  ");
		int pDia = Integer.parseInt(in.readLine());

		System.out.println("Ingrese el nombre de la ciudad de origen");
		System.out.print("-->  ");
		String pciudadOrigen = in.readLine();

		try
		{
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq9(pciudadOrigen, pHora, pDia);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		in.readLine();

	}

	/**
	 * Calcular e imprimir el MST para vuelos nacionales de una aerolinea particular, a partir de una ciudad especifica, 
	 * utilizando como peso de los arcos el costo de los vuelos
	 * @param params
	 * params[0]= Nombre aerolinea
	 * params[1]= Ciudad origen
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public void req10() throws NumberFormatException, IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar para ciudad en el MST al menos <Ciudad Origen>, <Ciudad Destino>, <Numero Vuelo>, <Hora Vuelo>, <Tarifa Vuelo>, <Dia Vuelo>  
		

		System.out.println("Introduzca el nombre de la ciudad de origen");
		System.out.print("-->  ");
		String pciudadOrigen = in.readLine();
		
		System.out.println("Introduzca el nombre de la aerolinea");
		System.out.print("-->  ");
		String pAerolinea = in.readLine();
		
		System.out.println("Introduzca una hora en el formato HH:MM");
		System.out.print("-->  ");
		String hora = in.readLine();
		double pHora = Double.parseDouble(hora.split(":")[0])*60 + Double.parseDouble(hora.split(":")[1]);
		
		System.out.println("Introduzca un dia en representacion numerica. EJ: LUNES = 1, MARETS = 2, VIERNES = 5, ECT...");
		System.out.print("-->  ");
		int pDia = Integer.parseInt(in.readLine());
		
		
		try 
		{
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq10(pciudadOrigen, pAerolinea, pHora, pDia);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		in.readLine();
	}

	/**
	 * Calcular e imprimir el MST  a partir de una ciudad especifica y de un dia particular, 
	 * sin importar cambios de aerolinea en el viaje
	 * @param params
	 * params[0]= Ciudad origen
	 * params[1]= Dia de partida
	 * @throws IOException 
	 */
	public void req11() throws IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar para ciudad en el MST al menos <Ciudad Origen>, <Ciudad Destino>, <Aerolinea>, <Numero Vuelo>, <Hora Vuelo>, <Tiempo Vuelo>, <Dia Vuelo>  



		System.out.println("Introduzca el nombre de la ciudad de origen");
		System.out.print("-->  ");
		String pciudadOrigen = in.readLine();
		
		
		System.out.println("Introduzca una hora en el formato HH:MM");
		System.out.print("-->  ");
		String hora = in.readLine();
		double pHora = Double.parseDouble(hora.split(":")[0])*60 + Double.parseDouble(hora.split(":")[1]);
		
		System.out.println("Introduzca un dia en representacion numerica. EJ: LUNES = 1, MARTES = 2, VIERNES = 5, ECT...");
		System.out.print("-->  ");
		int pDia = Integer.parseInt(in.readLine());
		
		
		try 
		{ 
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq11(pciudadOrigen, pHora, pDia);	
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		in.readLine();
		
	}

	/**
	 * Calcular e imprimir el itinerario de costo minimo para cada aerolinea
	 * @param params
	 * params[0]= Ciudad origen
	 * params[1]= Ciudad destino
	 * params[2]= Dia de partida
	 * @throws IOException 
	 * 
	 */
	public void req12() throws IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar cada itinerario como <Aerolinea> {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad Intermedia1>-<Costo>, ..., 
		//                                           <Ciudad Intermedia>-<Vuelo>-<Dia>-<Hora>-<Ciudad Destino>-<Costo>}, <Costo-Total-Minimo>
		
		
		System.out.println("Introduzca el nombre de la ciudad de origen");
		System.out.print("-->  ");
		String pCiudadOrigen = in.readLine();

		System.out.println("Introduzca el nombre de la ciudad de destino");
		System.out.print("-->  ");
		String pCiudadDestino = in.readLine();

		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal.leerReq12(pCiudadOrigen, pCiudadDestino);
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		in.readLine();
	}

	/**
	 * Calcular e imprimir el itinerario de costo minimo para diferentes aerolineas
	 * @param params
	 * params[0]= Ciudad origen
	 * params[1]= Ciudad destino
	 * params[2]= Dia de partida
	 * @throws IOException 
	 */
	public void req13() throws IOException
	{
		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar el itinerario como {<Ciudad Origen>-<Aerolinea>-<Vuelo>-<Dia>-<Hora>-<Ciudad Intermedia1>-<Costo>, ..., 
		//                             <Ciudad Intermedia>-<Aerolinea>-<Vuelo>-<Dia>-<Hora>-<Ciudad Destino>-<Costo>}, <Costo-Total-Minimo>
		
		
		System.out.println("Introduzca el nombre de la ciudad de origen");		
		System.out.print("-->  ");
		String pCiudadOrigen = in.readLine();
		
		
		System.out.println("Introduzca el nombre de la ciudad de destino");
		System.out.print("-->  ");
		String pCiudadDestino = in.readLine();
		
		double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
		principal.leerReq13(pCiudadOrigen, pCiudadDestino);
		System.out.println();
		System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");
		in.readLine();
	}

	/**
	 * Calcular e imprimir la ruta de costo minimo para ir a todas las otras ciudades cubiertas por una aerolinea
	 * @param params
	 * params[0]= Nombre ciudad
	 * params[1]= Nombre aerolinea
	 * @throws IOException 
	 * 
	 */
	
	public void req14() throws IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad A>-<Costo-Origen-A>, ..., 
		//                       <Ciudad ?>-<Vuelo>-<Dia>-<Hora>-<Ciudad X>-<Costo-Origen-X>}	
		

		System.out.println("Introduzca el nombre de la ciudad de origen");		
		System.out.print("-->  ");
		String pCiudadOrigen = in.readLine();
		
		
		System.out.println("Introduzca el nombre de la aerolinea");
		System.out.print("-->  ");
		String pCiudadDestino = in.readLine();
		
		try
		{		
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq14(pCiudadOrigen, pCiudadDestino);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");		
		}
		catch(Exception e)
		{
			System.out.println("La ciudad de origen no es cubierta por la aerolinea dada");
		}
		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");		
		in.readLine();
	}

	/**
	 * Calcular e imprimir la ruta de menor tiempo para ir a todas las otras ciudades cubiertas por una aerolinea
	 * @param params
	 * params[0]= Nombre ciudad
	 * params[1]= Nombre aerolinea
	 * @throws IOException 
	 */
	public void req15() throws IOException{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad A>-<Tiempo-Origen-A>, ..., 
		//                       <Ciudad ?>-<Vuelo>-<Dia>-<Hora>-<Ciudad X>-<Tiempo-Origen-X>}
		

		System.out.println("Introduzca el nombre de la ciudad de origen");		
		System.out.print("-->  ");
		String pCiudadOrigen = in.readLine();
		
		
		System.out.println("Introduzca el nombre de la aerolinea");
		System.out.print("-->  ");
		String pCiudadDestino = in.readLine();
		
		try
		{	
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq15(pCiudadOrigen, pCiudadDestino);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");	
		}
		catch(Exception e)
		{
			System.out.println("La ciudad de origen no es cubierta por la aerolinea dada");
		}
		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");		
		in.readLine();
	}

	/**
	 * Calcular e imprimir la ruta de minimo precio para visitar todas las otras ciudades cubiertas por una aerolinea
	 * @param params
	 * params[0]= Nombre ciudad
	 * params[1]= Dia de la semana
	 * params[2]= Nombre aerolinea
	 * @throws IOException 
	 */
	
	public void req16() throws IOException
	{
		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad A>-<Tiempo-Origen-A>, ..., 
		//                       <Ciudad ?>-<Vuelo>-<Dia>-<Hora>-<Ciudad X>-<Tiempo-Origen-X>}

		System.out.println("Introduzca el nombre de la ciudad de origen");		
		System.out.print("-->  ");
		String pCiudadOrigen = in.readLine();


		System.out.println("Introduzca el nombre de la aerolinea");
		System.out.print("-->  ");
		String pCiudadDestino = in.readLine();
		
		System.out.println("Introduzca el dia");
		System.out.print("-->  ");
		int  pDia = Integer.parseInt(in.readLine());

		try
		{	
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq16(pCiudadOrigen, pCiudadDestino, pDia);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");	
		}
		catch(Exception e)
		{
			System.out.println("La ciudad de origen no es cubierta por la aerolinea dada");
		}

		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");		
		in.readLine();
		
	}

	/**
	 * Buscar la aerolinea, ciudad, dia de la semana y hora para iniciar un viaje que permita a un viajero visitar la mayor cantidad de ciudades a minimo costo, en vuelos de una misma aerolinea.
	 * @param params
	 * @throws IOException 
	 */
	public void req17() throws IOException
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar <Aerolinea>, <Ciudad Origen>, <Dia de semana> y <Hora Salida> inicial del viaje
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad A>-<Costo-Origen-A>, 
		//                       <Ciudad A>-<Vuelo>-<Dia>-<Hora>-<Ciudad B>-<Costo-A-B>, ..., 
		//                       <Ciudad X>-<Vuelo>-<Dia>-<Hora>-<Ciudad Ultima>-<Costo-X-Ultima>}
		// Mostrar <Costo total viaje>
		System.out.println("Tenga paciencia con el m�todo. Se le mostrar� progresivamente las alternativas que logren ser m�s �ptimas hasta llegar a la mejor");
		System.out.println("Introduzca el nombre de la aerolinea");
		System.out.print("-->  ");
		String pAerolinea = in.readLine();
		
		try
		{				
			System.out.println();
			double tiempoInicial = System.currentTimeMillis()/Math.pow(10, 3);
			principal.leerReq17(pAerolinea);
			System.out.println();
			System.out.println("Tiempo: " + (System.currentTimeMillis()/Math.pow(10, 3)- tiempoInicial) + " segundos");	
		}
		catch(Exception e)
		{
			e.printStackTrace();;
		}
		
		System.out.println();
		System.out.println("Oprima cualquier letra");
		System.out.print("-->  ");		
		in.readLine();
	}

	/**
	 * Buscar el dia de la semana y hora para iniciar un viaje que permita a un viajero visitar la mayor cantidad de ciudades, en vuelos de una aerolinea preferida.
	 * @param params
	 * params[0]= Nombre aerolinea
	 */
	public void req18()
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar <Ciudad Origen>, <Dia de semana> y <Hora Salida> inicial del viaje
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Ciudad A>, 
		//                       <Ciudad A>-<Vuelo>-<Dia>-<Hora>-<Ciudad B>, ..., 
		//                       <Ciudad X>-<Vuelo>-<Dia>-<Hora>-<Ciudad Ultima>}
		// Mostrar <Numero ciudades visitas>

	}

	/**
	 * Buscar el dia de la semana y hora para iniciar un viaje que permita a un viajero visitar la mayor cantidad de ciudades, con posibilidad de cambios de aerolinea.
	 * @param params
	 */
	public void req19()
	{

		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar <Ciudad Origen>, <Dia de semana> y <Hora Salida> inicial del viaje
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad A>, 
		//                       <Ciudad A>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad B>, ..., 
		//                       <Ciudad X>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad Ultima>}
		// Mostrar <Numero ciudades visitas>, <Costo total viaje>, <Tiempo total en vuelos>

	}

	/**
	 * Buscar la ruta para visitar un conjunto de ciudades dadas bajo las restricciones definidas.
	 * Nota: No se requiere visitar las ciudades en el orden de ingreso en los parametros
	 * @param params
		String ciudad = params[0];
		String dia = params[1];
		String hora = params[2];
		String ciudad intermedia 1 = params[3];
		...
		String ciudad intermedia N = params[3+N-1];		
	 */
	public void req20()
	{

		// A continuacion deben venir los nombres de las ciudades intermedias a visitar (N)
		//		String ciudad_I1 = params[3]; // Nombre de ciudad intermedia a visitar
		// ...
		// String ciudad_IN = params[3+N-1];  // Nombre de ciudad intermedia a visitar 
		//TODO
		// Completar segun documentacion del requerimiento
		// Mostrar <Ciudad Origen>, <Dia de semana> y <Hora Salida> inicial del viaje
		// Mostrar la ruta como {<Ciudad Origen>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad Intermedia X>, 
		//                       <Ciudad Intermedia X>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad Intermedia �?>, ..., 
		//                       <Ciudad Intermedia �?>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad intermedia Ultima>,
		//                       <Ciudad Intermedia Ultima>-<Vuelo>-<Dia>-<Hora>-<Aerolinea>-<Ciudad Origen>}

	}

	public static void main(String args[])	
	{		
		new Cliente();		
	}
}
