package Estructuras;
public class ListaNodo<T> 
{
	private T elemento;
	private ListaNodo<T> siguiente;
	private ListaNodo<T> anterior;

	public ListaNodo( T pElemento )
	{
		elemento = pElemento;
		siguiente = null;
		anterior = null;
	}

	public T darElemento( )
	{
		return elemento;
	}

	public ListaNodo<T> darSiguiente( )
	{
		return siguiente;
	}

	public ListaNodo<T> darAnterior( )
	{
		return anterior;
	}

	public void insertarAntes( ListaNodo<T> nodo )
	{
		nodo.siguiente = this;
		nodo.anterior = anterior;
		if( anterior != null )
		{
			anterior.siguiente = nodo;
		}
		anterior = nodo;
	}

	public void insertarDespues( ListaNodo<T> nodo )
	{
		nodo.siguiente = siguiente;
		nodo.anterior = this;
		if( siguiente != null )
		{
			siguiente.anterior = nodo;
		}
		siguiente = nodo;
	}

	public ListaNodo<T> eliminarPrimero( )
	{
		ListaNodo<T> p = siguiente;
		siguiente = null;
		if( p != null )
		{
			p.anterior = null;
		}
		return p;
	}

	public void eliminarNodo( )
	{
		ListaNodo<T> ant = anterior;
		ListaNodo<T> sig = siguiente;
		anterior = null;
		siguiente = null;
		ant.siguiente = sig;
		if( sig != null )
		{
			sig.anterior = ant;
		}
	}

}
