package Estructuras;

public interface IQueue<Item>
{
	public void enQueue(Item item);

	public Item deQueue();

	public boolean isEmpty();

	public int size();
}
