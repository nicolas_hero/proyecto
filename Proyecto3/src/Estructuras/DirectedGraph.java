package Estructuras;

import java.util.*; // For HashMap

public final class DirectedGraph<T extends Comparable<T>, K> implements Iterable<T>
{
    /* A map from nodes in the graph to sets of outgoing edges.  Each
     * set of edges is represented by a map from edges to doubles.
     */
    private final HashTableLP<T, HashTableLP<T, K>> mGraph = new HashTableLP<T, HashTableLP<T, K>>();

    public boolean addNode(T node)
    {
        /* If the node already exists, don't do anything. */
        if (mGraph.contains(node))
            return false;

        /* Otherwise, add the node with an empty set of outgoing edges. */
        mGraph.put(node, new HashTableLP<T, K>());
        return true;
    }
    
    public Lista<T> getNodes()
    {
    	Iterator<T> iter = this.iterator();
    	Lista<T> answer = new Lista<T>();
    	
    	while(iter.hasNext())
    	{
    		
    		T object = iter.next();    		
    		answer.agregarAlFinal(object); 		
    	}
    	
    	return answer;    	
    }
    
    public Lista<T> adj(T start)
    {
    	if (!mGraph.contains(start))
            throw new NoSuchElementException("Node must be in the graph.");
    	
    	HashTableLP<T, K> adj = edgesFrom(start);
    	
    	Lista<T> answer = new Lista<T>();
    	
    	Lista<T> nodes = getNodes();
    	
    	ListaNodo<T> n = nodes.darPrimerNodo();  	
    	
    	while(n != null)
    	{
    		if(adj.contains(n.darElemento()))
    		{
    			answer.agregarAlFinal(n.darElemento());
    		}
    		n = n.darSiguiente();
    	}
    	
    	
    	return answer;   	
    }
    
    public void addEdge(T start, T dest, K length) 
    {
        /* Confirm both endpoints exist. */
        if (!mGraph.contains(start) || !mGraph.contains(dest))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        /* Add the edge. */
        mGraph.get(start).put(dest, length);
    }

    public void removeEdge(T start, T dest) 
    {
        /* Confirm both endpoints exist. */
        if (!mGraph.contains(start) || !mGraph.contains(dest))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        mGraph.get(start).delete(dest);
    }

 
    public HashTableLP<T, K> edgesFrom(T node) 
    {
        /* Check that the node exists. */
        HashTableLP<T, K> arcs = mGraph.get(node);
        if (arcs == null)
            throw new NoSuchElementException("Source node does not exist.");

        return arcs;
    }
    
    public int nodesAmount()
    {
    	return getNodes().darTamanio();
    }
    
    public DirectedGraph<T, K> reverse()
    {
    	DirectedGraph<T, K> R = new DirectedGraph<T,K>();	
    	
    	Lista<T> nodes = getNodes();
    	
    	ListaNodo<T> n = nodes.darPrimerNodo();
    	
    	while (n != null)
    	{
    		
    		R.addNode(n.darElemento()); 
    		n = n.darSiguiente();
    	}
    	
    	n = nodes.darPrimerNodo();
    	
    	while (n != null)
    	{
    		Lista<T> adj = adj(n.darElemento());
    		
    		ListaNodo<T> a = adj.darPrimerNodo();
    		
    		while (a != null)
    		{
    			R.addEdge(a.darElemento(), n.darElemento(), (K) edgesFrom(n.darElemento()).get(a.darElemento()));
    			a = a.darSiguiente();
    		}    			
    		
    		n = n.darSiguiente();
    	}	

		return R;
    }
 
    public Iterator<T> iterator() 
    {
        return mGraph.iterator();
    }
}