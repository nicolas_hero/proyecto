package Estructuras;

import Estructuras.Stack;



import Estructuras.DirectedEdge;
import Estructuras.In;
import Estructuras.IndexMinPQ;
import Estructuras.StdOut;

public class DijkstraSP<T extends Comparable<T>> 
{
    private double[] distTo;          // distTo[v] = distance  of shortest s->v path
    private EdgeDGW<T>[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
    private IndexMinPQ<Double> pq;    // priority queue of vertices
    private DirectedGraphWeight<T> graph;
    
    /**
     * Computes a shortest-paths tree from the source vertex {@code s} to every other
     * vertex in the edge-weighted digraph {@code G}.
     *
     * @param  G the edge-weighted digraph
     * @param  s the source vertex
     * @throws Exception 
     * @throws IllegalArgumentException if an edge weight is negative
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public DijkstraSP(DirectedGraphWeight<T> G, T s) throws Exception 
    {
    	graph = G;
        for (EdgeDGW<T> e : G.edges())
        {
            if (e.weight() < 0)
                throw new IllegalArgumentException("edge " + e + " has negative weight");
        }

        distTo = new double[G.nodesAmount()];
        edgeTo = new EdgeDGW[G.nodesAmount()];

        validateVertex(G.getNodes().posNode(s)-1);

        for (int v = 0; v < G.nodesAmount(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[G.getNodes().posNode(s)-1] = 0.0;

        // relax vertices in order of distance from s
        pq = new IndexMinPQ<Double>(G.nodesAmount());
        pq.insert(G.getNodes().posNode(s)-1, distTo[G.getNodes().posNode(s)-1]);
        while (!pq.isEmpty()) 
        {
            int v = pq.delMin();
            for (EdgeDGW<T> e : G.adjEdges(G.getNodes().dar(v+1)))
                relax(e);
        }

        // check optimality conditions
        assert check(G, G.getNodes().posNode(s)-1);
    }

    // relax edge e and update pq if changed
    private void relax(EdgeDGW<T> e) throws Exception 
    {
        int v = graph.getNodes().posNode(e.either())-1, w = graph.getNodes().posNode(e.other(e.either()))-1;
        if (distTo[w] > distTo[v] + e.weight()) 
        {
            distTo[w] = distTo[v] + e.weight();
            edgeTo[w] = e;
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
        }
    }

    /**
     * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
     * @param  v the destination vertex
     * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
     *         {@code Double.POSITIVE_INFINITY} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public double distTo(int v)
    {
        validateVertex(v);
        return distTo[v];
    }

    /**
     * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return {@code true} if there is a path from the source vertex
     *         {@code s} to vertex {@code v}; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean hasPathTo(int v)
    {
        validateVertex(v);
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * Returns a shortest path from the source vertex {@code s} to vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return a shortest path from the source vertex {@code s} to vertex {@code v}
     *         as an iterable of edges, and {@code null} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Lista<EdgeDGW<T>> pathTo(T pV) 
    {
    	int v = graph.getNodes().posNode(pV)-1;
        validateVertex(v);
        if (!hasPathTo(v)) return null;
        Lista<EdgeDGW<T>> path = new Lista<EdgeDGW<T>>();
        for (EdgeDGW<T> e = edgeTo[v]; e != null; e = edgeTo[graph.getNodes().posNode(e.either())-1])
        {
            path.agregarAlPrincipio(e);
        }
        return path;
    }


    // check optimality conditions:
    // (i) for all edges e:            distTo[e.to()] <= distTo[e.from()] + e.weight()
    // (ii) for all edge e on the SPT: distTo[e.to()] == distTo[e.from()] + e.weight()
    private boolean check(DirectedGraphWeight<T> G, int s) throws Exception
    {

        // check that edge weights are nonnegative
        for (EdgeDGW<T> e : G.edges()) 
        {
            if (e.weight() < 0) {
                System.err.println("negative edge weight detected");
                return false;
            }
        }

        // check that distTo[v] and edgeTo[v] are consistent
        if (distTo[s] != 0.0 || edgeTo[s] != null)
        {
            System.err.println("distTo[s] and edgeTo[s] inconsistent");
            return false;
        }
        for (int v = 0; v < G.nodesAmount(); v++)
        {
            if (v == s) continue;
            if (edgeTo[v] == null && distTo[v] != Double.POSITIVE_INFINITY) 
            {
                System.err.println("distTo[] and edgeTo[] inconsistent");
                return false;
            }
        }

        // check that all edges e = v->w satisfy distTo[w] <= distTo[v] + e.weight()
        for (int v = 0; v < G.nodesAmount(); v++)
        {
            for (EdgeDGW<T> e : G.adjEdges(graph.getNodes().dar(v+1)))
            {
                int w = G.getNodes().posNode(e.other(e.either()))-1;
                if (distTo[v] + e.weight() < distTo[w])
                {
                    System.err.println("edge " + e + " not relaxed");
                    return false;
                }
            }
        }

        // check that all edges e = v->w on SPT satisfy distTo[w] == distTo[v] + e.weight()
        for (int w = 0; w < G.nodesAmount(); w++)
        {
            if (edgeTo[w] == null) continue;
            EdgeDGW<T> e = edgeTo[w];
            int v = graph.getNodes().posNode(e.either())-1;
            if (w != graph.getNodes().posNode(e.other(e.either()))-1) return false;
            if (distTo[v] + e.weight() != distTo[w])
            {
                System.err.println("edge " + e + " on shortest path not tight");
                return false;
            }
        }
        return true;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) 
    {
        int V = distTo.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }    

}