package Estructuras;


import java.util.*; // For HashMap


//DirectedGraph pero con pesos DOUBLE en sus arcos, por eso no pide tipo K.
public final class DirectedGraphWeight<T extends Comparable<T>> implements Iterable<T>
{
    /* A map from nodes in the graph to sets of outgoing edges.  Each
     * set of edges is represented by a map from edges to doubles.
     */
    private final HashTableLP<T, HashTableLP<T, EdgeDGW<T>>> mGraph = new HashTableLP<T, HashTableLP<T, EdgeDGW<T>>>();

    public boolean addNode(T node)
    {
        /* If the node already exists, don't do anything. */
        if (mGraph.contains(node))
            return false;

        /* Otherwise, add the node with an empty set of outgoing edges. */
        mGraph.put(node, new HashTableLP<T, EdgeDGW<T>>());
        return true;
    }
    
    public Lista<T> getNodes()
    {
    	Iterator<T> iter = this.iterator();
    	Lista<T> answer = new Lista<T>();
    	
    	while(iter.hasNext())
    	{
    		
    		T object = iter.next();    		
    		answer.agregarAlFinal(object); 		
    	}
    	
    	return answer;    	
    }
    
    public Lista<T> adj(T start)
    {
    	if (!mGraph.contains(start))
            throw new NoSuchElementException("Node must be in the graph.");
    	
    	HashTableLP<T, EdgeDGW<T>> adj = edgesFrom(start);
    	
    	Lista<T> answer = new Lista<T>();
    	
    	Lista<T> nodes = getNodes();
    	
    	ListaNodo<T> n = nodes.darPrimerNodo();  	
    	
    	while(n != null)
    	{
    		if(adj.contains(n.darElemento()))
    		{
    			answer.agregarAlFinal(n.darElemento());
    		}
    		n = n.darSiguiente();
    	}
    	
    	
    	return answer;   	
    }
    
    public Stack<EdgeDGW<T>> adjEdges(T start)
    {
    	if (!mGraph.contains(start))
            throw new NoSuchElementException("Node must be in the graph.");
    	
    	HashTableLP<T, EdgeDGW<T>> adj = edgesFrom(start);
    	
    	Stack<EdgeDGW<T>> answer = new Stack<EdgeDGW<T>>();
    	
    	Lista<T> nodes = getNodes();
    	
    	ListaNodo<T> n = nodes.darPrimerNodo();  	
    	
    	while(n != null)
    	{
    		if(adj.contains(n.darElemento()))
    		{
    			answer.push(adj.get(n.darElemento()));
    		}
    		n = n.darSiguiente();
    	}
    	
    	
    	return answer;   	
    }
    
    
    public void addEdge(T start, T dest, Double weight) 
    {
        /* Confirm both endpoints exist. */
        if (!mGraph.contains(start) || !mGraph.contains(dest))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        /* Add the edge. */
        EdgeDGW<T> edge = new EdgeDGW<T>(start, dest, weight);
        mGraph.get(start).put(dest, edge);
    }

    public void removeEdge(T start, T dest) 
    {
        /* Confirm both endpoints exist. */
        if (!mGraph.contains(start) || !mGraph.contains(dest))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        mGraph.get(start).delete(dest);
    }

 
    public HashTableLP<T, EdgeDGW<T>> edgesFrom(T node) 
    {
        /* Check that the node exists. */
        HashTableLP<T, EdgeDGW<T>> arcs = mGraph.get(node);
        if (arcs == null)
            throw new NoSuchElementException("Source node does not exist.");

        return arcs;
    }
    
    public Stack<EdgeDGW<T>> edges()
    {
    	Stack<EdgeDGW<T>> edges = new Stack<EdgeDGW<T>>();
    	Lista<T> nodes = getNodes();    	
    	ListaNodo<T> node = nodes.darPrimerNodo();
    	
    	while(node != null)
    	{
    		T nodeElement = node.darElemento();
    		HashTableLP<T, EdgeDGW<T>> adjNode = edgesFrom(nodeElement);
    		ListaNodo<T> nodeAdj = nodes.darPrimerNodo();
    		
    		while(nodeAdj != null)
    		{
    			T nodeAdjElement = nodeAdj.darElemento();
    			if (adjNode.contains(nodeAdjElement))
    			{
    				edges.push(adjNode.get(nodeAdjElement));
    			}
    			nodeAdj = nodeAdj.darSiguiente();
    		}
    		
    		node = node.darSiguiente();
    	}
    	
    	return edges;
    }
    
    public int nodesAmount()
    {
    	return getNodes().darTamanio();
    }
    
    public DirectedGraphWeight<T> reverse()
    {
    	DirectedGraphWeight<T> R = new DirectedGraphWeight<T>();	
    	
    	Lista<T> nodes = getNodes();
    	
    	ListaNodo<T> n = nodes.darPrimerNodo();
    	
    	while (n != null)
    	{
    		
    		R.addNode(n.darElemento()); 
    		n = n.darSiguiente();
    	}
    	
    	n = nodes.darPrimerNodo();
    	
    	while (n != null)
    	{
    		Lista<T> adj = adj(n.darElemento());
    		
    		ListaNodo<T> a = adj.darPrimerNodo();
    		
    		while (a != null)
    		{
    			R.addEdge(a.darElemento(), n.darElemento(), edgesFrom(n.darElemento()).get(a.darElemento()).weight());
    			a = a.darSiguiente();
    		}    			
    		
    		n = n.darSiguiente();
    	}	

		return R;
    }
 
    public Iterator<T> iterator() 
    {
        return mGraph.iterator();
    }
}

