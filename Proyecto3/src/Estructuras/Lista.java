package Estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class Lista<T> implements Iterable<T>
{

	@SuppressWarnings("hiding")
	private class Iterador<T> implements Iterator<T>
	{
		private ListaNodo<T> prim;
		private boolean esPrimero;

		public Iterador(ListaNodo<T> nodo)
		{
			prim = nodo;
			esPrimero = (nodo == primero) ? true:false;
		}
		@Override
		public boolean hasNext() 
		{
			if(esPrimero)
			{
				return prim != null;
			}
			else
				return prim.darSiguiente()!= null;
		}

		@Override
		public T next() 
		{
			// TODO Auto-generated method stub
			if(!hasNext())
				throw new NoSuchElementException();

			if(esPrimero)
				esPrimero = false;

			else
				prim = prim.darSiguiente();

			return (T) prim.darElemento();

		}

		@Override
		public void remove() 
		{
			next();
			prim.darAnterior().eliminarNodo();
		}	
	}

	private ListaNodo<T> primero;
	private ListaNodo<T> ultimo;
	private int tamanio;

	public Lista( )
	{
		primero = null;
		ultimo = null;
		tamanio = 0;
	}

	public T darPrimero()
	{
		return primero.darElemento();
	}

	public ListaNodo<T> darPrimerNodo()
	{
		return primero;
	}
	
	public T darUltimo()
	{
		return ultimo.darElemento();
	}
	
	public ListaNodo<T> darUltimoNodo()
	{
		return ultimo;
	}
	
	public T[] darArreglo() {
		// TODO Auto-generated method stub
		T[] nodos=(T[])new Object[tamanio];

		int i=0;
		for(ListaNodo<T> n = primero;n!=null;n=n.darSiguiente(),i++)
		{
			nodos[i]=(T) n.darElemento();
		}

		return nodos;
	}


	public void agregarAlFinal(T nuevo) 
	{
		ListaNodo<T> nodo = new ListaNodo<T>( nuevo );
		if( primero == null )
		{
			primero = nodo;
			ultimo = nodo;
		}
		else
		{
			ultimo.insertarDespues( nodo );
			ultimo = nodo;
		}
		tamanio++;

	}


	public void agregarAlPrincipio(T nuevo) 
	{
		ListaNodo<T> nodo = new ListaNodo<T>( nuevo );
		if( primero == null )
		{
			primero = nodo;
			ultimo = nodo;
		}
		else
		{
			primero.insertarAntes( nodo );
			primero = nodo;
		}
		tamanio++;

	}
	
	

	public boolean agregarAntesDe(T nuevo,T ref) 
	{
		ListaNodo<T> x = new ListaNodo<T>(nuevo);		
		for( ListaNodo<T> p = primero; p != null; p = p.darSiguiente( ) )
		{
			if( p.darElemento( ).equals( ref ) )
			{
				p.insertarAntes(x);
				return true;
			}
		}
		return false;
	}


	public boolean agregarDespuesDe(T nuevo,T ref) 
	{
		ListaNodo<T> x = new ListaNodo<T>(nuevo);		
		for( ListaNodo<T> p = primero; p != null; p = p.darSiguiente( ) )
		{
			if( p.darElemento( ).equals( ref ) )
			{
				p.insertarDespues(x);
				return true;
			}
		}
		return false;
	}


	public T buscar(T buscado) 
	{
		for( ListaNodo<T> p = primero; p != null; p = p.darSiguiente( ) )
		{
			if( p.darElemento( ).equals( buscado ) )
			{
				return p.darElemento( );
			}
		}
		return null;
	}	
	
	public boolean estaEnLaLista(T pBuscado)
	{
		boolean esta = false;
		T n = buscar(pBuscado);
		
		if(n != null)
		{
			esta = true;
		}
		
		return esta;
		
	}


	public T dar(int pos) 
	{
		ListaNodo<T> aux = primero;
		for( int cont = 1; cont < pos; cont++ )
		{
			aux = aux.darSiguiente( );
		}
		return aux.darElemento( );
	}


	public int darTamanio() 
	{
		return tamanio;
	}
	
	//Retorna la posici�n en la lista de 1 a tamanio.
	
	public int posNode(T aBuscar)
	{
		int counter = 0;
		
		boolean encontrado = false;
				
		ListaNodo<T> a = darPrimerNodo();
		
		while (a != null && !encontrado)
		{			
			if(a.darElemento().equals(aBuscar))
			{
				encontrado = true;
			}
			counter ++;
			a = a.darSiguiente();
		}
		
		return counter;
	}


	public T eliminar(T aEliminar) 
	{
		T valor = null;

		if( primero.darElemento( ).equals( aEliminar ) )
		{
			if( primero.equals( ultimo ) )
			{
				ultimo = null;
			}
			valor = primero.darElemento( );
			primero = primero.eliminarPrimero( );
			tamanio--;
			return valor;
		}
		else
		{
			for( ListaNodo<T> p = primero.darSiguiente( ); p != null; p = p.darSiguiente( ) )
			{
				if( p.darElemento( ).equals( aEliminar ) )
				{
					if( p.equals( ultimo ) )
					{
						ultimo = p.darAnterior( );
					}
					valor = p.darElemento( );
					p.eliminarNodo( );
					tamanio--;
				}
			}
			return valor;
		}
	}

	public ListaNodo<T> eliminarPrimero()
	{
		if(primero == null)
		{
			return null;
		}
		else
		{
			ListaNodo<T> c = primero;
			primero = primero.darSiguiente();
			return c;
		}
	}


	public boolean estaVacia() 
	{
		if(tamanio == 0)
			return true;
		return false;
	}
	
	
		


	public Iterator<T> iterator() 
	{	
		return new Iterador<T>(primero);
	}

}
