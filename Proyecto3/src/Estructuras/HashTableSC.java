package Estructuras;

public class HashTableSC <Key extends Comparable<Key>, Value> implements IHashTable<Key, Value> 
{
	private int M;

	private SequentialSymbolTable<Key, Value>[] st;

	

	public HashTableSC() 
	{

		this(997);

	}

	

	@SuppressWarnings("unchecked")

	public HashTableSC(int M)
	{

		this.M = M;

		st = (SequentialSymbolTable<Key, Value>[]) new SequentialSymbolTable[M];

		for(int i = 0; i < st.length; i++)
		{

			st[i] = new SequentialSymbolTable<Key,Value>();

		}

	}

	

	public void put(Key key, Value value) 
	{

		st[hash(key)].put(key, value);

	}

	

	public Value get(Key key)
	{

		return (Value) st[hash(key)].get(key);

	}
	
	public boolean containsKey(Key key)
	{
		boolean ans = st[hash(key)].contains(key);
		return ans;
	}
	
	public SequentialSymbolTable<Key, Value>[] getST()
	{
		return st;
	}

	

	public void delete(Key key)
	{

		st[hash(key)].delete(key);

	}

	

	public int hash(Key key)
	{

		return (key.hashCode() & 0x7fffffff) % M;

	}
}
