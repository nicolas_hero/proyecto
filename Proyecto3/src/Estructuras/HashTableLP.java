package Estructuras;


import java.util.Iterator;
import java.util.NoSuchElementException;



public class HashTableLP <Key extends Comparable<Key>, Value> implements IHashTable<Key, Value>, Iterable<Key> 
{
	private int N;

	private int M = 7;

	private Key[] keys;

	private Value[] vals;

	private class Iterador<T> implements Iterator<T>
	{
		private T prim;
		private boolean esPrimero;
		private int contador;
		private boolean vacio;

		@SuppressWarnings("unchecked")
		public Iterador(T key)
		{			
			
			contador = 0;
			vacio = false;
			
			if(keys[contador]== null)
			{				
				while(!vacio && keys[contador+1] == null)
				{
					contador++;	
					if (contador == (M-1))
					{
						vacio = true;
					}
				}
				if(contador < M-1)
				{
					contador ++;
				}
			}
			
			esPrimero = true;
			
			prim = (T) keys[contador];
		}
		@Override
		public boolean hasNext() 
		{
			if(vacio)
			{
				return false;
			}
			
			if(esPrimero)
			{								
				return prim != null;
			}
			else
			{
				int contadorWas = contador;
				if(contador != (M-1))
				{
					while(keys[contador+1] == null)
					{					
						contador++;	
						if (contador == (M-1))
						{	
							contador = contadorWas;
							return false;
						}					
					}								
					contador = contadorWas;
					return true;	
				}
				else
				{
					return false;
				}
			}				
			
		}

		@SuppressWarnings("unchecked")
		@Override
		public T next() 
		{
			
			if(!hasNext())
				throw new NoSuchElementException();

			if(esPrimero)
				esPrimero = false;

			else
			{				
				while(keys[contador+1] == null)
				{
					contador++;						
				}			
				contador++;
				prim = (T) keys[contador];
			}

			return prim;
		}

		@Override
		public void remove() 
		{
			next();
			keys[contador-1]= null;
		}	
	}

	@SuppressWarnings("unchecked")

	public HashTableLP()
	{

		keys = (Key[]) new Comparable[M];

		vals = (Value[]) new Object[M];

	}

	

	@SuppressWarnings("unchecked")

	public HashTableLP(int max) 
	{

		M = max;

		keys = (Key[]) new Comparable[M];

		vals = (Value[]) new Object[M];

	}

	

	public void put(Key key, Value value) 
	{

		if(N >= M/2) resize(2*M);

		

		int i;

		for(i = hash(key); keys[i] != null; i = (i+1) % M)

			if(keys[i].equals(key)) { vals[i] = value; return;}

		

		keys[i] = key;

		vals[i] = value;

		N++;

	}

	

	public Value get(Key key) 
	{

		for(int i = hash(key); keys[i] != null; i = (i+1) % M)

			if(keys[i].equals(key)) return vals[i];

		

		return null;

	}

	

	public int hash(Key key)
	{

		return (key.hashCode() & 0x7fffffff) % M;

	}

	

	public void delete(Key key) 
	{

		if(!contains(key)) return;

		

		int i = hash(key);

		while(!key.equals(keys[i]))
		{

			i = (i+1) % M;

		}

		

		keys[i] = null;

		vals[i] = null;

		

		i = (i+1) % M;

		

		while(keys[i] != null)
		{

			Key keyToRedo = keys[i];

			Value valueToRedo = vals[i];

			keys[i] = null;

			vals[i] = null;

			N--;

			put(keyToRedo, valueToRedo);

			i = (i+1) % M;

		}

		N--;

		

		if(N > 0 && N == M/8)

			resize(M/2);

	}

	

    // Helpers 

	

	public void resize(int size)
	{

		  HashTableLP<Key, Value> t = new HashTableLP<Key, Value>(size);

		  for(int i = 0; i < M; i++) 
		  {

			  if(keys[i] != null)

				  t.put(keys[i], vals[i]);

		  }

		  

		  keys = t.keys;

		  vals = t.vals;

		  M = t.M;

	}	

	public boolean contains(Key key)
	{

		for(int i = hash(key); keys[i] != null; i = (i+1) % M) 
		{

			if(keys[i].equals(key))

				return true;

		}

		

		return false;
	}
	
	public Iterator<Key> iterator() 
	{	
		return new Iterador<Key>(keys[0]);
	}
}
