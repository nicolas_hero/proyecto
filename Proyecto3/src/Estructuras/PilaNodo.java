package Estructuras;

public class PilaNodo<T> 
{
	private T elemento;
	private PilaNodo<T> sigNodo;

	public PilaNodo( T pElemento )
	{
		elemento = pElemento;
		sigNodo = null;
	}

	public T darElemento( )
	{
		return elemento;
	}

	public PilaNodo<T> eliminarPrimero( )
	{
		PilaNodo<T> p = sigNodo;
		sigNodo = null;
		return p;
	}

	public PilaNodo<T> insertarAntes( PilaNodo<T> nodo )
	{
		nodo.sigNodo = this;
		return nodo;
	}

	public PilaNodo<T> darSiguiente( )
	{
		return sigNodo;
	}

}
