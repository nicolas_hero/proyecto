package Estructuras;

public class EdgeDGW<T> implements Comparable<EdgeDGW<T>>
{
	private final T v;
    private final T w;
    private final double weight;
    
    public EdgeDGW(T v, T w, double weight) 
    {
    	this.v = v;
    	this.w = w;
    	this.weight = weight;
    }
    
    public double weight() 
    {
    	return weight;
    }
    
    public T either() 
    {
    	return v;
    }
    
    public T other(T vertex) throws Exception 
    {
    	if(vertex.equals(v)) return w;
    	else if(vertex.equals(w)) return v;
    	throw new Exception("No coincide el nodo con los del arco.");
    }
    
    public int compareTo(EdgeDGW<T> that) 
    {    	
    	if(this.weight < that.weight) return -1;
    	else if(this.weight > that.weight) return 1;
    	else return 0;
    }
    
    public String toString() {
        return String.format("%d-%d %.2f", v, w, weight);
   }
}
