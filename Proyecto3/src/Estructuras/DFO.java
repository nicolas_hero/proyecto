package Estructuras;

import java.util.Iterator;

//Depht First Order

public class DFO <T extends Comparable<T>, K>
{
	private boolean[] marked;
	
	private Queue<T> pre;
	private Queue<T> post;
	private Stack<T> reversePost;
	
	public DFO(DirectedGraph<T, K> G) 
	{
		marked = new boolean[G.nodesAmount()];
		
		pre = new Queue<T>();
		post = new Queue<T>();
		reversePost = new Stack<T>();
		
		for(int v = 0; v < G.nodesAmount(); v++)
			if(!marked[v])
			{
				T object = (T) G.getNodes().dar(v+1);
				dfs(G, object);
			}
	}
	
	private void dfs(DirectedGraph<T, K> G, T v) 
	{
		pre.enQueue(v);
		
		marked[G.getNodes().posNode(v)-1] = true;
		
		Lista<T> adj = G.adj(v);
		
		ListaNodo<T> n = adj.darPrimerNodo();
		
		while (n != null)
		{
			T w = n.darElemento();
			if(!marked[G.getNodes().posNode(w)-1])
				dfs(G,w);
			n = n.darSiguiente();
		}		
		
		post.enQueue(v);
		reversePost.push(v);
	}	
	
	
	public Iterable<T> pre() 
	{
		return pre;
	}
	
	public Iterable<T> post() 
	{
		return post;
	}
	
	public Stack<T> reversePost()
	{
		return reversePost;
	}
}
