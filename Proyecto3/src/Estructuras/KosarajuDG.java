package Estructuras;

//Kosaraju Directed Graphs

public class KosarajuDG <T extends Comparable<T>, K>
{		
	private boolean[] marked;
	private int[] id;
	private int count;
	private Lista<Lista<T>> components;
	
	public KosarajuDG(DirectedGraph<T, K> G)
	{		
		marked = new boolean[G.nodesAmount()];		
		id = new int[G.nodesAmount()];
		components = new Lista<Lista<T>>();
		DFO<T, K> order = new DFO<T, K>(G.reverse());

		for(T s: order.reversePost())
		{
			if(!marked[G.getNodes().posNode(s)-1]) 
			{
				
				//Paseo por el reversePost para agregar a los componentes.
				Lista<T> component = new Lista<T>();	
				
				dfs(G, s);
				
				count++;
				
				for(T c: order.reversePost())
				{					
					if(id[G.getNodes().posNode(c)-1] == count)
					{						
						component.agregarAlFinal(c);
					}
				}				
				
				if(!component.estaVacia())
				{
					components.agregarAlFinal(component);
				}
			}
		}		
	}
	
	private void dfs(DirectedGraph<T,K> G, T v)
	{
		marked[G.getNodes().posNode(v)-1] = true;
		id[G.getNodes().posNode(v)-1] = count+1;		
		
		Lista<T> adj = G.adj(v);
		
		ListaNodo<T> n = adj.darPrimerNodo();
		
		while (n != null)
		{
			T w = n.darElemento();
			if(!marked[G.getNodes().posNode(w)-1])
				dfs(G,w);
			n = n.darSiguiente();
		}		
	}	
	
	public Lista<Lista<T>> getComponents()
	{
		return components;
	}
	
	public boolean stronglyConnected(int v, int w) 
	{
		return id[v] == id[w];
	}
	
	public int id(int v)
	{
		return id[v];
	}
	
	public int count() 
	{
		return count;
	}
	
}
