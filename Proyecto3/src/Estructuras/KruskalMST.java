package Estructuras;

public class KruskalMST<T extends Comparable<T>>
{
    private static final double FLOATING_POINT_EPSILON = 1E-12;

    private double weight;                        // weight of MST
    private Lista<EdgeDGW<T>> mst = new Lista<EdgeDGW<T>>();  // edges in MST

    /**
     * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
     * @param G the edge-weighted graph
     * @throws Exception 
     */
    public KruskalMST(DirectedGraphWeight<T> G) throws Exception 
    {
        // more efficient to build heap by passing array of edges
        MinPQ<EdgeDGW<T>> pq = new MinPQ<EdgeDGW<T>>();
        for (EdgeDGW<T> e : G.edges()) 
        {
            pq.insert(e);
        }

        // run greedy algorithm
        UF<T> uf = new UF<T>(G.nodesAmount());
        while (!pq.isEmpty() && mst.darTamanio() < G.nodesAmount() - 1) 
        {
            EdgeDGW<T> e = pq.delMin();
            int v = G.getNodes().posNode(e.either())-1;
            int w = G.getNodes().posNode(e.other(e.either()))-1;
            if (!uf.connected(v, w)) { // v-w does not create a cycle
                uf.union(v, w);  // merge v and w components
                mst.agregarAlFinal(e);  // add edge e to mst
                weight += e.weight();
            }
        }

        // check optimality conditions
        assert check(G);
    }

    /**
     * Returns the edges in a minimum spanning tree (or forest).
     * @return the edges in a minimum spanning tree (or forest) as
     *    an iterable of edges
     */
    public Lista<EdgeDGW<T>> edges() 
    {
        return mst;
    }

    /**
     * Returns the sum of the edge weights in a minimum spanning tree (or forest).
     * @return the sum of the edge weights in a minimum spanning tree (or forest)
     */
    public double weight() {
        return weight;
    }
    
    // check optimality conditions (takes time proportional to E V lg* V)
    private boolean check(DirectedGraphWeight<T> G) throws Exception 
    {

        // check total weight
        double total = 0.0;
        for (EdgeDGW<T> e : edges()) 
        {
            total += e.weight();
        }
        if (Math.abs(total - weight()) > FLOATING_POINT_EPSILON) 
        {
            System.err.printf("Weight of edges does not equal weight(): %f vs. %f\n", total, weight());
            return false;
        }

        // check that it is acyclic
        UF<T> uf = new UF<T>(G.nodesAmount());
        for (EdgeDGW<T> e : edges()) 
        {
            int v = G.getNodes().posNode(e.either())-1, w = G.getNodes().posNode(e.other(e.either()))-1;
            if (uf.connected(v, w)) {
                System.err.println("Not a forest");
                return false;
            }
            uf.union(v, w);
        }

        // check that it is a spanning forest
        for (EdgeDGW<T> e : G.edges())
        {
            int v = G.getNodes().posNode(e.either())-1, w = G.getNodes().posNode(e.other(e.either()))-1;
            if (!uf.connected(v, w)) {
                System.err.println("Not a spanning forest");
                return false;
            }
        }

        // check that it is a minimal spanning forest (cut optimality conditions)
        for (EdgeDGW<T> e : edges()) 
        {

            // all edges in MST except e
            uf = new UF<T>(G.nodesAmount());
            for (EdgeDGW<T> f : mst) 
            {
                int x = G.getNodes().posNode(f.either())-1, y = G.getNodes().posNode(f.other(f.either()))-1;
                if (f != e) uf.union(x, y);
            }
            
            // check that e is min weight edge in crossing cut
            for (EdgeDGW<T> f : G.edges())
            {
                int  x = G.getNodes().posNode(f.either())-1, y = G.getNodes().posNode(f.other(f.either()))-1;
                if (!uf.connected(x, y))
                {
                    if (f.weight() < e.weight())
                    {
                        System.err.println("Edge " + f + " violates cut optimality conditions");
                        return false;
                    }
                }
            }

        }

        return true;
    }
    

}

