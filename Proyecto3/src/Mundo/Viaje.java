package Mundo;



public class Viaje 
{
	private double nombre;	
	private Ciudad salida;	
	private Ciudad llegada;	
	private double horaSalida;	
	private double horaLlegada;	
	private boolean[] dias;	
	private String equipo;	
	private double[] costo;	
	private int sillas;	
	//tipo = True si el viaje es nacional, False d.l.c.
	private boolean tipo;	
	private Aerolinea aero;
	
	public Viaje(double pNombre, Ciudad pSalida, Ciudad pLlegada, double pHoraSalida, double pHoraLlegada, String pEquipo, int pSillas, boolean pTipo, Aerolinea pAero)
	{
		nombre = pNombre;
		salida = pSalida;
		llegada = pLlegada;
		horaSalida = pHoraSalida;
		horaLlegada = pHoraLlegada;		
		dias = new boolean[7];
		
		for(int i = 0; i < 7; i++)
		{
			dias[i]=false;
		}
		
		equipo = pEquipo;		
		costo = new double[7];		
		sillas = pSillas;
		tipo = pTipo;
		aero = pAero;		
		
	}
	
	public double darNombre()
	{
		return nombre;
	}
	
	public Ciudad darSalida()
	{
		return salida;
	}
	
	public Ciudad darLlegada()
	{
		return llegada;
	}
	
	public double darHoraSalida()
	{
		return horaSalida;
	}
	
	public double darHoraLlegada()
	{
		return horaLlegada;
	}
	
	public boolean darDia(int pIndice)
	{
		return dias[pIndice-1];
	}
	
	public String darEquipo()
	{
		return equipo;
	}
	
	public double darCosto(int pIndice)
	{
		return costo[pIndice-1];
	}
	
	public int darSillas()
	{
		return sillas;
	}
	
	public boolean darTipo()
	{
		return tipo;
	}
	
	public Aerolinea darAerolinea()
	{
		return aero;
	}
	
	public void cambiarCosto(double pCosto, int pIndice)
	{
		costo[pIndice - 1] = pCosto;
	}
	
	public void cambiarDia(String pDia, int pIndice)
	{
		if (pDia.equals("X"))
		{
			dias[pIndice-1]= true;
		}
		else
		{
			dias[pIndice-1]= false;
		}
		
	}
	
	public double darTiempoDeVuelo()
	{
		double tdv = horaLlegada - horaSalida;
		
		if(tdv < 0)
		{
			tdv = 24*60 + tdv;
		}
		
		return tdv;
	}
}
