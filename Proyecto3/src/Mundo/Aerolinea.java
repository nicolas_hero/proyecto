package Mundo;

public class Aerolinea implements Comparable<Aerolinea>
{
	private String nombre;	
	private int sillasMax;
	private double cpm;
	
	public Aerolinea(String pNombre)
	{
		nombre = pNombre;	
		sillasMax = 0;
		cpm = 0;
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public int darSillasMax()
	{
		return sillasMax;
	}
	
	public void cambiarSillasMax(int pSillasMax)
	{
		sillasMax = pSillasMax;
	}
	
	public double darCostoPorMinuto()
	{
		return cpm;
	}
	
	public void cambiarCostoPorMinuto(double pCpm)
	{
		cpm = pCpm;
	}

	@Override
	public int compareTo(Aerolinea pAero)
	{
		int rpta = 0;

		if (this.darNombre().compareTo(pAero.darNombre())==1)
		{
			rpta = 1;
		}
		else if (this.darNombre().compareTo(pAero.darNombre())== -1)
		{
			rpta = -1;
		}		

		return rpta;
	}
	
}
