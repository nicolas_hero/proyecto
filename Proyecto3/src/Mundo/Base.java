package Mundo;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.security.spec.MGF1ParameterSpec;
import java.util.ArrayList;

import javax.swing.JEditorPane;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import Estructuras.DijkstraSP;
import Estructuras.DirectedGraph;
import Estructuras.DirectedGraphWeight;
import Estructuras.EdgeDGW;
import Estructuras.HashTableLP;
import Estructuras.HashTableSC;
import Estructuras.HeapMin;
import Estructuras.KosarajuDG;
import Estructuras.KruskalMST;
import Estructuras.Lista;
import Estructuras.ListaNodo;
import Estructuras.MinPQ;
import Estructuras.Queue;
import Estructuras.Stack;


public class Base 
{
	private DirectedGraph<Ciudad,Lista<Viaje>> modelo;
	private Lista<Ciudad> ciudades;
	private Lista<Aerolinea> aerolineas;
	private KosarajuDG<Ciudad, Lista<Viaje>> kosarajuModelo;

	public final static String ARCHIVO_DATOS = "data/datos.json";
	public final static String ARCHIVO_CPM = "data/cpm.json";

	public Base(String pDatos, String pCPM)
	{
		try
		{
			ciudades = new Lista<Ciudad>();
			aerolineas = new Lista<Aerolinea>();
			modelo = new DirectedGraph<Ciudad, Lista<Viaje>>();			

			ciudadesAutorizadasJson(pDatos, pCPM);			
			armarModelo();		

			kosarajuModelo = new KosarajuDG<Ciudad, Lista<Viaje>>(modelo);				
		}

		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void ciudadesAutorizadasJson(String rutaDatos, String rutaCPM) throws Exception
	{
		JsonParser infoCPM = new JsonParser();

		JsonArray arrCPM = (JsonArray) infoCPM.parse(new FileReader(rutaCPM));

		for(int j = 0; j < arrCPM.size(); j++)
		{
			JsonObject objCPM = (JsonObject)arrCPM.get(j);
			JsonElement eleCPM = objCPM.get("VALOR MINUTO");
			JsonElement eleAero = objCPM.get("AEROL�NEA");
			JsonElement eleTSMax = objCPM.get("NUM SILLAS PARA VALOR MAX POR MINUTO");
			double CPM = Double.parseDouble(eleCPM.getAsString().split(" ")[0]);
			String nombreAero = eleAero.getAsString();
			int TSMax = eleTSMax.getAsInt();

			Aerolinea aeroAgregada = new Aerolinea(nombreAero);
			aeroAgregada.cambiarCostoPorMinuto(CPM);
			aeroAgregada.cambiarSillasMax(TSMax);

			aerolineas.agregarAlFinal(aeroAgregada);		
		}		

		JsonParser info = new JsonParser();

		JsonArray arr = (JsonArray) info.parse(new FileReader(rutaDatos));


		for (int i = 0; i < arr.size(); i++)
		{

			JsonObject obj = (JsonObject)arr.get(i);
			JsonElement aeroJ = obj.get("Nombre");			
			String aero = aeroJ.getAsString();	
			JsonElement numVueloJ = obj.get("N�mero de Vuelo");			
			double numVuelo = numVueloJ.getAsDouble();
			JsonElement origenJ = obj.get("Origen");			
			String origen = origenJ.getAsString();			
			JsonElement destinoJ = obj.get("Destino");
			String destino = destinoJ.getAsString();
			JsonElement hSalidaJ = obj.get("Hora de Salida");			
			double hSalida = hSalidaJ.getAsDouble();
			JsonElement hLlegadaJ = obj.get("Hora de Llegada");
			double hLlegada = hLlegadaJ.getAsDouble();
			JsonElement equipoJ = obj.get("Tipo Equipo");
			String equipo = equipoJ.getAsString();
			JsonElement numSillasJ = obj.get("N�mero de Sillas");
			int numSillas = numSillasJ.getAsInt();			
			JsonElement luJ = obj.get("Lu");
			String lu = luJ.getAsString();
			JsonElement maJ = obj.get("Ma");
			String ma = maJ.getAsString();
			JsonElement miJ = obj.get("Mi");
			String mi = miJ.getAsString();
			JsonElement juJ = obj.get("Ju");
			String ju = juJ.getAsString();
			JsonElement viJ = obj.get("Vi");
			String vi = viJ.getAsString();
			JsonElement saJ = obj.get("Sa");
			String sa = saJ.getAsString();
			JsonElement domJ = obj.get("Do");
			String dom = domJ.getAsString();
			JsonElement tipoVueloJ = obj.get("Tipo Vuelo");
			String tipoVuelo = tipoVueloJ.getAsString();

			//False si es internacional.
			boolean tipoVueloB = false;

			if (tipoVuelo.equals("Nacional"))
			{
				tipoVueloB = true;
			}

			Ciudad cOrigen = new Ciudad(origen);			
			Ciudad cDestino = new Ciudad(destino);			

			Aerolinea aeroP = new Aerolinea("");

			ListaNodo<Aerolinea> aeroPaseado = aerolineas.darPrimerNodo();
			boolean encontrado = false;
			while (aeroPaseado != null && !encontrado)
			{
				if(aeroPaseado.darElemento().darNombre().equals(aero))
				{
					aeroP = aeroPaseado.darElemento();
					encontrado = true;
				}

				aeroPaseado = aeroPaseado.darSiguiente();
			}

			Viaje viajeCreado = new Viaje(numVuelo, cOrigen, cDestino, hSalida, hLlegada, equipo, numSillas, tipoVueloB, aeroP);
			viajeCreado.cambiarDia(lu, 1);
			viajeCreado.cambiarDia(ma, 2);
			viajeCreado.cambiarDia(mi, 3);
			viajeCreado.cambiarDia(ju, 4);
			viajeCreado.cambiarDia(vi, 5);
			viajeCreado.cambiarDia(sa, 6);
			viajeCreado.cambiarDia(dom, 7);

			double[] costosViaje = new double[7];

			costosViaje = calcularCostos(viajeCreado);

			for(int k = 0; k<7;k++)
			{
				viajeCreado.cambiarCosto(costosViaje[k], k+1);
			}



			//Armar lista ciudades.



			if (!encontroCiudad(cOrigen))
			{
				ciudades.agregarAlFinal(cOrigen);				
			}

			if (!encontroCiudad(cDestino))
			{
				ciudades.agregarAlFinal(cDestino);
			}








			ListaNodo<Ciudad> analizado = ciudades.darPrimerNodo();



			while (analizado != null)
			{
				if (analizado.darElemento().darNombre().equals(cOrigen.darNombre()))
				{
					//					System.out.println(analizado.darElemento().darAerolineasSalida().darPrimerNodo().darElemento().darNombre());


					analizado.darElemento().agregarViajeSalida(viajeCreado);					

				}
				else if(analizado.darElemento().darNombre().equals(cDestino.darNombre()))
				{
					analizado.darElemento().agregarViajeLlegada(viajeCreado);					
				}


				analizado = analizado.darSiguiente();

			}





			//			System.out.println(aero + " " + numVuelo + " " + origen  + " " + destino + " " + hSalida + " " + hLlegada + " " + equipo + " " +numSillas  + " " +lu+ " " + ma+ " " + mi+ " " +ju + " " +vi + " " +sa + " " +dom + " " +tipoVuelo);

		}
	}

	public boolean agregarAerolineaCatalogo(String pNombreAero, double pCPM, int pTSMax)
	{
		Aerolinea nuevaAero = new Aerolinea(pNombreAero);
		nuevaAero.cambiarCostoPorMinuto(pCPM);
		nuevaAero.cambiarSillasMax(pTSMax);
		boolean res = false;
		if(!encontroAerolinea(nuevaAero))
		{
			aerolineas.agregarAlFinal(nuevaAero);
			res = true;
		}
		return res;		
	}

	public boolean eliminarAerolineaCatalogo(String pNombreAero)
	{
		Aerolinea aeroEliminada = new Aerolinea(pNombreAero);
		boolean res = false;
		if(encontroAerolinea(aeroEliminada))
		{
			aerolineas.eliminar(aeroEliminada);
			ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

			while (a != null)
			{
				ListaNodo<Viaje> vL = a.darElemento().darViajesLlegada().darPrimerNodo();

				while(vL != null)
				{
					if(vL.darElemento().darAerolinea().darNombre().equals(pNombreAero))
					{
						a.darElemento().darViajesLlegada().eliminar(vL.darElemento());
					}
					vL = vL.darSiguiente();
				}

				ListaNodo<Viaje> vS = a.darElemento().darViajesSalida().darPrimerNodo();

				while(vS != null)
				{
					if(vS.darElemento().darAerolinea().darNombre().equals(pNombreAero))
					{
						a.darElemento().darViajesSalida().eliminar(vS.darElemento());
					}
					vS = vS.darSiguiente();
				}
				a= a.darSiguiente();
			}
			res = true;
		}
		return res;
	}

	public boolean agregarCiudadCatalogo(String pNombreCiudad)
	{
		Ciudad nuevaCiudad = new Ciudad(pNombreCiudad);
		boolean res = false;
		if(!encontroCiudad(nuevaCiudad))
		{
			ciudades.agregarAlFinal(nuevaCiudad);
			res = true;
		}
		return res;		
	}

	public boolean eliminarCiudadCatalogo(String pNombreCiudad)
	{
		Ciudad ciudadEliminada = new Ciudad(pNombreCiudad);
		boolean res = false;
		if(encontroCiudad(ciudadEliminada))
		{
			ciudades.eliminar(ciudadEliminada);
			ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

			while (a != null)
			{
				ListaNodo<Viaje> vL = a.darElemento().darViajesLlegada().darPrimerNodo();

				while(vL != null)
				{
					if(vL.darElemento().darSalida().darNombre().equals(pNombreCiudad))
					{
						a.darElemento().darViajesLlegada().eliminar(vL.darElemento());
					}
					vL = vL.darSiguiente();
				}

				ListaNodo<Viaje> vS = a.darElemento().darViajesSalida().darPrimerNodo();

				while(vS != null)
				{
					if(vS.darElemento().darLlegada().darNombre().equals(pNombreCiudad))
					{
						a.darElemento().darViajesSalida().eliminar(vS.darElemento());
					}
					vS = vS.darSiguiente();
				}
				a= a.darSiguiente();
			}
			res = true;
		}
		return res;
	}

	public boolean agregarVueloCatalogo(double pNombre, String pNombreAerolinea, String pNombreCiudadOrigen, String pNombreCiudadDestino, double pHoraSalida, double pHoraLlegada, String pEquipo, int pCupo, boolean pTipo, String pLunes, String pMartes, String pMiercoles, String pJueves, String pViernes, String pSabado, String pDomingo)
	{
		boolean res = false;
		Ciudad origenVuelo = buscarCiudad(pNombreCiudadOrigen);
		Ciudad destinoVuelo = buscarCiudad(pNombreCiudadDestino);
		Aerolinea aerolineaVuelo = buscarAerolinea(pNombreAerolinea);	
		
		
		Viaje viajeCreado = new Viaje(pNombre, origenVuelo, destinoVuelo, pHoraSalida, pHoraLlegada, pEquipo, pCupo, pTipo, aerolineaVuelo);
		viajeCreado.cambiarDia(pLunes, 1);
		viajeCreado.cambiarDia(pMartes, 2);
		viajeCreado.cambiarDia(pMiercoles, 3);
		viajeCreado.cambiarDia(pJueves, 4);
		viajeCreado.cambiarDia(pViernes, 5);
		viajeCreado.cambiarDia(pSabado, 6);
		viajeCreado.cambiarDia(pDomingo, 7);

		double[] costosViaje = new double[7];

		costosViaje = calcularCostos(viajeCreado);

		for(int i = 0; i<7;i++)
		{
			viajeCreado.cambiarCosto(costosViaje[i], i+1);
		}
		
		if(origenVuelo != null && destinoVuelo != null && aerolineaVuelo != null)
		{
			origenVuelo.agregarViajeSalida(viajeCreado);
			destinoVuelo.agregarViajeLlegada(viajeCreado);
			res = true;
		}

		return res;
	}

	public double[] calcularCostos(Viaje pViaje)
	{
		double TM = pViaje.darAerolinea().darCostoPorMinuto();
		double TD = pViaje.darTiempoDeVuelo();
		double TS = pViaje.darSillas();
		double TSMax = pViaje.darAerolinea().darSillasMax();
		double[] costos = new double[7];

		for(int i = 0; i < 4; i++)
		{
			costos[i]= (TM*(TSMax/TS)*TD);
		}
		for(int i = 4; i < 7; i++)
		{
			costos[i]= ((TM*(TSMax/TS)*TD)*1.3);
		}

		return costos;		
	}
	
	public KosarajuDG<Ciudad,Lista<Viaje>> componentesPorAerolinea(String pAerolinea)
	{
		KosarajuDG<Ciudad,Lista<Viaje>> respuesta = new KosarajuDG<>(subModeloPorAerolinea(pAerolinea));
		return respuesta;
	}

	public KosarajuDG<Ciudad,Lista<Viaje>> componentesPorAerolineaDia(String pAerolinea,int  pDia)
	{
		KosarajuDG<Ciudad,Lista<Viaje>> respuesta = new KosarajuDG<>(subModeloPorAerolineaDia(pAerolinea, pDia));
		return respuesta;
	}

	public KosarajuDG<Ciudad,Lista<Viaje>> componentesPorDiaHora(double pHora,int pNumDia)
	{
		KosarajuDG<Ciudad,Lista<Viaje>> respuesta = new KosarajuDG<>(subModeloPorDiaHora(pHora, pNumDia));
		return respuesta;
	}

	public KosarajuDG<Ciudad,Lista<Viaje>> componentesPorDiaHoraAerolinea(double pHora,int pNumDia, String pAerolinea)
	{
		KosarajuDG<Ciudad,Lista<Viaje>> respuesta = new KosarajuDG<>(subModeloPorAerolineaDiaHora(pHora, pNumDia, pAerolinea));
		return respuesta;
	}
	


	//AC� PONER LOS M�TODOS DE LOS REQUERIMIENTOS 9, 10 Y 11.

	public Lista<EdgeDGW<Ciudad>> MSTGeneralPorOrigenTiempoDeVueloNacionales(Ciudad pCiudad, double pHora, int pNumDia) throws Exception
	{
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenTiempoDeVueloDiaHoraNacionales(pCiudad, pHora, pNumDia);
		KruskalMST<Ciudad> kruskal = new KruskalMST<Ciudad>(grafo);

		Lista<EdgeDGW<Ciudad>> mst = kruskal.edges();

		return mst;		
	}
	
	public Lista<EdgeDGW<Ciudad>> MSTGeneralPorOrigenTiempoDeVuelo(Ciudad pCiudad, double pHora, int pNumDia) throws Exception
	{
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenTiempoDeVueloDiaHora(pCiudad, pHora, pNumDia);
		KruskalMST<Ciudad> kruskal = new KruskalMST<Ciudad>(grafo);

		Lista<EdgeDGW<Ciudad>> mst = kruskal.edges();

		return mst;		
	}

	public Lista<EdgeDGW<Ciudad>> MSTPorOrigenCostoAerolineaNacionales(Ciudad pCiudad, double pHora, int pNumDia, Aerolinea pAerolinea) throws Exception
	{		
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenCostoDiaHoraAerolineaNacionales(pCiudad, pHora, pNumDia, pAerolinea.darNombre());
		
		KruskalMST<Ciudad> kruskal = new KruskalMST<Ciudad>(grafo);

		Lista<EdgeDGW<Ciudad>> mst = kruskal.edges();

		return mst;		
	}

	public HashTableLP<Aerolinea, Lista<Viaje>> itinerarioPorOrigenDestinoPartidaAerolineas(Ciudad pOrigen, Ciudad pDestino) throws Exception
	{
		HashTableLP<Aerolinea, Lista<Viaje>> respuesta = new HashTableLP<Aerolinea, Lista<Viaje>>();	

		HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>> grafos = subModelosWeightCostoAerolineas(pOrigen);

		ListaNodo<Aerolinea> a = aerolineas.darPrimerNodo();

		while(a != null)
		{
			Lista<Viaje> viajesItinerario = new Lista<Viaje>();

			if(grafos.contains(a.darElemento()))
			{


				DirectedGraphWeight<Ciudad> grafoAero = grafos.get(a.darElemento());
				DijkstraSP<Ciudad> dijkstraAero = new DijkstraSP<Ciudad>(grafoAero, pOrigen);
				Lista<EdgeDGW<Ciudad>> arcosDijkstra = dijkstraAero.pathTo(pDestino); 				

				if(arcosDijkstra != null && !arcosDijkstra.estaVacia())
				{	

					ListaNodo<EdgeDGW<Ciudad>> arcoViaje = arcosDijkstra.darPrimerNodo();
					while(arcoViaje != null)
					{						
						ListaNodo<Viaje> viajeAnalizado = arcoViaje.darElemento().either().darViajesSalida().darPrimerNodo();					

						boolean agregado = false;

						while(viajeAnalizado != null && !agregado)
						{
							Viaje viajeAnalizadoElemento = viajeAnalizado.darElemento();							

							if(viajeAnalizadoElemento.darLlegada().darNombre().equals(arcoViaje.darElemento().other(arcoViaje.darElemento().either()).darNombre()) && viajeAnalizadoElemento.darCosto(1) == arcoViaje.darElemento().weight() && viajeAnalizadoElemento.darAerolinea().darNombre().equals(a.darElemento().darNombre()))
							{								
								viajesItinerario.agregarAlFinal(viajeAnalizadoElemento);
								agregado = true;
							}
							viajeAnalizado = viajeAnalizado.darSiguiente();
						}


						arcoViaje = arcoViaje.darSiguiente();
					}
				}

				respuesta.put(a.darElemento(), viajesItinerario);
			}		

			a = a.darSiguiente();
		}

		return respuesta;	

	}

	public Lista<Viaje> itinerarioPorOrigenDestinoPartidaGeneral(Ciudad pOrigen, Ciudad pDestino) throws Exception
	{
		Lista<Viaje> respuesta = new Lista<Viaje>();	

		DirectedGraphWeight<Ciudad> grafo = subModeloWeightCostoGeneral(pOrigen);			
		DijkstraSP<Ciudad> dijkstra = new DijkstraSP<Ciudad>(grafo, pOrigen);
		Lista<EdgeDGW<Ciudad>> arcosDijkstra = dijkstra.pathTo(pDestino); 				

		if(arcosDijkstra != null && !arcosDijkstra.estaVacia())
		{
			ListaNodo<EdgeDGW<Ciudad>> arcoViaje = arcosDijkstra.darPrimerNodo();
			while(arcoViaje != null)
			{						
				ListaNodo<Viaje> viajeAnalizado = arcoViaje.darElemento().either().darViajesSalida().darPrimerNodo();					

				boolean agregado = false;

				while(viajeAnalizado != null && !agregado)
				{
					Viaje viajeAnalizadoElemento = viajeAnalizado.darElemento();							

					if(viajeAnalizadoElemento.darLlegada().darNombre().equals(arcoViaje.darElemento().other(arcoViaje.darElemento().either()).darNombre()) && viajeAnalizadoElemento.darCosto(1) == arcoViaje.darElemento().weight())
					{								
						respuesta.agregarAlFinal(viajeAnalizadoElemento);
						agregado = true;
					}
					viajeAnalizado = viajeAnalizado.darSiguiente();
				}


				arcoViaje = arcoViaje.darSiguiente();
			}
		}

		return respuesta;	
	}
	
	public Lista<Viaje> itinerarioPorOrigenCostoTodosDestinosAerolinea(Ciudad pCiudad, Aerolinea pAero) throws Exception
	{
		Lista<Viaje> respuesta = new Lista<Viaje>();		
		
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenCostoAerolinea(pCiudad, pAero.darNombre());		
		
		int numNodos = grafo.nodesAmount();
		
		boolean[] paso = new boolean[numNodos];
		
		for(int i = 0; i< numNodos;i++)
		{			
			paso[i] = false;
		}		
		
		DijkstraSP<Ciudad> dijkstra = new DijkstraSP<Ciudad>(grafo, pCiudad);
		
		paso[grafo.getNodes().posNode(pCiudad)-1] = true;	
		
		while(!pasoPorTodo(paso))
		{			
			
			Lista<Viaje> viajesMiniRecorrido = new Lista<Viaje>();
			
			Lista<EdgeDGW<Ciudad>> arcos = new Lista<EdgeDGW<Ciudad>>();
			
			ListaNodo<Ciudad> a = grafo.getNodes().darPrimerNodo();	
			
			Ciudad elegida = null;
			
			int numCiudadesRecorre = 0;		
			
			while(a != null)
			{
				if(!paso[grafo.getNodes().posNode(a.darElemento())-1] && (dijkstra.pathTo(a.darElemento()).darTamanio()+1) > numCiudadesRecorre)
				{					
					numCiudadesRecorre = dijkstra.pathTo(a.darElemento()).darTamanio()+1;	
					elegida = a.darElemento();
				}
				a = a.darSiguiente();
			}
			
			arcos = dijkstra.pathTo(elegida);
			dijkstra = new DijkstraSP<Ciudad>(grafo, elegida);
			paso[grafo.getNodes().posNode(elegida)-1] = true;
			
			ListaNodo<EdgeDGW<Ciudad>> arcoRecorrido = arcos.darPrimerNodo();			
			
			while(arcoRecorrido != null)
			{
				paso[grafo.getNodes().posNode(arcoRecorrido.darElemento().either())-1] = true;
				
				ListaNodo<Viaje> viajeAnalizado = vuelosEntreCiudadesPorAerolinea(arcoRecorrido.darElemento().either(),arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()),pAero.darNombre()).darPrimerNodo();					

				boolean agregado = false;			

				while(viajeAnalizado != null && !agregado)
				{
					Viaje viajeAnalizadoElemento = viajeAnalizado.darElemento();				

					if(viajeAnalizadoElemento.darLlegada().darNombre().equals(arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()).darNombre()) && viajeAnalizadoElemento.darCosto(1) == arcoRecorrido.darElemento().weight() && viajeAnalizadoElemento.darAerolinea().darNombre().equals(pAero.darNombre()))
					{			
						
						viajesMiniRecorrido.agregarAlFinal(viajeAnalizadoElemento);
						agregado = true;
					}
					
					viajeAnalizado = viajeAnalizado.darSiguiente();
				}
				
				arcoRecorrido = arcoRecorrido.darSiguiente();
			}
			
			ListaNodo<Viaje> viajeMR = viajesMiniRecorrido.darPrimerNodo();
			
			while (viajeMR != null)
			{
				respuesta.agregarAlFinal(viajeMR.darElemento());
				viajeMR = viajeMR.darSiguiente();
			}		
		}		
		
		return respuesta;
	}
	
	public Lista<Viaje> itinerarioPorOrigenCostoTodosDestinosAerolineaDia(Ciudad pCiudad, Aerolinea pAero, int pDia) throws Exception
	{
		Lista<Viaje> respuesta = new Lista<Viaje>();		
		
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenCostoAerolineaDia(pCiudad, pAero.darNombre(),pDia);		
		
		int numNodos = grafo.nodesAmount();
		
		boolean[] paso = new boolean[numNodos];
		
		for(int i = 0; i< numNodos;i++)
		{			
			paso[i] = false;
		}		
		
		DijkstraSP<Ciudad> dijkstra = new DijkstraSP<Ciudad>(grafo, pCiudad);
		
		paso[grafo.getNodes().posNode(pCiudad)-1] = true;	
		
		while(!pasoPorTodo(paso))
		{			
			
			Lista<Viaje> viajesMiniRecorrido = new Lista<Viaje>();
			
			Lista<EdgeDGW<Ciudad>> arcos = new Lista<EdgeDGW<Ciudad>>();
			
			ListaNodo<Ciudad> a = grafo.getNodes().darPrimerNodo();			
			
			Ciudad elegida = null;
			
			int numCiudadesRecorre = 0;		
			
			while(a != null)
			{
				if(!paso[grafo.getNodes().posNode(a.darElemento())-1] && (dijkstra.pathTo(a.darElemento()).darTamanio()+1) > numCiudadesRecorre)
				{					
					numCiudadesRecorre = dijkstra.pathTo(a.darElemento()).darTamanio()+1;	
					elegida = a.darElemento();
				}
				a = a.darSiguiente();
			}
			
			arcos = dijkstra.pathTo(elegida);
			dijkstra = new DijkstraSP<Ciudad>(grafo, elegida);
			
			ListaNodo<EdgeDGW<Ciudad>> arcoRecorrido = arcos.darPrimerNodo();			
	
			paso[grafo.getNodes().posNode(elegida)-1] = true;
			
			while(arcoRecorrido != null)
			{
				
				paso[grafo.getNodes().posNode(arcoRecorrido.darElemento().either())-1] = true;
				
				ListaNodo<Viaje> viajeAnalizado = vuelosEntreCiudadesPorAerolineaDia(arcoRecorrido.darElemento().either(),arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()),pAero.darNombre(), pDia).darPrimerNodo();					
			
				boolean agregado = false;			
	
				while(viajeAnalizado != null && !agregado)
				{
					Viaje viajeAnalizadoElemento = viajeAnalizado.darElemento();				
	
					if(viajeAnalizadoElemento.darLlegada().darNombre().equals(arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()).darNombre()) && viajeAnalizadoElemento.darTiempoDeVuelo() == arcoRecorrido.darElemento().weight() && viajeAnalizadoElemento.darAerolinea().darNombre().equals(pAero.darNombre()))
					{			
						
						viajesMiniRecorrido.agregarAlFinal(viajeAnalizadoElemento);
						agregado = true;
					}
					
					viajeAnalizado = viajeAnalizado.darSiguiente();
				}
				
				arcoRecorrido = arcoRecorrido.darSiguiente();
			}
			
			ListaNodo<Viaje> viajeMR = viajesMiniRecorrido.darPrimerNodo();
			
			while (viajeMR != null)
			{
				respuesta.agregarAlFinal(viajeMR.darElemento());
				viajeMR = viajeMR.darSiguiente();
			}		
		}		
		
		return respuesta;
	}

	public Lista<Viaje> itinerarioPorOrigenTiempoDeVueloTodosDestinosAerolinea(Ciudad pCiudad, Aerolinea pAero) throws Exception
	{
		Lista<Viaje> respuesta = new Lista<Viaje>();		
		
		DirectedGraphWeight<Ciudad> grafo = subModeloComponenteWeightPorOrigenTiempoDeVueloAerolinea(pCiudad, pAero.darNombre());		
		
		int numNodos = grafo.nodesAmount();
		
		boolean[] paso = new boolean[numNodos];
		
		for(int i = 0; i< numNodos;i++)
		{			
			paso[i] = false;
		}		
		
		DijkstraSP<Ciudad> dijkstra = new DijkstraSP<Ciudad>(grafo, pCiudad);
		
		paso[grafo.getNodes().posNode(pCiudad)-1] = true;	
		
		while(!pasoPorTodo(paso))
		{			
			
			Lista<Viaje> viajesMiniRecorrido = new Lista<Viaje>();
			
			Lista<EdgeDGW<Ciudad>> arcos = new Lista<EdgeDGW<Ciudad>>();
			
			ListaNodo<Ciudad> a = grafo.getNodes().darPrimerNodo();			
			
			Ciudad elegida = null;
			
			int numCiudadesRecorre = 0;		
			
			while(a != null)
			{
				if(!paso[grafo.getNodes().posNode(a.darElemento())-1] && (dijkstra.pathTo(a.darElemento()).darTamanio()+1) > numCiudadesRecorre)
				{					
					numCiudadesRecorre = dijkstra.pathTo(a.darElemento()).darTamanio()+1;	
					elegida = a.darElemento();
				}
				a = a.darSiguiente();
			}
			
			arcos = dijkstra.pathTo(elegida);
			dijkstra = new DijkstraSP<Ciudad>(grafo, elegida);
			
			ListaNodo<EdgeDGW<Ciudad>> arcoRecorrido = arcos.darPrimerNodo();			

			paso[grafo.getNodes().posNode(elegida)-1] = true;
			
			while(arcoRecorrido != null)
			{
				
				paso[grafo.getNodes().posNode(arcoRecorrido.darElemento().either())-1] = true;
				
				ListaNodo<Viaje> viajeAnalizado = vuelosEntreCiudadesPorAerolinea(arcoRecorrido.darElemento().either(),arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()),pAero.darNombre()).darPrimerNodo();					
			
				boolean agregado = false;			

				while(viajeAnalizado != null && !agregado)
				{
					Viaje viajeAnalizadoElemento = viajeAnalizado.darElemento();				

					if(viajeAnalizadoElemento.darLlegada().darNombre().equals(arcoRecorrido.darElemento().other(arcoRecorrido.darElemento().either()).darNombre()) && viajeAnalizadoElemento.darTiempoDeVuelo() == arcoRecorrido.darElemento().weight() && viajeAnalizadoElemento.darAerolinea().darNombre().equals(pAero.darNombre()))
					{			
						
						viajesMiniRecorrido.agregarAlFinal(viajeAnalizadoElemento);
						agregado = true;
					}
					
					viajeAnalizado = viajeAnalizado.darSiguiente();
				}
				
				arcoRecorrido = arcoRecorrido.darSiguiente();
			}
			
			ListaNodo<Viaje> viajeMR = viajesMiniRecorrido.darPrimerNodo();
			
			while (viajeMR != null)
			{
				respuesta.agregarAlFinal(viajeMR.darElemento());
				viajeMR = viajeMR.darSiguiente();
			}		
		}		
		
		return respuesta;
	}


	public boolean encontroCiudad (Ciudad pCiudad)
	{

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		boolean res = false;

		while(a != null  && !res)
		{
			if(a.darElemento().darNombre().equals(pCiudad.darNombre()))
			{
				res = true;
			}
			a = a.darSiguiente();
		}

		return res;
	}

	public boolean encontroAerolinea (Aerolinea pAero)
	{
		ListaNodo<Aerolinea> a = aerolineas.darPrimerNodo();
		boolean res = false;

		while( a != null && !res)
		{
			if(a.darElemento().darNombre().equals(pAero.darNombre()))
			{
				res = true;
			}			
			a = a.darSiguiente();		
		}	

		return res;
	}

	public void armarModelo()
	{

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		while (a != null)
		{
			modelo.addNode(a.darElemento());
			a = a.darSiguiente();
		}

		a = ciudades.darPrimerNodo();
		while(a != null)
		{
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{
					ciudadLlegada = c.darElemento();
					if(!modelo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
					{
						if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
						{
							Lista<Viaje> arco = vuelosEntreCiudades(ciudadOrigen, ciudadLlegada);
							if(!arco.estaVacia())
							{
								modelo.addEdge(ciudadOrigen, ciudadLlegada, arco);
							}							
							agregada = true;
						}
					}
					c = c.darSiguiente();
				}				


				b = b.darSiguiente();
			}
			a = a.darSiguiente();
		}
	}
	
	public DirectedGraph<Ciudad, Lista<Viaje>> subModeloPorAerolinea(String pAerolinea)
	{
		DirectedGraph<Ciudad, Lista<Viaje>> sub = new DirectedGraph<Ciudad, Lista<Viaje>>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		while (a != null)
		{
			boolean ciudadEnAerolinea = false;

			Ciudad ciudadAnalizada = a.darElemento();

			ListaNodo<Viaje> nodoViajeSalidaAnalizado = ciudadAnalizada.darViajesSalida().darPrimerNodo();
			ListaNodo<Viaje> nodoViajeLlegadaAnalizado = ciudadAnalizada.darViajesLlegada().darPrimerNodo();

			while(nodoViajeSalidaAnalizado != null)
			{
				if (nodoViajeSalidaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeSalidaAnalizado = nodoViajeSalidaAnalizado.darSiguiente();
			}

			while(nodoViajeLlegadaAnalizado != null)
			{
				if (nodoViajeLlegadaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeLlegadaAnalizado = nodoViajeLlegadaAnalizado.darSiguiente();
			}

			if(ciudadEnAerolinea)
			{
				sub.addNode(a.darElemento());
			}

			a = a.darSiguiente();
		}

		a = ciudades.darPrimerNodo();
		while(a != null)
		{
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{
					ciudadLlegada = c.darElemento();
					if(sub.getNodes().estaEnLaLista(ciudadOrigen))
					{
						if(!sub.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
						{
							if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
							{
								Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, pAerolinea);
								if(!arco.estaVacia())
								{
									sub.addEdge(ciudadOrigen, ciudadLlegada, arco);
								}
								agregada = true;
							}
						}
					}

					c = c.darSiguiente();
				}				


				b = b.darSiguiente();
			}
			a = a.darSiguiente();
		}

		return sub;
	}
	
	public DirectedGraph<Ciudad, Lista<Viaje>> subModeloPorAerolineaDia(String pAerolinea, int  pDia)
	{
		DirectedGraph<Ciudad, Lista<Viaje>> sub = new DirectedGraph<Ciudad, Lista<Viaje>>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		while (a != null)
		{
			boolean ciudadEnAerolinea = false;
	
			Ciudad ciudadAnalizada = a.darElemento();
	
			ListaNodo<Viaje> nodoViajeSalidaAnalizado = ciudadAnalizada.darViajesSalida().darPrimerNodo();
			ListaNodo<Viaje> nodoViajeLlegadaAnalizado = ciudadAnalizada.darViajesLlegada().darPrimerNodo();
	
			while(nodoViajeSalidaAnalizado != null)
			{
				if (nodoViajeSalidaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeSalidaAnalizado = nodoViajeSalidaAnalizado.darSiguiente();
			}
	
			while(nodoViajeLlegadaAnalizado != null)
			{
				if (nodoViajeLlegadaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeLlegadaAnalizado = nodoViajeLlegadaAnalizado.darSiguiente();
			}
	
			if(ciudadEnAerolinea)
			{
				sub.addNode(a.darElemento());
			}
	
			a = a.darSiguiente();
		}
	
		a = ciudades.darPrimerNodo();
		while(a != null)
		{
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();
	
			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	
	
				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();
	
				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
	
				Ciudad ciudadLlegada = null;
	
				boolean agregada = false;
	
				while(c != null && !agregada)
				{
					ciudadLlegada = c.darElemento();
					if(sub.getNodes().estaEnLaLista(ciudadOrigen))
					{
						if(!sub.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
						{
							if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
							{
								Lista<Viaje> arco = vuelosEntreCiudadesPorAerolineaDia(ciudadOrigen, ciudadLlegada, pAerolinea, pDia);
								if(!arco.estaVacia())
								{
									sub.addEdge(ciudadOrigen, ciudadLlegada, arco);
								}
								agregada = true;
							}
						}
					}
	
					c = c.darSiguiente();
				}				
	
	
				b = b.darSiguiente();
			}
			a = a.darSiguiente();
		}
	
		return sub;
	}

	public DirectedGraph<Ciudad, Lista<Viaje>> subModeloPorDiaHora(double pHora, int pNumDia)
	{
		DirectedGraph<Ciudad, Lista<Viaje>> sub = new DirectedGraph<Ciudad, Lista<Viaje>>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		while (a != null)
		{
			sub.addNode(a.darElemento());
			a = a.darSiguiente();
		}

		a = ciudades.darPrimerNodo();
		while(a != null)
		{
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{
					ciudadLlegada = c.darElemento();
					if(!sub.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
					{
						if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
						{
							Lista<Viaje> arco = vuelosEntreCiudadesPorDiaHora(ciudadOrigen, ciudadLlegada, pHora, pNumDia);
							if(!arco.estaVacia())
							{
								sub.addEdge(ciudadOrigen, ciudadLlegada, arco);
							}
							agregada = true;
						}
					}
					c = c.darSiguiente();
				}				


				b = b.darSiguiente();
			}
			a = a.darSiguiente();
		}

		return sub;
	}

	public DirectedGraph<Ciudad, Lista<Viaje>> subModeloPorAerolineaDiaHora(double pHora, int pNumDia, String pAerolinea)
	{
		DirectedGraph<Ciudad, Lista<Viaje>> sub = new DirectedGraph<Ciudad, Lista<Viaje>>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		while (a != null)
		{
			boolean ciudadEnAerolinea = false;

			Ciudad ciudadAnalizada = a.darElemento();

			ListaNodo<Viaje> nodoViajeSalidaAnalizado = ciudadAnalizada.darViajesSalida().darPrimerNodo();
			ListaNodo<Viaje> nodoViajeLlegadaAnalizado = ciudadAnalizada.darViajesLlegada().darPrimerNodo();

			while(nodoViajeSalidaAnalizado != null)
			{
				if (nodoViajeSalidaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeSalidaAnalizado = nodoViajeSalidaAnalizado.darSiguiente();
			}

			while(nodoViajeLlegadaAnalizado != null)
			{
				if (nodoViajeLlegadaAnalizado.darElemento().darAerolinea().darNombre().equals(pAerolinea))
				{
					ciudadEnAerolinea = true;
				}
				nodoViajeLlegadaAnalizado = nodoViajeLlegadaAnalizado.darSiguiente();
			}

			if(ciudadEnAerolinea)
			{
				sub.addNode(a.darElemento());
			}

			a = a.darSiguiente();
		}

		a = ciudades.darPrimerNodo();
		while(a != null)
		{
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{
					ciudadLlegada = c.darElemento();
					if(sub.getNodes().estaEnLaLista(ciudadOrigen))
					{
						if(!sub.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
						{
							if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
							{
								Lista<Viaje> arco = vuelosEntreCiudadesPorDiaHoraAerolinea(ciudadOrigen, ciudadLlegada, pHora, pNumDia, pAerolinea);
								if(!arco.estaVacia())
								{
									sub.addEdge(ciudadOrigen, ciudadLlegada, arco);
								}
								agregada = true;
							}
						}
					}

					c = c.darSiguiente();
				}				


				b = b.darSiguiente();
			}
			a = a.darSiguiente();
		}

		return sub;
	}

	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenCostoAerolinea(Ciudad cOrigen, String pAero)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorAerolinea(pAero);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();

			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	

					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

					Ciudad ciudadLlegada = null;

					boolean agregada = false;

					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();

						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, pAero);

									Lista<Double> costosViaje = new Lista<Double>();

									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

									while(vueloArco != null)
									{
										costosViaje.agregarAlFinal(vueloArco.darElemento().darCosto(1));

										vueloArco = vueloArco.darSiguiente();
									}

									ListaNodo<Double> costoLista = costosViaje.darPrimerNodo();

									double menor = Double.POSITIVE_INFINITY;

									while(costoLista != null)
									{
										if(costoLista.darElemento() < menor)
										{
											menor = costoLista.darElemento();
										}
										costoLista = costoLista.darSiguiente();
									}

									Double costoMinimo = menor;

									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
									}

									agregada = true;
								}
							}
						}
						c = c.darSiguiente();

					}

					b = b.darSiguiente();
				}
			}

			a = a.darSiguiente();

		}

		return grafo;		
	}
	
	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenCostoDeVueloAerolinea(Ciudad cOrigen, String pAero, int pDia)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();
	
		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorAerolineaDia(pAero, pDia);
	
		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();
	
		Lista<Ciudad> componenteElegido = null;
	
		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();
	
		boolean encontrado = false;
	
		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	
	
		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();
	
		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}
	
	
		a = ciudades.darPrimerNodo();
	
		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();
	
			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();
	
				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	
	
					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();
	
					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
	
					Ciudad ciudadLlegada = null;
	
					boolean agregada = false;
	
					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();
	
						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, pAero);
	
									Lista<Double> tdvViaje = new Lista<Double>();
	
									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();
	
									while(vueloArco != null)
									{
										tdvViaje.agregarAlFinal(vueloArco.darElemento().darCosto(pDia));
	
										vueloArco = vueloArco.darSiguiente();
									}
	
									ListaNodo<Double> tdvLista = tdvViaje.darPrimerNodo();
	
									double menor = Double.POSITIVE_INFINITY;
	
									while(tdvLista != null)
									{
										if(tdvLista.darElemento() < menor)
										{
											menor = tdvLista.darElemento();
										}
										tdvLista = tdvLista.darSiguiente();
									}
	
									Double tdvMinimo = menor;
	
									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
									}
	
									agregada = true;
								}
							}
						}
						c = c.darSiguiente();
	
					}
	
					b = b.darSiguiente();
				}
			}
	
			a = a.darSiguiente();
	
		}
	
		return grafo;		
	}

	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenTiempoDeVueloAerolinea(Ciudad cOrigen, String pAero)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorAerolinea(pAero);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();

			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	

					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

					Ciudad ciudadLlegada = null;

					boolean agregada = false;

					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();

						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, pAero);

									Lista<Double> tdvViaje = new Lista<Double>();

									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

									while(vueloArco != null)
									{
										tdvViaje.agregarAlFinal(vueloArco.darElemento().darTiempoDeVuelo());

										vueloArco = vueloArco.darSiguiente();
									}

									ListaNodo<Double> tdvLista = tdvViaje.darPrimerNodo();

									double menor = Double.POSITIVE_INFINITY;

									while(tdvLista != null)
									{
										if(tdvLista.darElemento() < menor)
										{
											menor = tdvLista.darElemento();
										}
										tdvLista = tdvLista.darSiguiente();
									}

									Double tdvMinimo = menor;

									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
									}

									agregada = true;
								}
							}
						}
						c = c.darSiguiente();

					}

					b = b.darSiguiente();
				}
			}

			a = a.darSiguiente();

		}

		return grafo;		
	}
	
	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenCostoDiaHora(Ciudad cOrigen, double pHora, int pNumDia)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorDiaHora(pHora, pNumDia);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{						
					ciudadLlegada = c.darElemento();

					if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
					{
						if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
						{
							Lista<Viaje> arco = vuelosEntreCiudades(ciudadOrigen, ciudadLlegada);

							Lista<Double> costosViaje = new Lista<Double>();

							ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

							while(vueloArco != null && vueloArco.darElemento().darDia(pNumDia))
							{
								costosViaje.agregarAlFinal(vueloArco.darElemento().darCosto(1));

								vueloArco = vueloArco.darSiguiente();
							}

							ListaNodo<Double> costoLista = costosViaje.darPrimerNodo();

							double menor = Double.POSITIVE_INFINITY;

							while(costoLista != null)
							{
								if(costoLista.darElemento() < menor)
								{
									menor = costoLista.darElemento();
								}
								costoLista = costoLista.darSiguiente();
							}

							Double costoMinimo = menor;

							if(!arco.estaVacia())
							{
								grafo.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
							}

							agregada = true;
						}

					}
					c = c.darSiguiente();
				}

				b = b.darSiguiente();

			}

			a = a.darSiguiente();

		}

		return grafo;		
	}

	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenTiempoDeVueloDiaHoraNacionales(Ciudad cOrigen, double pHora, int pNumDia)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorDiaHora(pHora, pNumDia);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{				
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			System.out.println(a.darElemento().darNombre());
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{	
			Ciudad ciudadOrigen = a.darElemento();
			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{				
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	

					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

					Ciudad ciudadLlegada = null;

					boolean agregada = false;

					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();

						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosNacionalesEntreCiudadesPorDiaHora(ciudadOrigen, ciudadLlegada, pHora, pNumDia);

									Lista<Double> tdvViaje = new Lista<Double>();

									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

									while(vueloArco != null)
									{
										tdvViaje.agregarAlFinal(vueloArco.darElemento().darTiempoDeVuelo());

										vueloArco = vueloArco.darSiguiente();
									}

									ListaNodo<Double> tdvLista = tdvViaje.darPrimerNodo();

									double menor = Double.POSITIVE_INFINITY;

									while(tdvLista != null)
									{
										if(tdvLista.darElemento() < menor)
										{
											menor = tdvLista.darElemento();
										}
										tdvLista = tdvLista.darSiguiente();
									}

									Double tdvMinimo = menor;

									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
									}

									agregada = true;
								}
							}
						}
						c = c.darSiguiente();
					}

					b = b.darSiguiente();
				}
			}

			a = a.darSiguiente();

		}

		return grafo;		
	}
	
	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenTiempoDeVueloDiaHora(Ciudad cOrigen, double pHora, int pNumDia)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorDiaHora(pHora, pNumDia);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{				
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{	
			Ciudad ciudadOrigen = a.darElemento();
			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{				
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	

					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

					Ciudad ciudadLlegada = null;

					boolean agregada = false;

					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();

						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosEntreCiudades(ciudadOrigen, ciudadLlegada);

									Lista<Double> tdvViaje = new Lista<Double>();

									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

									while(vueloArco != null && vueloArco.darElemento().darDia(pNumDia))
									{
										tdvViaje.agregarAlFinal(vueloArco.darElemento().darTiempoDeVuelo());

										vueloArco = vueloArco.darSiguiente();
									}

									ListaNodo<Double> tdvLista = tdvViaje.darPrimerNodo();

									double menor = Double.POSITIVE_INFINITY;

									while(tdvLista != null)
									{
										if(tdvLista.darElemento() < menor)
										{
											menor = tdvLista.darElemento();
										}
										tdvLista = tdvLista.darSiguiente();
									}

									Double tdvMinimo = menor;

									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
									}

									agregada = true;
								}
							}
						}
						c = c.darSiguiente();
					}

					b = b.darSiguiente();
				}
			}

			a = a.darSiguiente();

		}

		return grafo;		
	}

	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenCostoDiaHoraAerolineaNacionales(Ciudad cOrigen, double pHora, int pNumDia, String pAero)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();

		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorDiaHoraAerolinea(pHora, pNumDia, pAero);

		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();

		Lista<Ciudad> componenteElegido = null;

		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();

		boolean encontrado = false;

		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	

		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();

			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	

					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

					Ciudad ciudadLlegada = null;

					boolean agregada = false;

					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();

						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosNacionalesEntreCiudades(ciudadOrigen, ciudadLlegada);

									Lista<Double> costosViaje = new Lista<Double>();

									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

									while(vueloArco != null && vueloArco.darElemento().darDia(pNumDia))
									{
										costosViaje.agregarAlFinal(vueloArco.darElemento().darCosto(1));

										vueloArco = vueloArco.darSiguiente();
									}

									ListaNodo<Double> costoLista = costosViaje.darPrimerNodo();

									double menor = Double.POSITIVE_INFINITY;

									while(costoLista != null)
									{
										if(costoLista.darElemento() < menor)
										{
											menor = costoLista.darElemento();
										}
										costoLista = costoLista.darSiguiente();
									}

									Double costoMinimo = menor;

									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
									}

									agregada = true;
								}
							}
						}
						c = c.darSiguiente();

					}

					b = b.darSiguiente();
				}
			}

			a = a.darSiguiente();

		}

		return grafo;		
	}

	public DirectedGraphWeight<Ciudad> subModeloComponenteWeightPorOrigenCostoAerolineaDia(Ciudad cOrigen, String pAero, int pDia)
	{		
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();
	
		KosarajuDG<Ciudad,Lista<Viaje>> kosaraju = componentesPorAerolineaDia(pAero, pDia);
	
		Lista<Lista<Ciudad>> componentes = kosaraju.getComponents();
	
		Lista<Ciudad> componenteElegido = null;
	
		ListaNodo<Lista<Ciudad>> com = componentes.darPrimerNodo();
	
		boolean encontrado = false;
	
		while (com != null && !encontrado)
		{
			if(com.darElemento().estaEnLaLista(cOrigen))
			{
				componenteElegido = com.darElemento();
				encontrado = true;
			}
			com = com.darSiguiente();
		}	
	
		ListaNodo<Ciudad> a = componenteElegido.darPrimerNodo();
	
		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}
	
	
		a = ciudades.darPrimerNodo();
	
		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();
	
			if(grafo.getNodes().estaEnLaLista(ciudadOrigen))
			{
				ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();
	
				while (b != null)
				{
					Viaje viajeSalida = b.darElemento();	
	
					String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();
	
					ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
	
					Ciudad ciudadLlegada = null;
	
					boolean agregada = false;
	
					while(c != null && !agregada)
					{						
						ciudadLlegada = c.darElemento();
	
						if(grafo.getNodes().estaEnLaLista(ciudadLlegada))
						{
							if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
							{
								if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
								{
									Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, pAero);
	
									Lista<Double> costosViaje = new Lista<Double>();
	
									ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();
	
									while(vueloArco != null)
									{
										costosViaje.agregarAlFinal(vueloArco.darElemento().darCosto(1));
	
										vueloArco = vueloArco.darSiguiente();
									}
	
									ListaNodo<Double> costoLista = costosViaje.darPrimerNodo();
	
									double menor = Double.POSITIVE_INFINITY;
	
									while(costoLista != null)
									{
										if(costoLista.darElemento() < menor)
										{
											menor = costoLista.darElemento();
										}
										costoLista = costoLista.darSiguiente();
									}
	
									Double costoMinimo = menor;
	
									if(!arco.estaVacia())
									{
										grafo.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
									}
	
									agregada = true;
								}
							}
						}
						c = c.darSiguiente();
	
					}
	
					b = b.darSiguiente();
				}
			}
	
			a = a.darSiguiente();
	
		}
	
		return grafo;		
	}

	public HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>> subModelosWeightCostoAerolineas(Ciudad cOrigen)
	{
		HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>> grafos = new HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>>();

		ListaNodo<Aerolinea> aero = aerolineas.darPrimerNodo();

		while(aero != null)
		{
			DirectedGraphWeight<Ciudad> grafoAero = new DirectedGraphWeight<Ciudad>();


			ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
			boolean ciudadUsaAerolinea = false;
			while (a != null)
			{
				ciudadUsaAerolinea = false;
				ListaNodo<Viaje> viajeLlegada = a.darElemento().darViajesLlegada().darPrimerNodo();
				while(viajeLlegada != null && !ciudadUsaAerolinea)
				{
					if(viajeLlegada.darElemento().darAerolinea().darNombre().equals(aero.darElemento().darNombre()))
					{
						ciudadUsaAerolinea = true;
					}
					viajeLlegada = viajeLlegada.darSiguiente();
				}

				ListaNodo<Viaje> viajeSalida = a.darElemento().darViajesSalida().darPrimerNodo();
				while(viajeSalida != null && !ciudadUsaAerolinea)
				{
					if(viajeSalida.darElemento().darAerolinea().darNombre().equals(aero.darElemento().darNombre()))
					{
						ciudadUsaAerolinea = true;
					}

					viajeSalida = viajeSalida.darSiguiente();
				}

				if(ciudadUsaAerolinea)
				{
					grafoAero.addNode(a.darElemento());
				}

				a = a.darSiguiente();
			}


			a = ciudades.darPrimerNodo();

			while(a != null)
			{
				if(grafoAero.getNodes().estaEnLaLista(a.darElemento()))
				{
					Ciudad ciudadOrigen = a.darElemento();
					ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

					while (b != null)
					{
						Viaje viajeSalida = b.darElemento();	

						String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

						ListaNodo<Ciudad> c = ciudades.darPrimerNodo();



						Ciudad ciudadLlegada = null;

						boolean agregada = false;

						while(c != null && !agregada)
						{

							if(grafoAero.getNodes().estaEnLaLista(c.darElemento()))
							{
								ciudadLlegada = c.darElemento();

								if(!grafoAero.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
								{
									if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
									{
										Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, aero.darElemento().darNombre());

										Lista<Double> costosViajeAero = new Lista<Double>();

										ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();



										while(vueloArco != null)
										{									

											costosViajeAero.agregarAlFinal(vueloArco.darElemento().darCosto(1));

											vueloArco = vueloArco.darSiguiente();
										}

										ListaNodo<Double> costoLista = costosViajeAero.darPrimerNodo();

										double menor = Double.POSITIVE_INFINITY;

										while(costoLista != null)
										{
											if(costoLista.darElemento() < menor)
											{
												menor = costoLista.darElemento();
											}
											costoLista = costoLista.darSiguiente();
										}



										Double costoMinimo = menor;




										if(!arco.estaVacia())
										{

											grafoAero.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
										}

										agregada = true;
									}
								}
							}
							c = c.darSiguiente();
						}				


						b = b.darSiguiente();
					}

				}
				a = a.darSiguiente();
			}

			grafos.put(aero.darElemento(), grafoAero);

			aero = aero.darSiguiente();
		}


		return grafos;
	}

	public HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>> subModelosWeightTiempoDeVueloAerolineas(Ciudad cOrigen)
	{
		HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>> grafos = new HashTableLP<Aerolinea, DirectedGraphWeight<Ciudad>>();

		ListaNodo<Aerolinea> aero = aerolineas.darPrimerNodo();

		while(aero != null)
		{
			DirectedGraphWeight<Ciudad> grafoAero = new DirectedGraphWeight<Ciudad>();


			ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
			boolean ciudadUsaAerolinea = false;
			while (a != null)
			{
				ciudadUsaAerolinea = false;
				ListaNodo<Viaje> viajeLlegada = a.darElemento().darViajesLlegada().darPrimerNodo();
				while(viajeLlegada != null && !ciudadUsaAerolinea)
				{
					if(viajeLlegada.darElemento().darAerolinea().darNombre().equals(aero.darElemento().darNombre()))
					{
						ciudadUsaAerolinea = true;
					}
					viajeLlegada = viajeLlegada.darSiguiente();
				}

				ListaNodo<Viaje> viajeSalida = a.darElemento().darViajesSalida().darPrimerNodo();
				while(viajeSalida != null && !ciudadUsaAerolinea)
				{
					if(viajeSalida.darElemento().darAerolinea().darNombre().equals(aero.darElemento().darNombre()))
					{
						ciudadUsaAerolinea = true;
					}

					viajeSalida = viajeSalida.darSiguiente();
				}

				if(ciudadUsaAerolinea)
				{
					grafoAero.addNode(a.darElemento());
				}

				a = a.darSiguiente();
			}


			a = ciudades.darPrimerNodo();

			while(a != null)
			{
				if(grafoAero.getNodes().estaEnLaLista(a.darElemento()))
				{
					Ciudad ciudadOrigen = a.darElemento();
					ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

					while (b != null)
					{
						Viaje viajeSalida = b.darElemento();	

						String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

						ListaNodo<Ciudad> c = ciudades.darPrimerNodo();



						Ciudad ciudadLlegada = null;

						boolean agregada = false;

						while(c != null && !agregada)
						{

							if(grafoAero.getNodes().estaEnLaLista(c.darElemento()))
							{
								ciudadLlegada = c.darElemento();

								if(!grafoAero.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
								{
									if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
									{
										Lista<Viaje> arco = vuelosEntreCiudadesPorAerolinea(ciudadOrigen, ciudadLlegada, aero.darElemento().darNombre());

										Lista<Double> tdvViajeAero = new Lista<Double>();

										ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

										while(vueloArco != null)
										{									

											tdvViajeAero.agregarAlFinal(vueloArco.darElemento().darTiempoDeVuelo());

											vueloArco = vueloArco.darSiguiente();
										}

										ListaNodo<Double> tdvLista = tdvViajeAero.darPrimerNodo();

										double menor = Double.POSITIVE_INFINITY;

										while(tdvLista != null)
										{
											if(tdvLista.darElemento() < menor)
											{
												menor = tdvLista.darElemento();
											}
											tdvLista = tdvLista.darSiguiente();
										}

										Double tdvMinimo = menor;

										if(!arco.estaVacia())
										{

											grafoAero.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
										}

										agregada = true;
									}
								}
							}
							c = c.darSiguiente();
						}				


						b = b.darSiguiente();
					}

				}
				a = a.darSiguiente();
			}

			grafos.put(aero.darElemento(), grafoAero);

			aero = aero.darSiguiente();
		}


		return grafos;
	}
	
	
	public DirectedGraphWeight<Ciudad> subModeloWeightCostoGeneral(Ciudad cOrigen)
	{
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();	

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{						
					ciudadLlegada = c.darElemento();

					if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
					{
						if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
						{
							Lista<Viaje> arco = vuelosEntreCiudades(ciudadOrigen, ciudadLlegada);

							Lista<Double> costosViaje = new Lista<Double>();

							ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

							while(vueloArco != null)
							{
								costosViaje.agregarAlFinal(vueloArco.darElemento().darCosto(1));

								vueloArco = vueloArco.darSiguiente();
							}

							ListaNodo<Double> costoLista = costosViaje.darPrimerNodo();

							double menor = Double.POSITIVE_INFINITY;

							while(costoLista != null)
							{
								if(costoLista.darElemento() < menor)
								{
									menor = costoLista.darElemento();
								}
								costoLista = costoLista.darSiguiente();
							}

							Double costoMinimo = menor;

							if(!arco.estaVacia())
							{
								grafo.addEdge(ciudadOrigen, ciudadLlegada, costoMinimo);
							}

							agregada = true;
						}

					}
					c = c.darSiguiente();
				}

				b = b.darSiguiente();

			}

			a = a.darSiguiente();

		}

		return grafo;
	}

	public DirectedGraphWeight<Ciudad> subModeloWeightTiempoDeVueloGeneral(Ciudad cOrigen)
	{
		DirectedGraphWeight<Ciudad> grafo = new DirectedGraphWeight<Ciudad>();	

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{		
			grafo.addNode(a.darElemento());
			a = a.darSiguiente();
		}


		a = ciudades.darPrimerNodo();

		while(a != null)
		{			
			Ciudad ciudadOrigen = a.darElemento();
			ListaNodo<Viaje> b = ciudadOrigen.darViajesSalida().darPrimerNodo();

			while (b != null)
			{
				Viaje viajeSalida = b.darElemento();	

				String nombreCiudadLlegada = viajeSalida.darLlegada().darNombre();

				ListaNodo<Ciudad> c = ciudades.darPrimerNodo();

				Ciudad ciudadLlegada = null;

				boolean agregada = false;

				while(c != null && !agregada)
				{						
					ciudadLlegada = c.darElemento();

					if(!grafo.adj(ciudadOrigen).estaEnLaLista(ciudadLlegada))
					{
						if (ciudadLlegada.darNombre().equals(nombreCiudadLlegada))
						{
							Lista<Viaje> arco = vuelosEntreCiudades(ciudadOrigen, ciudadLlegada);

							Lista<Double> tdvViaje = new Lista<Double>();

							ListaNodo<Viaje> vueloArco = arco.darPrimerNodo();

							while(vueloArco != null)
							{
								tdvViaje.agregarAlFinal(vueloArco.darElemento().darTiempoDeVuelo());

								vueloArco = vueloArco.darSiguiente();
							}

							ListaNodo<Double> tdvLista = tdvViaje.darPrimerNodo();

							double menor = Double.POSITIVE_INFINITY;

							while(tdvLista != null)
							{
								if(tdvLista.darElemento() < menor)
								{
									menor = tdvLista.darElemento();
								}
								tdvLista = tdvLista.darSiguiente();
							}

							Double tdvMinimo = menor;

							if(!arco.estaVacia())
							{
								grafo.addEdge(ciudadOrigen, ciudadLlegada, tdvMinimo);
							}

							agregada = true;
						}

					}
					c = c.darSiguiente();
				}

				b = b.darSiguiente();

			}

			a = a.darSiguiente();

		}

		return grafo;
	}
	public Lista<Viaje> vuelosEntreCiudades(Ciudad pCiudadOrigen, Ciudad pCiudadDestino)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()))
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}
	
	public Lista<Viaje> vuelosNacionalesEntreCiudades(Ciudad pCiudadOrigen, Ciudad pCiudadDestino)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darTipo() == true)
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}

	public Lista<Viaje> vuelosEntreCiudadesPorAerolinea(Ciudad pCiudadOrigen, Ciudad pCiudadDestino, String pAerolinea)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darAerolinea().darNombre().equals(pAerolinea))
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}

	public Lista<Viaje> vuelosEntreCiudadesPorAerolineaDia(Ciudad pCiudadOrigen, Ciudad pCiudadDestino, String pAerolinea, int pDia)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();
	
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
	
		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();
	
				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darAerolinea().darNombre().equals(pAerolinea) && b.darElemento().darDia(pDia))
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}
	
		return viajes;
	}

	public Lista<Viaje> vuelosEntreCiudadesPorDiaHora(Ciudad pCiudadOrigen, Ciudad pCiudadDestino, double pHora, int pNumDia)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darHoraSalida()<= pHora && b.darElemento().darHoraLlegada() >= pHora && b.darElemento().darDia(pNumDia) == true )
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}
	
	public Lista<Viaje> vuelosNacionalesEntreCiudadesPorDiaHora(Ciudad pCiudadOrigen, Ciudad pCiudadDestino, double pHora, int pNumDia)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darHoraSalida()<= pHora && b.darElemento().darHoraLlegada() >= pHora && b.darElemento().darDia(pNumDia) == true && b.darElemento().darTipo() == true)
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}

	public Lista<Viaje> vuelosEntreCiudadesPorDiaHoraAerolinea(Ciudad pCiudadOrigen, Ciudad pCiudadDestino, double pHora, int pNumDia, String pAerolinea)
	{
		Lista<Viaje> viajes = new Lista<Viaje>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		while (a != null)
		{
			if (a.darElemento().darNombre().equals(pCiudadOrigen.darNombre()))
			{
				ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();

				while (b != null)
				{
					if (b.darElemento().darLlegada().darNombre().equals(pCiudadDestino.darNombre()) && b.darElemento().darHoraSalida()<= pHora && b.darElemento().darHoraLlegada() >= pHora && b.darElemento().darDia(pNumDia) == true && b.darElemento().darAerolinea().darNombre().equals(pAerolinea))
					{
						viajes.agregarAlFinal(b.darElemento());
					}
					b = b.darSiguiente();
				}
			}
			a = a.darSiguiente();
		}

		return viajes;
	}
	
	public Ciudad buscarCiudad(String pNombre)
	{
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();
		
		Ciudad rpta = null;
		
		boolean encontrada = false;
		
		while(a != null && !encontrada)
		{
			if(a.darElemento().darNombre().equals(pNombre))
			{
				rpta = a.darElemento();
			}
			a = a.darSiguiente();
		}
		
		return rpta;
	}
	
	public Aerolinea buscarAerolinea(String pAero)
	{
		ListaNodo<Aerolinea> a = aerolineas.darPrimerNodo();
		
		Aerolinea rpta = null;
		
		boolean encontrada = false;
		
		while(a != null && !encontrada)
		{
			if(a.darElemento().darNombre().equals(pAero))
			{
				rpta = a.darElemento();
			}
			a = a.darSiguiente();
		}
		
		return rpta;
	}
	
	public boolean pasoPorTodo(boolean[] pVector)
	{
		boolean rpta = true;
		
		for(int i = 0; i< pVector.length; i++)
		{
			if(!pVector[i])
			{
				rpta = false;
			}
		}
		
		return rpta;
	}


	//Prueba de listas con informaci�n.
	public void leer()
	{
		ListaNodo<Ciudad> a  = ciudades.darPrimerNodo();

		while(a != null)
		{
			System.out.println("---------------------- CIUDAD: "+ a.darElemento().darNombre() + " ----------------------" );
			System.out.println("***** VIAJES DE LLEGADA *****");
			ListaNodo<Viaje> b = a.darElemento().darViajesSalida().darPrimerNodo();
			while (b != null)
			{
				String nombreCiudadSalida = b.darElemento().darSalida().darNombre();
				System.out.println("Ciudad de donde sale: " + nombreCiudadSalida);
				System.out.println("Viaje: " + b.darElemento().darNombre());
				System.out.println("Costo viaje entre semana: "+ b.darElemento().darCosto(1) );				
				System.out.println("Costo viaje fin de semana: "+ b.darElemento().darCosto(5) );


				b = b.darSiguiente();
			}	

			System.out.println("***** VIAJES DE SALIDA *****");
			ListaNodo<Viaje> c = a.darElemento().darViajesLlegada().darPrimerNodo();
			while (c != null)
			{
				String nombreCiudadLlegada = c.darElemento().darLlegada().darNombre();
				System.out.println("Ciudad donde llega:" + nombreCiudadLlegada);
				System.out.println("Viaje:" + c.darElemento().darNombre());
				System.out.println("Costo viaje entre semana: "+ c.darElemento().darCosto(1) );				
				System.out.println("Costo viaje fin de semana: "+ c.darElemento().darCosto(5) );

				c = c.darSiguiente();
			}

			a = a.darSiguiente();

		}

		System.out.println("++++++++++++++++++++ AEROLINEAS ++++++++++++++++++++" );
		ListaNodo<Aerolinea> z = aerolineas.darPrimerNodo();
		while (z != null)
		{
			System.out.println("|| "+z.darElemento().darNombre()+ " ||");
			System.out.println("CPM: " + z.darElemento().darCostoPorMinuto());
			System.out.println("TSMax: " + z.darElemento().darSillasMax());

			z = z.darSiguiente();
		}	

		System.out.println(System.currentTimeMillis()/Math.pow(10, 3));


	}



	//Prueba del grafo armado y su kosaraju.
	public void leer1()
	{
		System.out.println("NODOS GRAFO:");
		Lista<Ciudad> listaNodos = modelo.getNodes();		
		ListaNodo<Ciudad> n = listaNodos.darPrimerNodo();

		int contador = 0;
		while( n != null)
		{
			contador ++;
			System.out.println(contador + ". "+ n.darElemento().darNombre());
			n = n.darSiguiente();			
		}

		System.out.println("N�mero de NODOS: " + ciudades.darTamanio());		


		int numComponentes = kosarajuModelo.count();

		System.out.println("El n�mero de componentes en el grafo es: " + numComponentes);

		Lista<Lista<Ciudad>> componentes = kosarajuModelo.getComponents();

		ListaNodo<Lista<Ciudad>> comp = componentes.darPrimerNodo();

		int contadorComp = 0;

		while(comp != null)
		{
			contadorComp ++;			
			System.out.println("Componente n�mero " + contadorComp + ":");
			ListaNodo<Ciudad> ciudadComp = comp.darElemento().darPrimerNodo();

			while(ciudadComp != null)
			{
				System.out.println("+ " + ciudadComp.darElemento().darNombre());
				ciudadComp = ciudadComp.darSiguiente();
			}

			comp = comp.darSiguiente();

		}

		System.out.println("N�mero de componentes en la lista: " + componentes.darTamanio());			

		System.out.println(System.currentTimeMillis()/Math.pow(10, 3));
	}

	//Prueba del requerimiento 7 y 8.
	public void leerReq8(double pHora, int pNumDia, String pAerolinea)
	{


		System.out.println("||| COMPONENTES PARA LA AEROLINEA " + pAerolinea + " |||");

		KosarajuDG<Ciudad,Lista<Viaje>> kosarajuReq7 = componentesPorDiaHoraAerolinea(pHora, pNumDia, pAerolinea);

		Lista<Lista<Ciudad>> componentesReq7 = kosarajuReq7.getComponents();

		ListaNodo<Lista<Ciudad>> compReq7 = componentesReq7.darPrimerNodo();

		int contadorCompReq7 = 0;



		while(compReq7 != null)
		{
			contadorCompReq7 ++;

			System.out.println("Componente n�mero " + contadorCompReq7 + ":");
			ListaNodo<Ciudad> ciudadCompReq7 = compReq7.darElemento().darPrimerNodo();

			while(ciudadCompReq7 != null)
			{
				System.out.println("+ " + ciudadCompReq7.darElemento().darNombre());
				ciudadCompReq7 = ciudadCompReq7.darSiguiente();
			}

			compReq7 = compReq7.darSiguiente();
		}

		System.out.println("N�mero de componentes en la lista: " + componentesReq7.darTamanio());

		System.out.println(System.currentTimeMillis()/Math.pow(10, 3));
	}

	public void leerReq7(double pHora, int pNumDia)
	{


		System.out.println("||| COMPONENTES |||");

		KosarajuDG<Ciudad,Lista<Viaje>> kosarajuReq7 = componentesPorDiaHora(pHora, pNumDia);

		Lista<Lista<Ciudad>> componentesReq7 = kosarajuReq7.getComponents();

		ListaNodo<Lista<Ciudad>> compReq7 = componentesReq7.darPrimerNodo();

		int contadorCompReq7 = 0;



		while(compReq7 != null)
		{
			contadorCompReq7 ++;

			System.out.println("Componente n�mero " + contadorCompReq7 + ":");
			ListaNodo<Ciudad> ciudadCompReq7 = compReq7.darElemento().darPrimerNodo();

			while(ciudadCompReq7 != null)
			{
				System.out.println("+ " + ciudadCompReq7.darElemento().darNombre());
				ciudadCompReq7 = ciudadCompReq7.darSiguiente();
			}

			compReq7 = compReq7.darSiguiente();
		}

		System.out.println("N�mero de componentes en la lista: " + componentesReq7.darTamanio());

		System.out.println(System.currentTimeMillis()/Math.pow(10, 3));
	}

	//Prueba requerimiento 12.
	public void leerReq12(String pOrigen, String pDestino)
	{
		HashTableLP<Aerolinea, Lista<Viaje>> itinerarios = new HashTableLP<Aerolinea, Lista<Viaje>>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		boolean aEncontrada = false;
		boolean bEncontrada = false;
		while ( a!= null && !aEncontrada)
		{
			if( a.darElemento().darNombre().equals(pOrigen))
			{
				aEncontrada = true;
				ListaNodo<Ciudad> b = ciudades.darPrimerNodo();
				while ( b!= null && !bEncontrada)
				{
					if( b.darElemento().darNombre().equals(pDestino))
					{
						bEncontrada = true;
						try
						{							
							itinerarios = itinerarioPorOrigenDestinoPartidaAerolineas(a.darElemento(),b.darElemento());

						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}

					b= b.darSiguiente();
				}				
			}

			a= a.darSiguiente();
		}

		ListaNodo<Aerolinea> c = aerolineas.darPrimerNodo();

		Lista<Viaje> itinerarioAerolinea = new Lista<Viaje>();



		while(c != null)
		{		
			itinerarioAerolinea = itinerarios.get(c.darElemento());									

			ListaNodo<Viaje> viajeItinerario = itinerarioAerolinea.darPrimerNodo();		

			if(itinerarioAerolinea.estaVacia() || !itinerarioAerolinea.darPrimero().darSalida().darNombre().equals(pOrigen) || !itinerarioAerolinea.darUltimo().darLlegada().darNombre().equals(pDestino))
			{
				System.out.println("La ciudad de origen no puede llegar a la ciudad destino por medio de la aerolinea " + c.darElemento().darNombre() );
			}
			else
			{
				System.out.println("||| Itinerario para llegar desde " + pOrigen + " a " + pDestino + " por medio de la aerol�nea "+ c.darElemento().darNombre() +" |||");
				while(viajeItinerario != null)
				{				
					System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " + viajeItinerario.darElemento().darLlegada().darNombre() + " gastando " + viajeItinerario.darElemento().darCosto(1) + " d�lares");
					viajeItinerario = viajeItinerario.darSiguiente();
				}
			}

			c = c.darSiguiente();
		}	


	}

	public void leerReq13(String pOrigen, String pDestino)
	{
		Lista<Viaje> itinerario = new Lista<Viaje>();
		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();

		boolean aEncontrada = false;
		boolean bEncontrada = false;
		while ( a != null && !aEncontrada)
		{
			if( a.darElemento().darNombre().equals(pOrigen))
			{
				aEncontrada = true;
				ListaNodo<Ciudad> b = ciudades.darPrimerNodo();
				while ( b!= null && !bEncontrada)
				{
					if( b.darElemento().darNombre().equals(pDestino))
					{
						bEncontrada = true;
						try
						{							
							itinerario = itinerarioPorOrigenDestinoPartidaGeneral(a.darElemento(),b.darElemento());

						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}

					b = b.darSiguiente();
				}				
			}

			a = a.darSiguiente();
		}

		ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();


		if(itinerario.estaVacia() || !itinerario.darPrimero().darSalida().darNombre().equals(pOrigen) || !itinerario.darUltimo().darLlegada().darNombre().equals(pDestino))
		{
			System.out.println("La ciudad de origen "+ pOrigen +" no puede llegar a la ciudad destino "+ pDestino +" de ninguna manera");
		}
		else
		{
			System.out.println("||| Itinerario para llegar desde " + pOrigen + " a " + pDestino + " |||");
			while(viajeItinerario != null)
			{				
				System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " + viajeItinerario.darElemento().darLlegada().darNombre() + " por medio de la aerol�nea " + viajeItinerario.darElemento().darAerolinea().darNombre() + " gastando " + viajeItinerario.darElemento().darCosto(1) + " d�lares");
				viajeItinerario = viajeItinerario.darSiguiente();
			}
		}



	}

	public void leerReq11(String pOrigen, double pHora, int pNumDia) throws Exception
	{

		Lista<EdgeDGW<Ciudad>> mst = new Lista<EdgeDGW<Ciudad>>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();				

		boolean encontrado = false;

		while(a != null && !encontrado)
		{
			if(a.darElemento().darNombre().equals(pOrigen))
			{
				mst = MSTGeneralPorOrigenTiempoDeVuelo(a.darElemento(), pHora, pNumDia);
				encontrado = true;
			}
			a = a.darSiguiente();
		}

		ListaNodo<EdgeDGW<Ciudad>> arco = mst.darPrimerNodo();

		System.out.println("MST del componente donde se encuentra la ciudad " + pOrigen + ":");
		while(arco != null)
		{		
			System.out.println(arco.darElemento().either().darNombre() + " -> " + arco.darElemento().other(arco.darElemento().either()).darNombre() + ", con un tiempo de vuelo de " + arco.darElemento().weight()/60 + " horas");
			arco = arco.darSiguiente();
		}	
	}

	public void leerReq10(String pOrigen, String pAerolinea, double pHora, int pNumDia) throws Exception
	{

		Lista<EdgeDGW<Ciudad>> mst = new Lista<EdgeDGW<Ciudad>>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();				

		boolean aEncontrado = false;
		boolean bEncontrado = false;

		while(a != null && !aEncontrado)
		{
			if(a.darElemento().darNombre().equals(pOrigen))
			{
				ListaNodo<Aerolinea> b = aerolineas.darPrimerNodo();

				while(b != null && !bEncontrado)
				{
					if(b.darElemento().darNombre().equals(pAerolinea))
					{
						
						mst = MSTPorOrigenCostoAerolineaNacionales(a.darElemento(), pHora, pNumDia, b.darElemento());
						bEncontrado = true;
					}
					b = b.darSiguiente();
				}				
				aEncontrado = true;
			}
			a = a.darSiguiente();
		}

		ListaNodo<EdgeDGW<Ciudad>> arco = mst.darPrimerNodo();
		
		System.out.println("MST del componente donde se encuentra la ciudad " + pOrigen + " usando la aerol�nea " + pAerolinea +":");
		while(arco != null)
		{		
			System.out.println(arco.darElemento().either().darNombre() + " -> " + arco.darElemento().other(arco.darElemento().either()).darNombre() + ", con un costo de " + arco.darElemento().weight() + " d�lares");
			arco = arco.darSiguiente();
		}	
	}
	
	public void leerReq9(String pOrigen, double pHora, int pNumDia) throws Exception
	{
		Lista<EdgeDGW<Ciudad>> mst = new Lista<EdgeDGW<Ciudad>>();

		ListaNodo<Ciudad> a = ciudades.darPrimerNodo();				

		boolean encontrado = false;

		while(a != null && !encontrado)
		{
			if(a.darElemento().darNombre().equals(pOrigen))
			{
				mst = MSTGeneralPorOrigenTiempoDeVueloNacionales(a.darElemento(), pHora, pNumDia);
				encontrado = true;
			}
			a = a.darSiguiente();
		}

		ListaNodo<EdgeDGW<Ciudad>> arco = mst.darPrimerNodo();

		
		if(mst.estaVacia())
		{
			System.out.println("La ciudad "+ pOrigen +", en dicha hora y d�a, conforma un componente singular en el grafo, por tanto, no hay conexi�n con otra ciudad ni MST correspondiente");
		}
		else
		{
			System.out.println("MST del componente donde se encuentra la ciudad " + pOrigen + ":");
			while(arco != null)
			{		
				System.out.println(arco.darElemento().either().darNombre() + " -> " + arco.darElemento().other(arco.darElemento().either()).darNombre() + ", con un tiempo de vuelo de " + arco.darElemento().weight()/60 + " horas");
				arco = arco.darSiguiente();
			}	
		}
		
	}
	
	public void leerReq14(String pOrigen, String pAerolinea) throws Exception
	{
		Aerolinea aerolinea = buscarAerolinea(pAerolinea);
		Ciudad ciudad = buscarCiudad(pOrigen);
		
		Lista<Viaje> itinerario = itinerarioPorOrigenCostoTodosDestinosAerolinea(ciudad, aerolinea);
		
		Lista<Ciudad> ciudades = subModeloComponenteWeightPorOrigenTiempoDeVueloAerolinea(ciudad, pAerolinea).getNodes();
		
		String ciudadesString = "";
		
		ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
		
		while(c != null)
		{
			if(c.equals(ciudades.darPrimerNodo()))
			{
				ciudadesString = ciudadesString + c.darElemento().darNombre();
			}
			else
			{
				ciudadesString = ciudadesString + ", " + c.darElemento().darNombre();
			}
			c = c.darSiguiente();
		}
		
		ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();
		System.out.println("NOTA IMPORTANTE: No se puede llegar a todas las ciudades que cubre la aerolinea " + pAerolinea + " desde " + pOrigen +", por tanto, el itinerario se");
		System.out.println("hace sobre las ciudades del COMPONENTE donde se encuentra (ciudades accesibles)");
		System.out.println("||| Itinerario con menor costo de viaje desde "+ pOrigen + " para pasar por todas las ciudades accesibles que cubra la aerolinea " + pAerolinea + " |||");
		System.out.println("Ciudades accesibles que cubre "+ pAerolinea + ": " + ciudadesString);
		while( viajeItinerario != null)
		{
			System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " +  viajeItinerario.darElemento().darLlegada().darNombre() + " con un costo de " +  viajeItinerario.darElemento().darCosto(1) + " d�lares");
			viajeItinerario = viajeItinerario.darSiguiente();
		}
	}
	
	public void leerReq15(String pOrigen, String pAerolinea) throws Exception
	{
		Aerolinea aerolinea = buscarAerolinea(pAerolinea);
		Ciudad ciudad = buscarCiudad(pOrigen);
		
		Lista<Viaje> itinerario = itinerarioPorOrigenTiempoDeVueloTodosDestinosAerolinea(ciudad, aerolinea);
		
		Lista<Ciudad> ciudades = subModeloComponenteWeightPorOrigenCostoAerolinea(ciudad, pAerolinea).getNodes();
		
		String ciudadesString = "";
		
		ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
		
		while(c != null)
		{
			if(c.equals(ciudades.darPrimerNodo()))
			{
				ciudadesString = ciudadesString + c.darElemento().darNombre();
			}
			else
			{
				ciudadesString = ciudadesString + ", " + c.darElemento().darNombre();
			}
			c = c.darSiguiente();
		}
		
		ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();
		System.out.println("NOTA IMPORTANTE: No se puede llegar a todas las ciudades que cubre la aerolinea " + pAerolinea + " desde " + pOrigen +", por tanto, el itinerario se");
		System.out.println("hace sobre las ciudades del COMPONENTE donde se encuentra (ciudades accesibles)");
		System.out.println("||| Itinerario con menor tiempo de viaje desde "+ pOrigen + " para pasar por todas las ciudades accesibles que cubra la aerolinea " + pAerolinea + " |||");
		System.out.println("Ciudades accesibles que cubre "+ pAerolinea + ": " + ciudadesString);
		while( viajeItinerario != null)
		{
			System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " +  viajeItinerario.darElemento().darLlegada().darNombre() + " con un tiempo de vuelo de " +  viajeItinerario.darElemento().darTiempoDeVuelo()/60 + " horas");
			viajeItinerario = viajeItinerario.darSiguiente();
		}
	}
	
	public void leerReq16(String pOrigen, String pAerolinea, int pDia) throws Exception
	{
		Aerolinea aerolinea = buscarAerolinea(pAerolinea);
		Ciudad ciudad = buscarCiudad(pOrigen);
		
		Lista<Viaje> itinerario = itinerarioPorOrigenCostoTodosDestinosAerolineaDia(ciudad, aerolinea, pDia);
		
		Lista<Ciudad> ciudades = subModeloComponenteWeightPorOrigenCostoAerolineaDia(ciudad, pAerolinea, pDia).getNodes();
		
		String ciudadesString = "";
		
		ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
		
		while(c != null)
		{
			if(c.equals(ciudades.darPrimerNodo()))
			{
				ciudadesString = ciudadesString + c.darElemento().darNombre();
			}
			else
			{
				ciudadesString = ciudadesString + ", " + c.darElemento().darNombre();
			}
			c = c.darSiguiente();
		}
		
		ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();
		System.out.println("NOTA IMPORTANTE: No se puede llegar a todas las ciudades que cubre la aerolinea " + pAerolinea + " desde " + pOrigen +", por tanto, el itinerario se");
		System.out.println("hace sobre las ciudades del COMPONENTE donde se encuentra (ciudades accesibles)");
		System.out.println("||| Itinerario con menor tiempo de viaje desde "+ pOrigen + " para pasar por todas las ciudades accesibles que cubra la aerolinea " + pAerolinea + " |||");
		System.out.println("Ciudades accesibles que cubre "+ pAerolinea + ": " + ciudadesString);
		while( viajeItinerario != null)
		{
			System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " +  viajeItinerario.darElemento().darLlegada().darNombre() + " con un tiempo de vuelo de " +  viajeItinerario.darElemento().darTiempoDeVuelo()/60 + " horas");
			viajeItinerario = viajeItinerario.darSiguiente();
		}
	}

	public void leerReq17(String pAerolinea) throws Exception
	{
		Aerolinea aerolinea = buscarAerolinea(pAerolinea);
		DirectedGraph<Ciudad, Lista<Viaje>> grafo = subModeloPorAerolinea(pAerolinea);
		
		Lista<Ciudad> ciudades = grafo.getNodes();		
		
		int maxNumCiudades = 0;
		
		double costoMinimo = Double.POSITIVE_INFINITY;
		
		ListaNodo<Ciudad> c = ciudades.darPrimerNodo();
		
		Ciudad elegida = null;	
		
		c = ciudades.darPrimerNodo();
		
		while(c != null)
		{
						
			
			Lista<Ciudad> ciudadesAccesibles = subModeloComponenteWeightPorOrigenCostoAerolinea(c.darElemento(), pAerolinea).getNodes();
			
			if(ciudadesAccesibles.darTamanio() >= maxNumCiudades)
			{					
				Lista<Viaje> itinerario = itinerarioPorOrigenCostoTodosDestinosAerolinea(c.darElemento(), aerolinea);
				
				maxNumCiudades = ciudadesAccesibles.darTamanio();
				
				ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();
				
				double costoTotal = 0;
				
				while( viajeItinerario != null)
				{
					costoTotal += viajeItinerario.darElemento().darCosto(1);
					viajeItinerario = viajeItinerario.darSiguiente();
				}
				
				if (costoTotal < costoMinimo)
				{
					System.out.println("Empezando en la ciudad " + c.darElemento().darNombre() + " puedo pasar por " + maxNumCiudades + " ciudades gastando " + costoTotal + " d�lares en total");
					costoMinimo = costoTotal;
					elegida = c.darElemento();
				}
			}
			
			c = c.darSiguiente();
		}
		
		System.out.println();
		
		Lista<Viaje> itinerario = itinerarioPorOrigenTiempoDeVueloTodosDestinosAerolinea(elegida, aerolinea);
		
		Lista<Ciudad> ciudadesElegida = subModeloComponenteWeightPorOrigenCostoAerolinea(elegida, pAerolinea).getNodes();
		
		String ciudadesString = "";
		
		ListaNodo<Ciudad> cElegida = ciudadesElegida.darPrimerNodo();
		
		while(cElegida != null)
		{
			if(cElegida.equals(ciudadesElegida.darPrimerNodo()))
			{
				ciudadesString = ciudadesString + cElegida.darElemento().darNombre();
			}
			else
			{
				ciudadesString = ciudadesString + ", " + cElegida.darElemento().darNombre();
			}
			cElegida = cElegida.darSiguiente();
		}
		
		ListaNodo<Viaje> viajeItinerario = itinerario.darPrimerNodo();
		
		boolean diaEncontrado = false;
		
		int dia = 0;
		
		for(int i = 1; i<8 && !diaEncontrado ; i++)
		{
			if(viajeItinerario.darElemento().darDia(i))
			{
				dia = i;
				diaEncontrado = true;
			}
		}
		
		String diaNombre = "";
		
		if (dia == 1)
		{
			diaNombre = "Lunes";
		}
		else if(dia ==2)
		{
			diaNombre = "Martes";
		}
		else if(dia ==3)
		{
			diaNombre = "Miercoles";
		}
		else if(dia ==4)
		{
			diaNombre = "Jueves";
		}
		else if(dia ==5)
		{
			diaNombre = "Viernes";
		}
		else if(dia ==6)
		{
			diaNombre = "S�bado";
		}
		else if(dia ==7)
		{
			diaNombre = "Domingo";
		}
		
		System.out.println();		
		System.out.println("El viajero debe comenzar su traves�a saliendo de la ciudad " + elegida.darNombre() + " el dia " + diaNombre + " a las " + viajeItinerario.darElemento().darHoraSalida()/60 + " horas con el viaje " + viajeItinerario.darElemento().darNombre());
		System.out.println("A continuaci�n se presenta el itinerario de tal decisi�n:");
		System.out.println();
		while( viajeItinerario != null)
		{
			System.out.println("- Viaje " + viajeItinerario.darElemento().darNombre() + " desde " + viajeItinerario.darElemento().darSalida().darNombre() + " hasta " +  viajeItinerario.darElemento().darLlegada().darNombre() + " con un costo de " +  viajeItinerario.darElemento().darCosto(1) + " d�lares");
			viajeItinerario = viajeItinerario.darSiguiente();
		}
		
	}

	public static void main(String args[])	
	{		
		new Base(ARCHIVO_DATOS,ARCHIVO_CPM);		
	}
}