package Mundo;

import Estructuras.Lista;



public class Ciudad implements Comparable<Ciudad>
{
	private String nombre;	
	private Lista<Viaje> vLlegada;	
	private Lista<Viaje> vSalida;

	public Ciudad(String pNombre)
	{
		nombre = pNombre;
		vLlegada = new Lista<Viaje>();
		vSalida = new Lista<Viaje>();
	}

	public String darNombre()
	{
		return nombre;
	}

	public Lista<Viaje> darViajesLlegada()
	{
		return vLlegada;
	}

	public Lista<Viaje> darViajesSalida()
	{
		return vSalida;
	}

	public void agregarViajeLlegada(Viaje pViaje)
	{
		vLlegada.agregarAlFinal(pViaje);
	}

	public void agregarViajeSalida(Viaje pViaje)
	{
		vSalida.agregarAlFinal(pViaje);
	}

	public void eliminarViajelineaLlegada(Viaje pViaje)
	{
		vLlegada.eliminar(pViaje);
	}

	public void eliminarViajelineaSalida(Viaje pViaje)
	{
		vSalida.eliminar(pViaje);
	}
	
	

	@Override
	public int compareTo(Ciudad pCiudad) 
	{


		int rpta = 0;

		if (this.darNombre().compareTo(pCiudad.darNombre())==1)
		{
			rpta = 1;
		}
		else if (this.darNombre().compareTo(pCiudad.darNombre())== -1)
		{
			rpta = -1;
		}		

		return rpta;
	}



}
